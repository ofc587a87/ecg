package com.progenies.ecg.asm31.model.factory;

import com.progenies.ecg.asm31.model.codeparts.TryCatchCodePartASM31;
import com.progenies.ecg.model.codeparts.TryCatchCodePart;
import com.progenies.ecg.model.codeparts.TryCatchCodePart.CatchBlock;
import com.progenies.ecg.model.factory.IObjectTryCatchFactory;

public class ObjectTryCatchFactoryASM31 implements IObjectTryCatchFactory {

	@Override
	public TryCatchCodePart createTryCatchBlock()
	{
		return new TryCatchCodePartASM31();
	}

	@Override
	public CatchBlock createCatchBlock(Class<? extends Throwable> exceptionCls) {
		return new TryCatchCodePartASM31.CatchBlockASM31(exceptionCls);
	}

}
