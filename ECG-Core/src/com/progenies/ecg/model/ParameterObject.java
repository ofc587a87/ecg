package com.progenies.ecg.model;

/**
 * Represents any of the parameters of the current method.
 * @author oc587a87
 *
 */
public final class ParameterObject <T>
{
	/** Name of the parameter */
	private String name;
	
	/** Class type of the parameter. Primitive types will be used
	 * by their wrapper classes, using autoboxing. */
	private Class<T> classType;
	
	/** Static value of the parameter **/ 
	private T value;
	
	/** Variable that will populate the value of the parameter. Can be either local or instance variable */
	private String variableName;

	/**
	 * Public constructor of the class.
	 * @param name Name of the parameter
	 * @param classType Class type of the parameter. Primitives must be specified by their wrapper classes.
	 */
	public ParameterObject(String name, Class<T> classType)
	{
		this.name=name;
		this.classType=classType;
	}

	/**
	 * Public constructor of the class.
	 * @param name Name of the parameter
	 * @param classType Class type of the parameter. Primitives must be specified by their wrapper classes.
	 * @param value Static value of the parameter 
	 */
	public ParameterObject(String name, Class<T> classType, T value)
	{
		this(name, classType);
		this.value=value;
	}

	/**
	 * Public constructor of the class.
	 * @param name Name of the parameter
	 * @param classType Class type of the parameter. Primitives must be specified by their wrapper classes.
	 * @param variableName Variable that store the value of the parameter. Can be either local or instance variable.
	 */
	public ParameterObject(String name, String variableName, Class<T> classType)
	{
		this(name, classType);
		this.variableName=variableName;
	}
	
	
	
	
	@SuppressWarnings("unchecked")
	public  static <T> ParameterObject<T> changeType(ParameterObject<?> parameterObject, Class<T> paramType) {
		ParameterObject<T> result=new ParameterObject<T>(parameterObject.name, paramType);
		
		result.value=(T)parameterObject.value;
		result.variableName=parameterObject.variableName;
		
		return result;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Class<T> getClassType() {
		return classType;
	}

	public void setClassType(Class<T> classType) {
		this.classType = classType;
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}

	public String getVariableName() {
		return variableName;
	}

	public void setVariableName(String variableName) {
		this.variableName = variableName;
	}



}