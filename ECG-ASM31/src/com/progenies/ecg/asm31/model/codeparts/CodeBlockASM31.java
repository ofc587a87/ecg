package com.progenies.ecg.asm31.model.codeparts;

import java.util.Iterator;

import org.objectweb.asm.MethodVisitor;

import com.progenies.ecg.asm31.util.VariableRegister;
import com.progenies.ecg.model.AbstractCodeObject;
import com.progenies.ecg.model.codeparts.CodeBlock;
import com.progenies.ecg.model.codeparts.ICodePart;

public class CodeBlockASM31 extends CodeBlock implements IConfigurableCodePart
{

	
	@Override
	public void configure(MethodVisitor visitor, AbstractCodeObject parent, VariableRegister varRegister)
	{
		Iterator<ICodePart> it=this.codeParts.iterator();
		while(it.hasNext())
			((IConfigurableCodePart)it.next()).configure(visitor, parent, varRegister);
	}

	@Override
	public boolean isReturnCodePart() {
		return false;
	}

}
