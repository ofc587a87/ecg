package com.progenies.ecg.model.factory;

import com.progenies.ecg.model.codeparts.TryCatchCodePart;
import com.progenies.ecg.model.codeparts.TryCatchCodePart.CatchBlock;

public interface IObjectTryCatchFactory
{
	
	
	
	public TryCatchCodePart createTryCatchBlock();

	public CatchBlock createCatchBlock(Class<? extends Throwable> exceptionCls);

}
