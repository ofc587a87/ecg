package com.progenies.ecg.asm31.model.codeparts.lines;

import java.util.Arrays;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

import com.progenies.ecg.asm31.util.BytecodeInstructionsUtils;
import com.progenies.ecg.asm31.util.GenerationUtils;
import com.progenies.ecg.asm31.util.VariableRegister.Variable;
import com.progenies.ecg.model.ParameterObject;

public class CreateObjectLinePart<T> extends ConfigurableLineCodePart
{
	
	private Class<T> classType;
	private ParameterObject<?> constructorParams[];
	private String variableName;

	public CreateObjectLinePart(Class<T> classType,	ParameterObject<?>[] constructorParams, String variableName)
	{
		this.setClassType(classType);
		this.setConstructorParams(constructorParams);
		this.setVariableName(variableName);
	}
	
	
	
	@Override
	public void generateBytecode() {
		
		if(this.visitor==null )
			throw new IllegalStateException("LinePart has not been configurated, please use ClassGenerator");
		
		//creates new object and store in stack
		BytecodeInstructionsUtils.createNewObjectIntoStack(visitor, Type.getType(classType), constructorParams, varRegister);
		
		//store the value into the variable (if i must to)
		if(this.variableName!=null)
		{
			Variable var=varRegister.getVariable(variableName);
			if(var.isLocal())
				visitor.visitVarInsn(var.getType().getOpcode(Opcodes.ISTORE), var.getIndex());
			else if(var.isStatic())
				visitor.visitFieldInsn(Opcodes.PUTSTATIC, GenerationUtils.getFieldOwner(var.getName(), parent), var.getName(), var.getType().getDescriptor());
			else
			{
				//i must insert "this" before the value. I'll insert at the end and move it later to the start
				BytecodeInstructionsUtils.insertLocalVariableIntoStack(visitor, varRegister, "this");
				visitor.visitInsn(Type.getType(classType).getSize()==1?Opcodes.DUP_X1:Opcodes.DUP2_X1); //duplicate the last value of size 2 ('this') and insert it at the start
				visitor.visitInsn(Opcodes.POP); //delete the extra reference
				visitor.visitFieldInsn(Opcodes.PUTFIELD, GenerationUtils.getFieldOwner(var.getName(), parent), var.getName(), var.getType().getDescriptor());
			}
				
		}
		else //empty stack
		{
			visitor.visitInsn(Opcodes.POP);
		}
		
	}



	public Class<T> getClassType() {
		return classType;
	}

	public void setClassType(Class<T> classType) {
		this.classType = classType;
	}

	public ParameterObject<?>[] getConstructorParams() {
		return constructorParams==null?null:Arrays.copyOf(constructorParams, constructorParams.length);
	}

	public void setConstructorParams(ParameterObject<?> constructorParams[]) {
		this.constructorParams = constructorParams==null?null:Arrays.copyOf(constructorParams, constructorParams.length);
	}

	public String getVariableName() {
		return variableName;
	}

	public void setVariableName(String variableName) {
		this.variableName = variableName;
	}

}
