package test.progenies.ecg.asm31.arrayvariables.bidimensional.primitives;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;

import org.junit.Test;
import org.objectweb.asm.Opcodes;

import test.progenies.ecg.asm31.arrayvariables.bidimensional.BaseTestInstancelArrayVariables;

import com.progenies.ecg.model.ClassObject;
import com.progenies.ecg.model.FieldObject;
import com.progenies.ecg.model.MethodObject;
import com.progenies.ecg.model.ValueReference;
import com.progenies.ecg.model.codeparts.CodeBlock;

public class TestStaticArrayVariables extends BaseTestInstancelArrayVariables
{
	@Test
	public void testCreateNotInitializedArrayVariable() throws Exception
	{
		ClassObject cls=createClass();
		
		//an instance array will be default initialized to null
		FieldObject<int[][]> fld0=clsGenerator.objectFactory.fields.createField("instanceArray", int[][].class);
		fld0.setModifiers(new int[] {Opcodes.ACC_STATIC});
		cls.getFields().add(fld0);
		
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int[][].class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceArray"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNull(result);
	}
	
	@Test
	public void testCreateNullArrayVariable() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int[][].class);
		
		FieldObject<int[][]> fld0=clsGenerator.objectFactory.fields.createField("instanceArray", int[][].class);
		fld0.setModifiers(new int[] {Opcodes.ACC_STATIC});
		fld0.setValue(null);
		cls.getFields().add(fld0);
				
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceArray"));
		method.setCode(block);
		cls.getMethods().add(method);

		Class<?> clsResult=clsGenerator.generateClass(cls);

		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNull(result);
	}
	
	@Test
	public void testCreateEmptyArrayVariable() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int[][].class);
		
		FieldObject<int[][]> fld0=(clsGenerator.objectFactory.fields.createFieldArrayWithDefaultValue("instanceArray", int[][].class, new int[5][3]));
		fld0.setModifiers(new int[] {Opcodes.ACC_STATIC});
		cls.getFields().add(fld0);

		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceArray"));
		method.setCode(block);
		cls.getMethods().add(method);

		Class<?> clsResult=clsGenerator.generateClass(cls);

		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertTrue(result instanceof int[][]);
		assertArrayEquals(new int[5][3], (int[][])result);
	}
	
	
	
	@Test
	public void testCreateStaticValuesArrayVariable() throws Exception
	{
		ClassObject cls=createClass();
		
		FieldObject<int[][]> fld0=(clsGenerator.objectFactory.fields.createFieldArrayWithDefaultValue("instanceArray", int[][].class, new int[][] {{1, 2, 5}, {2, 6, 1}}));
		fld0.setModifiers(new int[] {Opcodes.ACC_STATIC});
		cls.getFields().add(fld0);

		
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int[][].class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceArray"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);

		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertTrue(result instanceof int[][]);
		assertArrayEquals(new int[][] {{1, 2, 5}, {2, 6, 1}}, (int[][])result);
	}
	
	@Test
	public void testCreateMixedTypedArrayVariable() throws Exception
	{
		ClassObject cls=createClass();
		
		cls.getFields().add(clsGenerator.objectFactory.fields.createFieldWithDefaultValue("instanceFieldVal1", int.class, 2));
		cls.getFields().add(clsGenerator.objectFactory.fields.createFieldWithDefaultValue("instanceFieldVal2", long.class, 3l));
		
		FieldObject<int[][]> fld0=(clsGenerator.objectFactory.fields.createFieldArray("instanceArray", int[][].class, new ValueReference[][] {{
			ValueReference.forVariable("instanceFieldVal1"),
			ValueReference.forVariable("instanceFieldVal2"),
			ValueReference.forVariable("instanceVar"),
			ValueReference.forVariable("instanceVar2"),
			ValueReference.forVariable("staticVar"),
			ValueReference.forVariable("staticVar2")},{
			ValueReference.forStaticValue(97),
			ValueReference.forStaticValue(98l),
			ValueReference.forStaticValue(99d),
			ValueReference.forStaticValue(100f),
			ValueReference.forFieldInVariable("instanceValue", "instanceObj"),
			ValueReference.forFieldInVariable("instanceValue2", "instanceObj")},{
			ValueReference.forFieldInVariable("instanceValue", "staticObj"),
			ValueReference.forFieldInVariable("instanceValue2", "staticObj"),
			ValueReference.forStaticFieldInClass("staticValue", TestObj.class),
			ValueReference.forStaticFieldInClass("staticValue2", TestObj.class),
			ValueReference.forStaticValue(97),
			ValueReference.forStaticValue(98l)
			}}));
		fld0.setModifiers(new int[] {Opcodes.ACC_STATIC});
		cls.getFields().add(fld0);

		
		
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int[][].class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceArray"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);

		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertTrue(result instanceof int[][]);
		assertArrayEquals(new int[][] {{2, 3, 4, 5, 6, 7}, {97, 98, 99, 100, 50, 51}, {50, 51, 100, 101,97, 98}}, (int[][])result);
	}
	
	
	
	
	
	/*--------------------------------------------
	 *----------- From method invocation --------- 
	 *--------------------------------------------
	 */
	
	
	@Test
	public void testCreateArrayFromLocalMethod() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int[][].class);
		
		FieldObject<int[][]> fld0=(clsGenerator.objectFactory.fields.createFieldArray("instanceArray", int[][].class));
		fld0.setModifiers(new int[] {Opcodes.ACC_STATIC});
		cls.getFields().add(fld0);

		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.methods.invokeMethodAndStore("createArray", "this", null, int[][].class, "instanceArray"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceArray"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);

		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertTrue(result instanceof int[][]);
		assertArrayEquals(new int[][] {{2,4,6},	{3,5,7}, {4,6,8}}, (int[][])result);
	}
	
	
	
	@Test
	public void testCreateArrayFromExternalInstanceMethod() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int[][].class);
		
		FieldObject<int[][]> fld0=(clsGenerator.objectFactory.fields.createFieldArray("instanceArray", int[][].class));
		fld0.setModifiers(new int[] {Opcodes.ACC_STATIC});
		cls.getFields().add(fld0);

		
		FieldObject<MethodClass> fld=clsGenerator.objectFactory.fields.createField("localObj", MethodClass.class);
		fld.setInitialize(true);
		cls.getFields().add(fld);
		
		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.methods.invokeMethodAndStore("createArray", "localObj", null, int[][].class, "instanceArray"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceArray"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);

		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertTrue(result instanceof int[][]);
		assertArrayEquals(new int[][] {{2,4,6},	{3,5,7}, {4,6,8}}, (int[][])result);
	}
	
	
	
	@Test
	public void testCreateArrayFromStaticMethod() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int[][].class);
		
		FieldObject<int[][]> fld0=(clsGenerator.objectFactory.fields.createFieldArray("instanceArray", int[][].class));
		fld0.setModifiers(new int[] {Opcodes.ACC_STATIC});
		cls.getFields().add(fld0);

		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.methods.invokeStaticMethodAndStore("createStaticArray", MethodClass.class, null, int[][].class, "instanceArray"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceArray"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);

		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertTrue(result instanceof int[][]);
		assertArrayEquals(new int[][] {{2,4,6,8}, {3,5,7,9}}, (int[][])result);
	}

}
