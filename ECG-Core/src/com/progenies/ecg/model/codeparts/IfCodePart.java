package com.progenies.ecg.model.codeparts;

import com.progenies.ecg.model.conditions.ICondition;

/**
 * Represent a simple If block, without else clause
 * 
 * @author ofc587a87
 *
 */
public abstract class IfCodePart implements ICodePart
{
	/**
	 * Body of the IF block 
	 */
	protected CodeBlock bodyBlock;
	
	/**
	 * Condition of the IF clause
	 */
	protected ICondition condition;

	
	

	public CodeBlock getBodyBlock() {
		return bodyBlock;
	}

	public void setBodyBlock(CodeBlock bodyBlock) {
		this.bodyBlock = bodyBlock;
	}



	public ICondition getCondition() {
		return condition;
	}



	public void setCondition(ICondition condition) {
		this.condition = condition;
	}

}
