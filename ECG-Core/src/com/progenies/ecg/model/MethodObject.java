package com.progenies.ecg.model;

/**
 * Represents a method of a class.
 * @author ofc587a87
 *
 */
public abstract class MethodObject extends ConstructorObject
{
	
	
	/** Return type of the method. A null value will represent a void type */
	protected Class<?> returnType;

	
	
	




	public Class<?> getReturnType() {
		return returnType;
	}




	public void setReturnType(Class<?> returnType) {
		this.returnType = returnType;
	}




	
}
