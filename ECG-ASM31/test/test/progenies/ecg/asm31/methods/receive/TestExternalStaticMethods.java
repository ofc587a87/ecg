package test.progenies.ecg.asm31.methods.receive;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.junit.Test;

import test.progenies.ecg.asm31.methods.BaseTestExternalStaticMethods;

import com.progenies.ecg.model.ClassObject;
import com.progenies.ecg.model.MethodObject;
import com.progenies.ecg.model.ParameterObject;
import com.progenies.ecg.model.codeparts.CodeBlock;

/**
 * Test method invocation, being the methods static from an external class
 * @author ofc587a87
 *
 */
public class TestExternalStaticMethods extends BaseTestExternalStaticMethods {

	
	
	/* --------------------------------------------- */
	/* --    Receipt Tests    ----------------------- */
	/* --------------------------------------------- */
	
	@Test
	public void testReceiveVoid() throws Exception
	{
		ClassObject cls=createClass();
		
		//create method
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setCode(clsGenerator.lineFactory.methods.invokeStaticMethod("testVoid", MethodClass.class, null, null));
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=(Object) clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNull(result);
		assertEquals(1, MethodClass.methodCheck());
	}
	
	@Test
	public void testReceivePrimitiveSize1() throws Exception
	{
		ClassObject cls=createClass();
		
		//create method
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("value", int.class));
		block.add(clsGenerator.lineFactory.methods.invokeStaticMethodAndStore("testInt", MethodClass.class, null, int.class, "value"));
		block.add(clsGenerator.lineFactory.methods.invokeStaticMethod("setValue", MethodClass.class, new ParameterObject<?>[]{new ParameterObject<Integer>("p1", "value", int.class)}, null));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=(Object) clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNull(result);
		assertEquals(5, MethodClass.methodCheck());
	}
	
	
	@Test
	public void testReceivePrimitiveSize2() throws Exception
	{
		ClassObject cls=createClass();
		
		//create method
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("value", int.class));
		block.add(clsGenerator.lineFactory.methods.invokeStaticMethodAndStore("testDouble", MethodClass.class, null, double.class, "value"));
		block.add(clsGenerator.lineFactory.methods.invokeStaticMethod("setValue", MethodClass.class, new ParameterObject<?>[]{new ParameterObject<Integer>("p1", "value", int.class)}, null));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=(Object) clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNull(result);
		assertEquals(7, MethodClass.methodCheck());
	}
	
	@Test
	public void testReceiveObject() throws Exception
	{
		ClassObject cls=createClass();
		
		//create method
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("tmpObj", ValueObject.class));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("value", int.class));
		block.add(clsGenerator.lineFactory.methods.invokeStaticMethodAndStore("testValueObject", MethodClass.class, new ParameterObject<?>[] {new ParameterObject<Integer>("param1", int.class, 8)}, ValueObject.class, "tmpObj"));
		block.add(clsGenerator.lineFactory.methods.invokeMethodAndStore("getInternalValue", "tmpObj", null, int.class, "value"));
		block.add(clsGenerator.lineFactory.methods.invokeStaticMethod("setValue", MethodClass.class, new ParameterObject<?>[]{new ParameterObject<Integer>("p1", "value", int.class)}, null));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=(Object) clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNull(result);
		assertEquals(8, MethodClass.methodCheck());
	}
	
	
	@Test(expected=NullPointerException.class)
	public void testReceiveNullObject() throws Throwable
	{
		ClassObject cls=createClass();
		
		//create method
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("tmpObj", ValueObject.class));
		block.add(clsGenerator.lineFactory.methods.invokeStaticMethodAndStore("testValueObject", MethodClass.class, new ParameterObject<?>[] {new ParameterObject<Integer>("param1", int.class, -8)}, ValueObject.class, "tmpObj"));
		block.add(clsGenerator.lineFactory.methods.invokeMethodAndStore("getInternalValue", "tmpObj", null, int.class, "value"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=(Object) clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		try
		{
			mth.invoke(obj, (Object[])null);
			fail("Should has been thrown an exception!");
		}catch(InvocationTargetException ex)
		{
			throw ex.getCause();
		}
	}
	
}
