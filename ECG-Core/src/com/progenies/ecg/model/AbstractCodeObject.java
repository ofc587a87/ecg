package com.progenies.ecg.model;

import java.util.Arrays;


/**
 * Abstract class base of all objects inside a java class
 * that are entities by themselves
 * @author ofc587a87
 *
 */
public abstract class AbstractCodeObject
{	
	/** Name of the entity object */
	protected String name;
	
	/**
	 * Modifiers applicable to the object.
	 * @see org.objectweb.asm.Opcodes
	 */
	protected int[] modifiers;
	
	
	
	/**
	 * Generate the bytecode. Must be implemented by each engine.
	 */
	public abstract void generateBytecode();
	
	
	
	public String getName() {
		return name;
	}
	
	public void setName(String name) {
		this.name = name;
	}
	
	public int[] getModifiers() {
		return modifiers==null?null:Arrays.copyOf(modifiers, modifiers.length);
	}
	
	public void setModifiers(int[] modifiers) {
		this.modifiers = modifiers==null?null:Arrays.copyOf(modifiers, modifiers.length);
	}
	
	
}
