package com.progenies.ecg.model.codeparts.lines;

import com.progenies.ecg.model.ParameterObject;

public interface IMethodFactory {

	/**
	 * Generate a LineCodePart that will generate a method invocation, not storing the result.
	 * @param name Name of the method to invoke
	 * @param classOwner Class owner of the method. A null value will means a local method.
	 * @param params parameters of the method, they can be static or dynamic values. A null or empty array will mean no parameters.
	 * @param returnType returnType of the invoked method. Null value for void methods.
	 * @return LineCodePArt created
	 */
	public <T> LineCodePart invokeMethod(String name, String variableOwner, ParameterObject<?> params[], Class<?> returnType);

	/**
	 * Generate a LineCodePart that will generate a method invocation, storing the result in a local or instance variable.
	 * @param name Name of the method to invoke
	 * @param classOwner Class owner of the method. A null value will means a local method.
	 * @param params parameters of the method, they can be static or dynamic values. A null or empty array will mean no parameters.
	 * @param returnType returnType of the invoked method. Null value for void methods.
	 * @param variableName name of the variable that will store the result. It can be either local or instance variable.
	 * @return LineCodePArt created
	 */
	public <T> LineCodePart invokeMethodAndStore(String name, String variableOwner, ParameterObject<?> params[], Class<?> returnType, String variableName);

	/**
	 * Generate a LineCodePart that will generate a method invocation, storing the result in a local or instance variable.
	 * @param name Name of the method to invoke
	 * @param classOwner Class owner of the method. A null value will means a local method.
	 * @param params parameters of the method, they can be static or dynamic values. A null or empty array will mean no parameters.
	 * @param returnType returnType of the invoked method. Null value for void methods.
	 * @param variableName name of the variable that will store the result. It can be either local or instance variable.
	 * @return LineCodePArt created
	 */
	public <T> LineCodePart invokeMethodAndReturn(String name, String variableOwner, ParameterObject<?> params[], Class<?> returnType);

	/**
	 * Generate a LineCodePart that will generate a method invocation, not storing the result.
	 * @param name Name of the method to invoke
	 * @param classOwner Class owner of the method. A null value will means a local method.
	 * @param params parameters of the method, they can be static or dynamic values. A null or empty array will mean no parameters.
	 * @return LineCodePArt created
	 */
	public <T> LineCodePart invokeStaticMethod(String name, Class<T> classOwner, ParameterObject<?> params[], Class<?> returnType);

	/**
	 * Generate a LineCodePart that will generate a method invocation, storing the result in a local or instance variable.
	 * @param name Name of the method to invoke
	 * @param classOwner Class owner of the method. A null value will means a local method.
	 * @param params parameters of the method, they can be static or dynamic values. A null or empty array will mean no parameters.
	 * @param variableName name of the variable that will store the result. It can be either local or instance variable.
	 * @return LineCodePArt created
	 */
	public <T> LineCodePart invokeStaticMethodAndStore(String name, Class<T> classOwner, ParameterObject<?> params[], Class<?> returnType, String variableName);
	
	/**
	 * Generate a LineCodePart that will generate a method invocation, storing the result in a local or instance variable.
	 * @param name Name of the method to invoke
	 * @param classOwner Class owner of the method. A null value will means a local method.
	 * @param params parameters of the method, they can be static or dynamic values. A null or empty array will mean no parameters.
	 * @param variableName name of the variable that will store the result. It can be either local or instance variable.
	 * @return LineCodePArt created
	 */
	public <T> LineCodePart invokeStaticMethodAndReturn(String name, Class<T> classOwner, ParameterObject<?> params[], Class<?> returnType);
	
	/**
	 * Generate a LineCodePart that returns a variable value from a method
	 * @param name name of the variable, either local or instance variable.
	 * @return LiceCodePart created
	 */
	public LineCodePart returnVariableValue(String name);
	
	public LineCodePart returnEmpty();
	
	public <T> LineCodePart returnStaticValue(T value);
	
	

	public LineCodePart invokeSuperConstructor();

	public LineCodePart invokeSuperConstructor(ParameterObject<?> params[]);
	
	
	public LineCodePart throwNewException(Class<? extends Throwable> exception, String message);
	
	public LineCodePart throwNewException(Class<? extends Throwable> exception, String message, String causeVariableName);
	
	public LineCodePart throwException(String variableName);
	
	
}
