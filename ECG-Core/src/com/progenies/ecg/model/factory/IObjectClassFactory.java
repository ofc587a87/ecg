package com.progenies.ecg.model.factory;

import com.progenies.ecg.model.ClassObject;

public interface IObjectClassFactory
{
	
	/**
	 * Creates a new ClassObject model that will be the base of the new Class.
	 * It will default its base class to java.lang.Object
	 * @param className Name of the new class, including package
	 * @return Class Object
	 */
	public ClassObject createClass(String className);
	
	/**
	 * Creates a new ClassObject model that will be the base of the new Class.
	 * Its base class will be the specified one.
	 * @param className Name of the new class, including package
	 * @param baseClass Base class of the new class. It cannot be null.
	 * @return Class Object
	 */
	public ClassObject createClass(String className, Class<?> baseClass);

}
