package com.progenies.ecg.model.codeparts.lines;

import com.progenies.ecg.model.codeparts.CodeBlock;

public abstract class LineFactory
{
	
	public final IVariableFactory variables=createVariableFactory();
	
	public final IMethodFactory methods=createMethodFactory();
	
	public final IObjectManipulationFactory objects=createObjectManipulationFactory();
	
	
	

	public abstract CodeBlock createCodeBlock();

	protected abstract IVariableFactory createVariableFactory();

	protected abstract IMethodFactory createMethodFactory();
	
	protected abstract IObjectManipulationFactory createObjectManipulationFactory();




}