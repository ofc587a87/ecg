package test.progenies.ecg.asm31.variables.conversion;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.junit.Test;

import com.progenies.ecg.model.ClassObject;
import com.progenies.ecg.model.FieldObject;
import com.progenies.ecg.model.MethodObject;
import com.progenies.ecg.model.codeparts.CodeBlock;

import test.progenies.ecg.asm31.variables.BaseTestLocalVariables;

public class TestInstanceVariables extends BaseTestLocalVariables
{
	@Test
	public void testSetVariableWithIntegerToByteConversion() throws Exception
	{
		ClassObject cls=createClass();
		
		FieldObject<Byte> fld=clsGenerator.objectFactory.fields.createField("instanceField1", byte.class);
		cls.getFields().add(fld);
		
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setReturnType(byte.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar2", int.class, 76));
		block.add(clsGenerator.lineFactory.variables.setVariableValueFromVariable("instanceField1", "localVar2"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceField1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Byte);
		assertEquals((byte)76, result);
	}
	
	@Test
	public void testSetVariableWithIntegerToCharConversion() throws Exception
	{
		ClassObject cls=createClass();
		
		FieldObject<Character> fld=clsGenerator.objectFactory.fields.createField("instanceField1", char.class);
		cls.getFields().add(fld);

		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setReturnType(char.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar2", int.class, 65)); //ASCII 65 -> 'A'
		block.add(clsGenerator.lineFactory.variables.setVariableValueFromVariable("instanceField1", "localVar2"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceField1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Character);
		assertEquals('A', result);
	}
	
	@Test
	public void testSetVariableWithIntegerToShortConversion() throws Exception
	{
		ClassObject cls=createClass();

		FieldObject<Short> fld=clsGenerator.objectFactory.fields.createField("instanceField1", short.class);
		cls.getFields().add(fld);

		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setReturnType(short.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar2", int.class, 75));
		block.add(clsGenerator.lineFactory.variables.setVariableValueFromVariable("instanceField1", "localVar2"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceField1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Short);
		assertEquals((short)75, result);
	}
	
	@Test
	public void testSetVariableWithLongToIntegerConversion() throws Exception
	{
		ClassObject cls=createClass();
		
		FieldObject<Integer> fld=clsGenerator.objectFactory.fields.createField("instanceField1", int.class);
		cls.getFields().add(fld);

		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setReturnType(int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar2", long.class, 75l));
		block.add(clsGenerator.lineFactory.variables.setVariableValueFromVariable("instanceField1", "localVar2"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceField1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Integer);
		assertEquals(75, result);
	}
	
	@Test
	public void testSetVariableWithFloatToIntegerConversion() throws Exception
	{
		ClassObject cls=createClass();
		
		FieldObject<Integer> fld=clsGenerator.objectFactory.fields.createField("instanceField1", int.class);
		cls.getFields().add(fld);

		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setReturnType(int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar2", float.class, 75.3f)); //always lower round (discard decimals)
		block.add(clsGenerator.lineFactory.variables.setVariableValueFromVariable("instanceField1", "localVar2"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceField1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Integer);
		assertEquals(75, result);
	}
	
	@Test
	public void testSetVariableWithDoubleToIntegerConversion() throws Exception
	{
		ClassObject cls=createClass();
		
		FieldObject<Integer> fld=clsGenerator.objectFactory.fields.createField("instanceField1", int.class);
		cls.getFields().add(fld);

		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setReturnType(int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar2", double.class, 75.9d)); //always lower round (discard decimals)
		block.add(clsGenerator.lineFactory.variables.setVariableValueFromVariable("instanceField1", "localVar2"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceField1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Integer);
		assertEquals(75, result);
	}
	
	@Test
	public void testSetVariableWithIntegerToLongConversion() throws Exception
	{
		ClassObject cls=createClass();
		
		FieldObject<Long> fld=clsGenerator.objectFactory.fields.createField("instanceField1", long.class);
		cls.getFields().add(fld);

		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setReturnType(long.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar2", int.class, 75));
		block.add(clsGenerator.lineFactory.variables.setVariableValueFromVariable("instanceField1", "localVar2"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceField1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Long);
		assertEquals(75l, result);
	}
	
	
	@Test
	public void testSetVariableWithFloatToLongConversion() throws Exception
	{
		ClassObject cls=createClass();
		
		FieldObject<Long> fld=clsGenerator.objectFactory.fields.createField("instanceField1", long.class);
		cls.getFields().add(fld);

		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setReturnType(long.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar2", float.class, 75.3f));
		block.add(clsGenerator.lineFactory.variables.setVariableValueFromVariable("instanceField1", "localVar2"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceField1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Long);
		assertEquals(75l, result);
	}
	
	@Test
	public void testSetVariableWithDoubleToLongConversion() throws Exception
	{
		ClassObject cls=createClass();
		
		FieldObject<Long> fld=clsGenerator.objectFactory.fields.createField("instanceField1", long.class);
		cls.getFields().add(fld);

		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setReturnType(long.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar2", double.class, 75.9d));
		block.add(clsGenerator.lineFactory.variables.setVariableValueFromVariable("instanceField1", "localVar2"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceField1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Long);
		assertEquals(75l, result);
	}
	
	
	@Test
	public void testSetVariableWithIntegerToFloatConversion() throws Exception
	{
		ClassObject cls=createClass();
		
		FieldObject<Float> fld=clsGenerator.objectFactory.fields.createField("instanceField1", float.class);
		cls.getFields().add(fld);

		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setReturnType(float.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar2", int.class, 75));
		block.add(clsGenerator.lineFactory.variables.setVariableValueFromVariable("instanceField1", "localVar2"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceField1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Float);
		assertEquals(75f, result);
	}
	
	@Test
	public void testSetVariableWithLongToFloatConversion() throws Exception
	{
		ClassObject cls=createClass();
		
		FieldObject<Float> fld=clsGenerator.objectFactory.fields.createField("instanceField1", float.class);
		cls.getFields().add(fld);

		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setReturnType(float.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar2", long.class, 75l));
		block.add(clsGenerator.lineFactory.variables.setVariableValueFromVariable("instanceField1", "localVar2"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceField1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Float);
		assertEquals(75f, result);
	}
	
	@Test
	public void testSetVariableWithDoubleToFloatConversion() throws Exception
	{
		ClassObject cls=createClass();
		
		FieldObject<Float> fld=clsGenerator.objectFactory.fields.createField("instanceField1", float.class);
		cls.getFields().add(fld);

		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setReturnType(float.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar2", double.class, 75.3d));
		block.add(clsGenerator.lineFactory.variables.setVariableValueFromVariable("instanceField1", "localVar2"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceField1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Float);
		assertTrue(Math.abs(75.3f-((Float)result).floatValue())<0.0001f); //difference less than 0.001, due to float point precision
	}
	
	@Test
	public void testSetVariableWithIntegerToDoubleConversion() throws Exception
	{
		ClassObject cls=createClass();
		
		FieldObject<Double> fld=clsGenerator.objectFactory.fields.createField("instanceField1", double.class);
		cls.getFields().add(fld);

		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setReturnType(double.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar2", int.class, 75));
		block.add(clsGenerator.lineFactory.variables.setVariableValueFromVariable("instanceField1", "localVar2"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceField1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Double);
		assertEquals(75d, result);
	}
	
	@Test
	public void testSetVariableWithLongToDoubleConversion() throws Exception
	{
		ClassObject cls=createClass();
		
		FieldObject<Double> fld=clsGenerator.objectFactory.fields.createField("instanceField1", double.class);
		cls.getFields().add(fld);

		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setReturnType(double.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar2", long.class, 75l));
		block.add(clsGenerator.lineFactory.variables.setVariableValueFromVariable("instanceField1", "localVar2"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceField1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Double);
		assertEquals(75d, result);
	}
	
	@Test
	public void testSetVariableWithFloatToDoubleConversion() throws Exception
	{
		ClassObject cls=createClass();
		
		FieldObject<Double> fld=clsGenerator.objectFactory.fields.createField("instanceField1", double.class);
		cls.getFields().add(fld);

		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setReturnType(double.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar2", float.class, 75.9f));
		block.add(clsGenerator.lineFactory.variables.setVariableValueFromVariable("instanceField1", "localVar2"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceField1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Double);
		assertTrue(Math.abs(75.9d-((Double)result).doubleValue())<0.0001d); //difference less than 0.001, due to float point precision
	}
	
	@Test
	public void testSetVariableWithObjectCasting() throws Exception
	{
		ClassObject cls=createClass();
		
		FieldObject<Object> fld=clsGenerator.objectFactory.fields.createField("instanceField1", Object.class);
		cls.getFields().add(fld);

		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setReturnType(Object.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar2", Object.class, "TEST"));
		block.add(clsGenerator.lineFactory.variables.setVariableValueFromVariable("instanceField1", "localVar2"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceField1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Object);
		assertTrue(result instanceof String);
		assertEquals("TEST", result);
	}
	
	@Test(expected=ClassCastException.class)
	public void testSetVariableWithObjectIllegalCasting() throws Throwable
	{
		ClassObject cls=createClass();
		
		FieldObject<Integer> fld=clsGenerator.objectFactory.fields.createField("instanceField1", Integer.class);
		cls.getFields().add(fld);

		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setReturnType(Integer.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar2", String.class, "TEST"));
		block.add(clsGenerator.lineFactory.variables.setVariableValueFromVariable("instanceField1", "localVar2"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceField1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		try
		{
			mth.invoke(obj, (Object[])null);
		}catch(InvocationTargetException ex)
		{
			throw ex.getCause();
		}
		
		fail("A exception should have been thrown!");
	}
}
