package test.progenies.ecg.asm31.methods.receive;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.junit.Test;

import test.progenies.ecg.asm31.methods.BaseTestInternalStaticMethods;

import com.progenies.ecg.model.ClassObject;
import com.progenies.ecg.model.MethodObject;
import com.progenies.ecg.model.ParameterObject;
import com.progenies.ecg.model.codeparts.CodeBlock;

/**
 * Test method invocation, being the methods static but from the same class class
 * @author ofc587a87
 *
 */
public class TestInternalStaticMethods extends BaseTestInternalStaticMethods {

	
	
	
	
	
	/* --------------------------------------------- */
	/* --    Receipt Tests    ----------------------- */
	/* --------------------------------------------- */
	
	@Test
	public void testReceiveVoid() throws Exception
	{
		ClassObject cls=createClass();
		
		//create method
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setCode(clsGenerator.lineFactory.methods.invokeStaticMethod("testVoid", null, null, null));
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		MethodClass obj=(MethodClass) clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNull(result);
		assertEquals(1, MethodClass.methodCheck());
	}
	
	@Test
	public void testReceivePrimitiveSize1() throws Exception
	{
		ClassObject cls=createClass();
		
		//create method
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("value", int.class));
		block.add(clsGenerator.lineFactory.methods.invokeStaticMethodAndStore("testInt", null, null, int.class, "value"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("value"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		MethodClass obj=(MethodClass) clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(1, MethodClass.methodCheck());
		assertEquals(5, result);
	}
	
	
	@Test
	public void testReceivePrimitiveSize2() throws Exception
	{
		ClassObject cls=createClass();
		
		//create method
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("value", int.class));
		block.add(clsGenerator.lineFactory.methods.invokeStaticMethodAndStore("testDouble", null, null, double.class, "value"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("value"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		MethodClass obj=(MethodClass) clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(1, MethodClass.methodCheck());
		assertEquals(7, result);
	}
	
	
	@Test
	public void testReceivePrimitiveWithConversion() throws Exception
	{
		ClassObject cls=createClass();
		
		//create method
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setReturnType(int.class);
		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("valueDouble", double.class));
		block.add(clsGenerator.lineFactory.methods.invokeStaticMethodAndStore("testDouble", null, null, double.class, "valueDouble"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("valueDouble"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		MethodClass obj=(MethodClass) clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(7, result);
	}
	
	@Test
	public void testReceiveObject() throws Exception
	{
		ClassObject cls=createClass();
		
		//create method
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("tmpObj", ValueObject.class));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("value", int.class));
		block.add(clsGenerator.lineFactory.methods.invokeStaticMethodAndStore("testValueObject", null, new ParameterObject<?>[] {new ParameterObject<Integer>("param1", int.class, 8)}, ValueObject.class, "tmpObj"));
		block.add(clsGenerator.lineFactory.methods.invokeMethodAndStore("getInternalValue", "tmpObj", null, int.class, "value"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("value"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		MethodClass obj=(MethodClass) clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(0, MethodClass.methodCheck());
		assertEquals(8, result);
	}
	
	
	@Test(expected=NullPointerException.class)
	public void testReceiveNullObject() throws Throwable
	{
		ClassObject cls=createClass();
		
		//create method
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("tmpObj", ValueObject.class));
		block.add(clsGenerator.lineFactory.methods.invokeStaticMethodAndStore("testValueObject", null, new ParameterObject<?>[] {new ParameterObject<Integer>("param1", int.class, -8)}, ValueObject.class, "tmpObj"));
		block.add(clsGenerator.lineFactory.methods.invokeMethodAndStore("getInternalValue", "tmpObj", null, int.class, "value"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		MethodClass obj=(MethodClass) clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		try
		{
			mth.invoke(obj, (Object[])null);
			fail("Should has been thrown an exception!");
		}catch(InvocationTargetException ex)
		{
			throw ex.getCause();
		}
	}
}
