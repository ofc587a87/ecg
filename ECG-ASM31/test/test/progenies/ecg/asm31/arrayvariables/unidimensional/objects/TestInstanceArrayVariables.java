package test.progenies.ecg.asm31.arrayvariables.unidimensional.objects;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;

import org.junit.Test;

import test.progenies.ecg.asm31.arrayvariables.unidimensional.BaseTestInstancelArrayVariables;

import com.progenies.ecg.model.ClassObject;
import com.progenies.ecg.model.FieldObject;
import com.progenies.ecg.model.MethodObject;
import com.progenies.ecg.model.ParameterObject;
import com.progenies.ecg.model.ValueReference;
import com.progenies.ecg.model.codeparts.CodeBlock;

public class TestInstanceArrayVariables extends BaseTestInstancelArrayVariables
{
	@Test
	public void testCreateNotInitializedArrayVariable() throws Exception
	{
		ClassObject cls=createClass();
		
		//an instance array will be default initialized to null
		FieldObject<MethodClass[]> fld0=clsGenerator.objectFactory.fields.createFieldArray("instanceArray", MethodClass[].class);
		cls.getFields().add(fld0);
				
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", MethodClass[].class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceArray"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNull(result);
	}
	
	@Test
	public void testCreateNullArrayVariable() throws Exception
	{
		ClassObject cls=createClass();
		
		FieldObject<MethodClass[]> fld0=clsGenerator.objectFactory.fields.createFieldArrayWithDefaultValue("instanceArray", MethodClass[].class, (MethodClass[])null);
		cls.getFields().add(fld0);
		
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", MethodClass[].class);
		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceArray"));
		method.setCode(block);
		cls.getMethods().add(method);

		Class<?> clsResult=clsGenerator.generateClass(cls);

		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNull(result);
	}
	
	@Test
	public void testCreateEmptyArrayVariable() throws Exception
	{
		ClassObject cls=createClass();
		
		cls.getFields().add(clsGenerator.objectFactory.fields.createFieldArray("instanceArray", MethodClass[].class, 5));
		
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", MethodClass[].class);
		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceArray"));
		method.setCode(block);
		cls.getMethods().add(method);

		Class<?> clsResult=clsGenerator.generateClass(cls);

		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertTrue(result instanceof MethodClass[]);
		assertArrayEquals(new MethodClass[5], (MethodClass[])result);
	}
	
	
	// Objects are not static values, so this test its not possible
//	@Test  
//	public void testCreateStaticValuesArrayVariable() throws Exception
//	{
//		ClassObject cls=createClass();
//		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", MethodClass[].class);
//		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
//		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar1", MethodClass[].class, new MethodClass[] {new MethodClass(1), new MethodClass(2), new MethodClass(3)}));
//		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localVar1"));
//		method.setCode(block);
//		cls.getMethods().add(method);
//		
//		Class<?> clsResult=clsGenerator.generateClass(cls);
//
//		Object obj=clsResult.newInstance();
//		Method mth=clsResult.getMethod("method1", (Class[])null);
//		Object result=mth.invoke(obj, (Object[])null);
//		assertNotNull(result);
//		assertTrue(result instanceof MethodClass[]);
//		assertArrayEquals(new MethodClass[] {new MethodClass(1), new MethodClass(2), new MethodClass(3)}, (MethodClass[])result);
//	}
	
	
	
	@Test
	public void testCreateMixedTypedArrayVariable() throws Exception
	{
		ClassObject cls=createClass();
		
		FieldObject<MethodClass> fld1=clsGenerator.objectFactory.fields.createField("instanceFieldVal1", MethodClass.class);
		FieldObject<ExtendedMethodClass> fld2=clsGenerator.objectFactory.fields.createField("instanceFieldVal2", ExtendedMethodClass.class);
		fld1.setInitializationParams(new ParameterObject<?>[] {new ParameterObject<Integer>("p1", int.class, 2)});
		fld2.setInitializationParams(new ParameterObject<?>[] {new ParameterObject<Integer>("p1", int.class, 3)}); //TODO: Parameter conversion?
		cls.getFields().add(fld1);
		cls.getFields().add(fld2);
		
		cls.getFields().add(clsGenerator.objectFactory.fields.createFieldArray("instanceArray", MethodClass[].class, new ValueReference[] {
			ValueReference.forVariable("instanceFieldVal1"),
			ValueReference.forVariable("instanceFieldVal2"),
			ValueReference.forVariable("instanceObjVar"),
			ValueReference.forVariable("staticObjVar"),
			ValueReference.forFieldInVariable("instanceObjVarValue", "instanceObj"),
			ValueReference.forFieldInVariable("instanceObjVarValue", "staticObj"),
			ValueReference.forStaticFieldInClass("staticObjVarValue", TestObj.class)
			}));
		
		
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", MethodClass[].class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceArray"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);

		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertTrue(result instanceof MethodClass[]);
		assertArrayEquals(new MethodClass[] {new MethodClass(2), new ExtendedMethodClass(3), new MethodClass(4), new MethodClass(6), new MethodClass(50), new MethodClass(50), new MethodClass(100)}, (MethodClass[])result);
	}
	
	
	
	
	
	/*--------------------------------------------
	 *----------- From method invocation --------- 
	 *--------------------------------------------
	 */
	
	
	@Test
	public void testCreateArrayFromLocalMethod() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", MethodClass[].class);
		
		cls.getFields().add(clsGenerator.objectFactory.fields.createFieldArray("instanceArray", MethodClass[].class));
		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.methods.invokeMethodAndStore("createObjMArray", "this", null, MethodClass[].class, "instanceArray"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceArray"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);

		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertTrue(result instanceof MethodClass[]);
		assertArrayEquals(new MethodClass[] {new MethodClass(2), new MethodClass(4), new MethodClass(6)}, (MethodClass[])result);
	}
	
	
	
	@Test
	public void testCreateArrayFromExternalInstanceMethod() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", MethodClass[].class);
		
		cls.getFields().add(clsGenerator.objectFactory.fields.createFieldArray("instanceArray", MethodClass[].class));
		
		FieldObject<MethodClass> fld=clsGenerator.objectFactory.fields.createField("localObj", MethodClass.class);
		fld.setInitialize(true);
		cls.getFields().add(fld);
		
		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.methods.invokeMethodAndStore("createObjMArray", "localObj", null, MethodClass[].class, "instanceArray"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceArray"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);

		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertTrue(result instanceof MethodClass[]);
		assertArrayEquals(new MethodClass[] {new MethodClass(2), new MethodClass(4), new MethodClass(6)}, (MethodClass[])result);
	}
	
	
	
	@Test
	public void testCreateArrayFromStaticMethod() throws Exception
	{
		ClassObject cls=createClass();
		
		cls.getFields().add(clsGenerator.objectFactory.fields.createFieldArray("instanceArray", MethodClass[].class));
		
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", MethodClass[].class);
		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.methods.invokeStaticMethodAndStore("createStaticObjMArray", MethodClass.class, null, MethodClass[].class, "instanceArray"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceArray"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);

		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertTrue(result instanceof MethodClass[]);
		assertArrayEquals(new MethodClass[] {new MethodClass(2), new MethodClass(4), new MethodClass(6), new MethodClass(8)}, (MethodClass[])result);
	}

	
	
}
