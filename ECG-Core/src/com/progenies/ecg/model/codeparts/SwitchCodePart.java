package com.progenies.ecg.model.codeparts;

import java.util.Collections;
import java.util.List;

import com.progenies.ecg.model.AbstractCodeObject;
import com.progenies.ecg.model.ValueReference;

public abstract class SwitchCodePart extends AbstractCodeObject  implements ICodePart
{
	
	protected CodeBlock defaultCodeBlock;
	protected List<CaseBlock> caseBlocks;
	protected ValueReference<?> evaluatedValue;
	
	
	
	public ValueReference<?> getEvaluatedValue() {
		return evaluatedValue;
	}

	public CodeBlock getDefaultCodeBlock() {
		return defaultCodeBlock;
	}

	public List<CaseBlock> getCaseBlocks() {
		return Collections.unmodifiableList(caseBlocks);
	}

	
	
	


}
