package com.progenies.ecg.asm31.model.factory;

import com.progenies.ecg.asm31.model.codeparts.DoWhileLoopCodePartASM31;
import com.progenies.ecg.asm31.model.codeparts.ForLoopCodePartASM31;
import com.progenies.ecg.asm31.model.codeparts.WhileLoopCodePartASM31;
import com.progenies.ecg.asm31.model.codeparts.lines.BreakContinueLinePartASM31;
import com.progenies.ecg.model.ValueReference;
import com.progenies.ecg.model.codeparts.CodeBlock;
import com.progenies.ecg.model.codeparts.DoWhileLoopCodePart;
import com.progenies.ecg.model.codeparts.ForLoopCodePart;
import com.progenies.ecg.model.codeparts.WhileLoopCodePart;
import com.progenies.ecg.model.codeparts.lines.LineCodePart;
import com.progenies.ecg.model.conditions.ICondition;
import com.progenies.ecg.model.factory.IObjectLoopFactory;

public class ObjectLoopsFactoryASM31 implements IObjectLoopFactory {

	@Override
	public ForLoopCodePart createForLoop(LineCodePart initialCode, ICondition condition, LineCodePart postCode, CodeBlock codeBlock) {
		return new ForLoopCodePartASM31(initialCode, condition, postCode, codeBlock);
	}

	@Override
	public ForLoopCodePart createForEachLoop(Class<?> variableType, String variableName, ValueReference<? extends Object> iterable, CodeBlock codeBlock) {
		return new ForLoopCodePartASM31(variableType, variableName, iterable, codeBlock);
	}

	@Override
	public WhileLoopCodePart createWhileLoop(ICondition condition, CodeBlock codeBlock) {
		return new WhileLoopCodePartASM31(condition, codeBlock);
	}

	@Override
	public DoWhileLoopCodePart createDoWhileLoop(ICondition condition, CodeBlock codeBlock) {
		return new DoWhileLoopCodePartASM31(condition, codeBlock);
	}

	@Override
	public LineCodePart createBreak() {
		return BreakContinueLinePartASM31.createBreak();
	}

	@Override
	public LineCodePart createContinue() {
		return BreakContinueLinePartASM31.createContinue();
	}

}
