package test.progenies.ecg.asm31.methods.exceptions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.objectweb.asm.Opcodes;

import test.progenies.ecg.asm31.methods.BaseTestExternalStaticMethods;

import com.progenies.ecg.model.ClassObject;
import com.progenies.ecg.model.MethodObject;
import com.progenies.ecg.model.ParameterObject;

/**
 * Test method invocation, being the methods static from an external class
 * @author ofc587a87
 *
 */
public class TestExternalStaticMethods extends BaseTestExternalStaticMethods {


	/* --------------------------------------------- */
	/* --    Exception Tests    -------------------- */
	/* --------------------------------------------- */
	
	
	@Test
	public void testCheckedExceptionNotThrown() throws Exception
	{
		
		ClassObject cls=createClass();
		
		//create method
		List<Class<? extends Throwable>> exceptions=new ArrayList<Class<? extends Throwable>>();
		exceptions.add(ClassNotFoundException.class);
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", (ParameterObject[])null, exceptions);
		method.setModifiers(new int[] {Opcodes.ACC_STATIC, Opcodes.ACC_PUBLIC});
		method.setCode(clsGenerator.lineFactory.methods.invokeStaticMethod("testVoid", MethodClass.class, null, null));
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(clsResult.newInstance(), (Object[])null);
		assertNull(result);
		assertEquals(1, MethodClass.methodCheck());
	}
	
	@Test
	public void testCheckedExceptionThrown() throws Exception
	{
		ClassObject cls=createClass();
		
		//create method
		List<Class<? extends Throwable>> exceptions=new ArrayList<Class<? extends Throwable>>();
		exceptions.add(ClassNotFoundException.class);
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", (ParameterObject[])null, exceptions);
		method.setModifiers(new int[] {Opcodes.ACC_STATIC, Opcodes.ACC_PUBLIC});
		method.setCode(clsGenerator.lineFactory.methods.throwNewException(ClassNotFoundException.class, "Class not found in there"));
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Method mth=clsResult.getMethod("method1", (Class[])null);
		
		Object result=null;
		try
		{
			result=mth.invoke(clsResult.newInstance(), (Object[])null);
			result.toString();
		}catch(Exception ex)
		{
			if(ex.getCause()==null || !ex.getCause().getMessage().equals("Class not found in there"))
				throw ex;
		}
		assertNull(result);
		
		
	}
	
	@Test
	public void testUncheckedExceptionThrown() throws Exception
	{
		ClassObject cls=createClass();
		
		//create method
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", (ParameterObject[])null);
		method.setModifiers(new int[] {Opcodes.ACC_STATIC, Opcodes.ACC_PUBLIC});
		method.setCode(clsGenerator.lineFactory.methods.throwNewException(RuntimeException.class, "Class not found in there"));
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Method mth=clsResult.getMethod("method1", (Class[])null);
		
		Object result=null;
		try
		{
			result=mth.invoke(clsResult.newInstance(), (Object[])null);
			result.toString();
		}catch(Exception ex)
		{
			if(ex.getCause()==null || !ex.getCause().getMessage().equals("Class not found in there"))
				throw ex;
		}
		assertNull(result);
		
	}
	
	//TODO: Exception thrown inside the method
}
