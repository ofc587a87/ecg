package test.progenies.ecg.asm31.variables.creation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.lang.reflect.Method;

import org.junit.Test;

import test.progenies.ecg.asm31.variables.BaseTestLocalVariables;

import com.progenies.ecg.model.ClassObject;
import com.progenies.ecg.model.MethodObject;
import com.progenies.ecg.model.codeparts.CodeBlock;

public class TestLocalVariables extends BaseTestLocalVariables {

	
	
	@Test(expected=RuntimeException.class)
	public void testCreateEmptyVariable() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setReturnType(int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar1", int.class));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localVar1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=null;
		try
		{
			//TODO: now ASM is throwing an exception, i should be able to know in an internal analysis if the variable has been initialized before load into stack
			clsResult=clsGenerator.generateClass(cls);
			clsResult.newInstance();
		}catch(Exception ex)
		{
			
			if(ex.getMessage().startsWith("Error at instruction 2: Expected I, but found ."))
				throw ex;

		}
		
		fail("An exception shold have been thrown!!");
	}
	
	@Test
	public void testCreateVariableWithPrimitiveValue() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setReturnType(int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar1", int.class, 5));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localVar1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Integer);
		assertEquals(5, result);
	}
	
	@Test
	public void testCreateVariableWithObjectValue() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setReturnType(String.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar1", String.class, "CHECKING"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localVar1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertTrue(result instanceof String);
		assertEquals("CHECKING", result);
	}
	
	@Test
	public void testCreateWithNullObjectValue() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setReturnType(String.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar1", String.class, (String)null));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localVar1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNull(result);
	}
	
	
	@Test
	public void testCreatePrimitiveWithNullObjectValue() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setReturnType(int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar1", int.class, (Integer)null));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localVar1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		try
		{
			clsGenerator.generateClass(cls);
			fail("Error should had been thrown");
		}
		catch(RuntimeException ex)
		{
			assertEquals("The variable type is primitive or array, can't accept null values", ex.getMessage());
		}
	}
	
	@Test
	public void testSetPrimitiveWithNullObjectValue() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setReturnType(int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar1", int.class, 1));
		block.add(clsGenerator.lineFactory.variables.setVariableValueNull("localVar1"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localVar1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		try
		{
			clsGenerator.generateClass(cls);
			fail("Error should had been thrown");
		}
		catch(RuntimeException ex)
		{
			assertEquals("The variable type is primitive or array, can't accept null values", ex.getMessage());
		}
	}

	@Test
	public void testCreateAndInitializePrimitive() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setReturnType(int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariableAndInitialize("localVar1", int.class));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localVar1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		
		try
		{
			clsGenerator.generateClass(cls); //exception thrown!!

			fail("An exceptions should have been thrown!!");
		}catch(Exception ex)
		{
			assertTrue(ex instanceof RuntimeException);
			assertEquals("Type int can't be initialized", ex.getMessage());
		}
		
	}
	
	
	@Test
	public void testCreateAndInitializeObject() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setReturnType(String.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariableAndInitialize("localVar1", String.class));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localVar1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertTrue(result instanceof String);
		assertEquals("", result);
	}
		
	
	

}
