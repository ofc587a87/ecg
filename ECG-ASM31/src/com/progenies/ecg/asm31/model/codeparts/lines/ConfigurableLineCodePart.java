package com.progenies.ecg.asm31.model.codeparts.lines;

import org.objectweb.asm.MethodVisitor;

import com.progenies.ecg.asm31.model.codeparts.IConfigurableCodePart;
import com.progenies.ecg.asm31.util.VariableRegister;
import com.progenies.ecg.model.AbstractCodeObject;
import com.progenies.ecg.model.codeparts.lines.LineCodePart;

public abstract class ConfigurableLineCodePart extends LineCodePart implements IConfigurableCodePart {

	protected MethodVisitor visitor;
	protected AbstractCodeObject parent;
	protected VariableRegister varRegister;

	@Override
	public void configure(MethodVisitor visitor, AbstractCodeObject parent, VariableRegister varRegister)
	{
		this.visitor=visitor;
		this.parent=parent;
		this.varRegister=varRegister;
	}
	
	
	

	public MethodVisitor getVisitor() {
		return visitor;
	}

	public AbstractCodeObject getParent() {
		return parent;
	}


	public VariableRegister getVarRegister() {
		return varRegister;
	}
	
	@Override
	public boolean isReturnCodePart() {
		return false; //false by default
	}



}
