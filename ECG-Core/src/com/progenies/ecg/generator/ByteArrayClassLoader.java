package com.progenies.ecg.generator;

//TODO: Class reload (if possible, it may need a ClassLoader per class?)
public class ByteArrayClassLoader extends ClassLoader
{
	public ByteArrayClassLoader(ClassLoader parent)
	{
		super(parent);
	}
	
	public Class<?> loadByteArray(byte[] b) throws ClassFormatError
	{
		return super.defineClass(null, b, 0, b.length);
	}
}
