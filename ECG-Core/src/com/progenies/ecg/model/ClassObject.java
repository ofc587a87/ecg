package com.progenies.ecg.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Define a Class Object. Can be also used for internal classes. 
 * @author ofc587a87
 *
 */
public abstract class ClassObject extends AbstractCodeObject
{
	/** SuperClass of the current one. Cannot be null, being java.lang.Object by default */
	protected Class<?> superClass=java.lang.Object.class;
	
	/** Interfaces implemented by the current Class. It can be null or an empty array. */
	protected Class<?> interfaces[];
	
	//TODO: Implement StaticInitializerCodePart
	/** Static initializer block */
//	protected StaticInitializerCodePart staticInitializer;
	
	/** Field of the current Class. It can be null or an empty Array */
	protected List<FieldObject<?>> fields;
	
	/** Methods of the current Class. It can be null or an empty Array */
	protected List<MethodObject> methods;
	
	/** Constructors of the current class. If it's null or an empty array, a default constructor will be created */
	protected List<ConstructorObject> constructors;
	
	
	public ClassObject()
	{
		this.fields=new ArrayList<FieldObject<?>>();
		this.methods=new ArrayList<MethodObject>();
		this.constructors=new ArrayList<ConstructorObject>();
	}
	
	

	public Class<?> getSuperClass() {
		return superClass;
	}

	public void setSuperClass(Class<?> superClass) {
		this.superClass = superClass;
	}

	public Class<?>[] getInterfaces() {
		return interfaces==null?null: Arrays.copyOf(interfaces, interfaces.length);
	}

	public void setInterfaces(Class<?>[] interfaces) {
		this.interfaces = interfaces==null?null:Arrays.copyOf(interfaces, interfaces.length);
	}

	public List<FieldObject<?>> getFields() {
		return fields;
	}

	public List<MethodObject> getMethods() {
		return methods;
	}

	public List<ConstructorObject> getConstructors() {
		return constructors;
	}
	
}
