package test.progenies.ecg.asm31.methods.invoke;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.junit.Test;

import test.progenies.ecg.asm31.methods.BaseTestExternalFieldMethods;

import com.progenies.ecg.model.ClassObject;
import com.progenies.ecg.model.MethodObject;
import com.progenies.ecg.model.ParameterObject;
import com.progenies.ecg.model.codeparts.CodeBlock;

/**
 * Test method invocation, being the methods external to the class, from a field object
 * @author ofc587a87
 *
 */
public class TestExternalFieldMethods extends BaseTestExternalFieldMethods {

	
	
	/* --------------------------------------------- */
	/* --    Invoke Tests    ----------------------- */
	/* --------------------------------------------- */
	
	@Test
	public void testInvokeWithoutParameters() throws Exception
	{
		ClassObject cls=createClass();
		
		//create method
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setCode(clsGenerator.lineFactory.methods.invokeMethod("testVoid", "fieldObj", null, null));
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		MethodClass obj=new MethodClass();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Constructor<?> ctr=clsResult.getConstructor(new Class[] {MethodClass.class});
		Object result=mth.invoke(ctr.newInstance(new Object[] {obj}), (Object[])null);
		assertNull(result);
		assertEquals(1, obj.methodCheck());
	}
	
	@Test
	public void testInvokeWithPrimitiveParameters() throws Exception
	{
		ClassObject cls=createClass();
		
		//create method
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", new ParameterObject<?>[]{new ParameterObject<Integer>("param1", int.class)});
		method.setCode(clsGenerator.lineFactory.methods.invokeMethod("testValueObject", "fieldObj", new ParameterObject<?>[]{new ParameterObject<Integer>("param1", "param1", int.class)}, ValueObject.class));
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		MethodClass obj=new MethodClass();
		Method mth=clsResult.getMethod("method1", new Class[]{int.class});
		Constructor<?> ctr=clsResult.getConstructor(new Class[] {MethodClass.class});
		Object result=mth.invoke(ctr.newInstance(new Object[] {obj}), new Object[] {63});
		assertNull(result);
		assertEquals(0, obj.methodCheck());
	}
	
	@Test
	public void testInvokeWithObjectParameters() throws Exception
	{
		ClassObject cls=createClass();
		
		//create method
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", new ParameterObject<?>[]{new ParameterObject<ValueObject>("param1", ValueObject.class)});
		method.setCode(clsGenerator.lineFactory.methods.invokeMethod("setValueObject", "fieldObj", new ParameterObject<?>[]{new ParameterObject<ValueObject>("param1", "param1", ValueObject.class)}, null));
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		MethodClass obj=new MethodClass();
		Method mth=clsResult.getMethod("method1", new Class[]{ValueObject.class});
		Constructor<?> ctr=clsResult.getConstructor(new Class[] {MethodClass.class});
		Object result=mth.invoke(ctr.newInstance(new Object[] {obj}), new Object[] {new ValueObject(43)});
		assertNull(result);
		assertEquals(43, obj.methodCheck());
	}
	
	
	@Test
	public void testInvokeWithPrimitiveArrayParameter() throws Exception
	{
		ClassObject cls=createClass();
		
		//create method
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", new ParameterObject<?>[]{new ParameterObject<int[]>("param1", int[].class)}, int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("tmpVar", ValueObject.class));
		block.add(clsGenerator.lineFactory.methods.invokeMethodAndStore("testValueObject", "fieldObj", new ParameterObject<?>[]{new ParameterObject<int[]>("param1", "param1", int[].class)}, ValueObject.class, "tmpVar"));
		block.add(clsGenerator.lineFactory.methods.invokeMethodAndReturn("getInternalValue", "tmpVar", null, int.class));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		MethodClass obj=new MethodClass();
		Method mth=clsResult.getMethod("method1", new Class[]{int[].class});
		Constructor<?> ctr=clsResult.getConstructor(new Class[] {MethodClass.class});
		Object result=mth.invoke(ctr.newInstance(new Object[] {obj}), new Object[] {new int[] {63, 5, 76}});
		assertNotNull(result);
		assertEquals((63+5+76), ((Integer)result).intValue());
		assertEquals(0, obj.methodCheck());
	}
	
	@Test
	public void testInvokeWithObjectArrayParameter() throws Exception
	{
		ClassObject cls=createClass();
		
		//create method
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", new ParameterObject<?>[]{new ParameterObject<ValueObject[]>("param1", ValueObject[].class)}, int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("tmpVar", ValueObject.class));
		block.add(clsGenerator.lineFactory.methods.invokeMethodAndStore("testValueObject", "fieldObj", new ParameterObject<?>[]{new ParameterObject<ValueObject[]>("param1", "param1", ValueObject[].class)}, ValueObject.class, "tmpVar"));
		block.add(clsGenerator.lineFactory.methods.invokeMethodAndReturn("getInternalValue", "tmpVar", null, int.class));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		MethodClass obj=new MethodClass();
		Method mth=clsResult.getMethod("method1", new Class[]{ValueObject[].class});
		Constructor<?> ctr=clsResult.getConstructor(new Class[] {MethodClass.class});
		Object result=mth.invoke(ctr.newInstance(new Object[] {obj}), new Object[] {new ValueObject[] {new ValueObject(64), new ValueObject(5), new ValueObject(77)}});
		assertNotNull(result);
		assertEquals((64+5+77), ((Integer)result).intValue());
		assertEquals(0, obj.methodCheck());
	}
	
	
	@Test(expected=NullPointerException.class)
	public void testInvokeWithNullParameter() throws Throwable
	{
		
		ClassObject cls=createClass();
		
		//create method
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("tmpObj", ValueObject.class));
		block.add(clsGenerator.lineFactory.methods.invokeMethodAndStore("testValueObject", "fieldObj", new ParameterObject<?>[]{new ParameterObject<Integer>("param1", int.class, -1)}, ValueObject.class, "tmpObj"));
		block.add(clsGenerator.lineFactory.methods.invokeMethodAndStore("getInternalValue", "tmpObj", null, int.class, "value"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		MethodClass obj=new MethodClass();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Constructor<?> ctr=clsResult.getConstructor(new Class[] {MethodClass.class});
		
		try
		{
			Object result=mth.invoke(ctr.newInstance(new Object[] {obj}), (Object[])null);
			result.hashCode();
			fail("Should have been thrown a NullPointerException");
		}catch(InvocationTargetException ex)
		{
			throw ex.getCause();
		}
	}
}
