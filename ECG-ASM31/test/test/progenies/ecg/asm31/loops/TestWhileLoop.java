package test.progenies.ecg.asm31.loops;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.lang.reflect.Method;

import org.junit.Before;
import org.junit.Test;

import com.progenies.ecg.generator.ClassGenerator;
import com.progenies.ecg.generator.ClassGeneratorFactory;
import com.progenies.ecg.model.ClassObject;
import com.progenies.ecg.model.ConstructorObject;
import com.progenies.ecg.model.MethodObject;
import com.progenies.ecg.model.ValueReference;
import com.progenies.ecg.model.codeparts.CodeBlock;
import com.progenies.ecg.model.codeparts.IfCodePart;
import com.progenies.ecg.model.codeparts.WhileLoopCodePart;
import com.progenies.ecg.model.codeparts.lines.LineFactory;
import com.progenies.ecg.model.conditions.BooleanCondition;
import com.progenies.ecg.model.conditions.ICondition;

public class TestWhileLoop {
	protected ClassGenerator clsGenerator;
	private static int counter = 0;
	
	@Test
	public void testSimpleIteration() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("i", int.class, 0));

		ICondition condition=new BooleanCondition<Object>(ValueReference.forVariable("i"), ValueReference.forStaticValue(5), BooleanCondition.ConditionType.IS_LESS_THAN);
		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue"));
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("i"));
		
		WhileLoopCodePart loopCode=clsGenerator.objectFactory.loops.createWhileLoop(condition, bodyBlock);
		block.add(loopCode);
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(2 + 5, result);
	}

	@Test
	public void testNoIteration() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));

		ICondition condition=new BooleanCondition<Boolean>(ValueReference.forStaticValue(Boolean.FALSE), BooleanCondition.ConditionType.IS_TRUE);
		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue"));
		
		WhileLoopCodePart loopCode=clsGenerator.objectFactory.loops.createWhileLoop(condition, bodyBlock);
		block.add(loopCode);
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(2, result);
	}
	
	
	@Test
	public void testBreak_NoConditionIteration() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("i", int.class, 0));

		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue"));
		
		ICondition condition=new BooleanCondition<Object>(ValueReference.forVariable("i"), ValueReference.forStaticValue(4), BooleanCondition.ConditionType.IS_GREATER_OR_EQUALS_TO);
		CodeBlock ifBodyBlock=clsGenerator.lineFactory.createCodeBlock();
		ifBodyBlock.add(clsGenerator.objectFactory.loops.createBreak());
		IfCodePart ifCode=clsGenerator.objectFactory.conditionals.createIfBlock(condition, ifBodyBlock);
		bodyBlock.add(ifCode);
		
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("i"));
		
		WhileLoopCodePart loopCode=clsGenerator.objectFactory.loops.createWhileLoop(null, bodyBlock);
		block.add(loopCode);
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		
		
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(2 + 5, result);
	}
	
	
	@Test
	public void testBreakContinue_NoConditionIteration() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("i", int.class, 0));

		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		
		
		ICondition condition=new BooleanCondition<Object>(ValueReference.forVariable("i"), ValueReference.forStaticValue(4), BooleanCondition.ConditionType.IS_GREATER_OR_EQUALS_TO);
		CodeBlock ifBodyBlock=clsGenerator.lineFactory.createCodeBlock();
		ifBodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 10));
		ifBodyBlock.add(clsGenerator.objectFactory.loops.createBreak());
		
		CodeBlock elseBodyBlock=clsGenerator.lineFactory.createCodeBlock();
		elseBodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue"));
		elseBodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("i"));
		elseBodyBlock.add(clsGenerator.objectFactory.loops.createContinue());
		
		IfCodePart ifCode=clsGenerator.objectFactory.conditionals.createIfElseBlock(condition, ifBodyBlock, elseBodyBlock);
		bodyBlock.add(ifCode);
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 50)); //dead code (if break/continue works well)
		
		
		WhileLoopCodePart loopCode=clsGenerator.objectFactory.loops.createWhileLoop(null, bodyBlock);
		block.add(loopCode);
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		
		
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(2 + 4 + 10, result);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	
	
	
	@Before
	public void setUp() throws Exception {
		clsGenerator=ClassGeneratorFactory.getClassGenerator();
		assertNotNull(clsGenerator);
	}

	
	
	

	protected ClassObject createClass()
	{
		counter++;
		int index=counter;
		LineFactory lineFactory=clsGenerator.lineFactory;
		
		String subpackage=this.getClass().getPackage().getName().substring(this.getClass().getPackage().getName().lastIndexOf('.')+1);
		ClassObject cls=clsGenerator.objectFactory.classes.createClass("test.loops."+subpackage+".WhileTestClass"+index);
		cls.setSuperClass(Object.class);

		
		//override default constructor
		ConstructorObject consObject=clsGenerator.objectFactory.constructors.createConstructor();
		consObject.setCode(lineFactory.methods.invokeSuperConstructor());
		cls.getConstructors().add(consObject);
		
		
		return cls;
		
	}

}
