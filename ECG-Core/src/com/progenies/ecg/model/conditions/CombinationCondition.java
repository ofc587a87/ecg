package com.progenies.ecg.model.conditions;


public class CombinationCondition implements ICondition
{
	
	public static enum CombinationConditionType {	AND, OR; }
	
	protected ICondition conditionCodeLeft;
	protected ICondition conditionCodeRight;
	protected CombinationConditionType conditionType;
	
	
	public CombinationCondition(ICondition left, ICondition right, CombinationConditionType conditionType)
	{
		this.conditionCodeLeft=left;
		this.conditionCodeRight=right;
		this.conditionType=conditionType;
	}
	
	
	

	public CombinationConditionType getConditionType() {
		return conditionType;
	}

	public void setConditionType(CombinationConditionType conditionType) {
		this.conditionType = conditionType;
	}

	public ICondition getConditionCodeLeft() {
		return conditionCodeLeft;
	}

	public void setConditionCodeLeft(ICondition conditionCodeLeft) {
		this.conditionCodeLeft = conditionCodeLeft;
	}



	public ICondition getConditionCodeRight() {
		return conditionCodeRight;
	}

	public void setConditionCodeRight(ICondition conditionCodeRight) {
		this.conditionCodeRight = conditionCodeRight;
	}

}
