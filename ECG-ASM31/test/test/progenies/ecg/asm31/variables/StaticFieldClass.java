package test.progenies.ecg.asm31.variables;

public final class StaticFieldClass
{
	public final long longField=6l;
	
	public static final long staticLongField=7l;
}