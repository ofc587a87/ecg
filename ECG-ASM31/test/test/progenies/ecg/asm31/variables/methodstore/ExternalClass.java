package test.progenies.ecg.asm31.variables.methodstore;

public class ExternalClass
{
	
	public int externalMethodInt()
	{
		return 9;
	}

	public long externalMethodLong()
	{
		return 9l;
	}
	
	public static int externalStaticMethodInt()
	{
		return 19;
	}

	public static long externalStaticMethodLong()
	{
		return 19l;
	}

}
