package com.progenies.ecg.model.codeparts;

public final class CaseBlock
{
	
	protected int key;
	protected CodeBlock bodyBlock;
	
	
	
	
	
	public CaseBlock(int key, CodeBlock bodyBlock)
	{
		this.key=key;
		this.bodyBlock=bodyBlock;
	}
	
	
	
	public int getKey() {
		return key;
	}
	public void setKey(int key) {
		this.key = key;
	}
	public CodeBlock getBodyBlock() {
		return bodyBlock;
	}
	public void setBodyBlock(CodeBlock bodyBlock) {
		this.bodyBlock = bodyBlock;
	} 


	

}
