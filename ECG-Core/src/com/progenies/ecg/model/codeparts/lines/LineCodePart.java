package com.progenies.ecg.model.codeparts.lines;

import com.progenies.ecg.model.codeparts.ICodePart;

/**
 * Abstract class that represents an independent line of code.
 * It may generate several bytecode instructions internally, but it's not related to other lines.
 * It can be composed of several instructions (i++,j++) 
 * 
 *  
 * @author ofc587a87
 *
 */
public abstract class LineCodePart implements ICodePart
{

}
