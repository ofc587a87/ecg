package test.progenies.ecg.asm31.methods.returns;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;

import org.junit.Test;

import test.progenies.ecg.asm31.methods.BaseTestInternalStaticMethods;

import com.progenies.ecg.model.ClassObject;
import com.progenies.ecg.model.MethodObject;
import com.progenies.ecg.model.ParameterObject;
import com.progenies.ecg.model.codeparts.CodeBlock;

/**
 * Test method invocation, being the methods static but from the same class class.
 * 
 * All tests should be developed as "Test of creation of a method that internally invokes a static method from the same class that returns XXXX"
 * @author ofc587a87
 *
 */
public class TestInternalStaticMethods extends BaseTestInternalStaticMethods {


	@Test
	public void testReturnVoid() throws Exception
	{
		ClassObject cls=createClass();
		
		//create method
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.methods.invokeStaticMethod("testVoid", null, null, null)); //invokes a local method that returns void
		block.add(clsGenerator.lineFactory.methods.returnEmpty());
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		MethodClass obj=(MethodClass) clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNull(result);
	}
	
	@Test
	public void testReturnPrimitiveSimpleWord() throws Exception
	{
		ClassObject cls=createClass();
		
		//create method
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("value", int.class));
		block.add(clsGenerator.lineFactory.methods.invokeStaticMethodAndStore("testInt", null, null, int.class, "value")); //invokes a local method that returns int
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("value"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		MethodClass obj=(MethodClass) clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertTrue(result instanceof Integer);
		assertEquals(5, result);
	}
	
	
	@Test
	public void testReturnPrimitiveDoubleWord() throws Exception
	{
		ClassObject cls=createClass();
		
		//create method
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", double.class);
		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("valueDouble", double.class));
		block.add(clsGenerator.lineFactory.methods.invokeStaticMethodAndStore("testDouble", null, null, double.class, "valueDouble")); //invokes a local method that returns double
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("valueDouble"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		MethodClass obj=(MethodClass) clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertTrue(result instanceof Double);
		assertEquals(7d, result);
	}
	
	
	
	@Test
	public void testReturnPrimitiveWithConversion() throws Exception
	{
		ClassObject cls=createClass();
		
		//create method
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("value", int.class));
		block.add(clsGenerator.lineFactory.methods.invokeStaticMethodAndStore("testDouble", null, null, double.class, "value")); //invokes a local method that returns double but is stored into an int
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("value"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		MethodClass obj=(MethodClass) clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertTrue(result instanceof Integer);
		assertEquals(7, result);
	}
	
	
	@Test
	public void testReturnObject() throws Exception
	{
		ClassObject cls=createClass();
		
		//create method
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", ValueObject.class);
		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("valueObj", ValueObject.class));
		block.add(clsGenerator.lineFactory.methods.invokeStaticMethodAndStore("testValueObject", null, new ParameterObject<?>[]{new ParameterObject<Integer>("param1", int.class, 9)}, ValueObject.class, "valueObj")); //invokes a local method that returns a non-null object
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("valueObj"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		MethodClass obj=(MethodClass) clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertTrue(result instanceof ValueObject);
		assertEquals(9, ((ValueObject)result).getInternalValue());
	}
	
	@Test
	public void testReturnAssignableObject() throws Exception
	{
		ClassObject cls=createClass();
		
		//create method
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", ValueObject.class);
		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("valueObj", ValueObject.class));
		block.add(clsGenerator.lineFactory.methods.invokeStaticMethodAndStore("testExtendedValueObject", null, new ParameterObject<?>[]{new ParameterObject<Integer>("param1", int.class, 9)}, ExtendedValueObject.class, "valueObj")); //invokes a local method that returns a non-null object
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("valueObj"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		MethodClass obj=(MethodClass) clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertTrue(result instanceof ValueObject);
		assertEquals(18, ((ValueObject)result).getInternalValue());
	}
	
	
	@Test
	public void testReturnInterfaceObject() throws Exception
	{
		ClassObject cls=createClass();
		
		//create method
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", ValueIface.class);
		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("valueObj", ValueObject.class));
		block.add(clsGenerator.lineFactory.methods.invokeStaticMethodAndStore("testIfaceValueObject", null, new ParameterObject<?>[]{new ParameterObject<Integer>("param1", int.class, 12)}, ValueIface.class, "valueObj")); //invokes a local method that returns a non-null object
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("valueObj"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		MethodClass obj=(MethodClass) clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertTrue(result instanceof ValueIface);
		assertEquals(12, ((ValueIface)result).getInternalValue());
	}
	
	@Test
	public void testReturnNullObject() throws Exception
	{
		ClassObject cls=createClass();
		
		//create method
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", ValueObject.class);
		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("valueObj", ValueObject.class));
		block.add(clsGenerator.lineFactory.methods.invokeStaticMethodAndStore("testValueObject", null, new ParameterObject<?>[]{new ParameterObject<Integer>("param1", int.class, -9)}, ValueObject.class, "valueObj")); //invokes a local method that returns a non-null object
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("valueObj"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		MethodClass obj=(MethodClass) clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNull(result);
	}
		
}
