package test.progenies.ecg.asm31.variables;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import test.progenies.ecg.asm31.variables.conversion.AllConversionVariablesTests;
import test.progenies.ecg.asm31.variables.creation.AllVariableCreationTests;
import test.progenies.ecg.asm31.variables.methodstore.AllVariableMethodStoreTests;
import test.progenies.ecg.asm31.variables.set.AllVariableSetTests;

@RunWith(Suite.class)
@SuiteClasses({ AllVariableCreationTests.class, AllVariableSetTests.class, AllVariableMethodStoreTests.class, AllConversionVariablesTests.class })
public class AllVariableTests
{

}
