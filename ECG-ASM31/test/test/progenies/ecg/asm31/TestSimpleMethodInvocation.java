package test.progenies.ecg.asm31;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

import com.progenies.ecg.generator.ClassGenerator;
import com.progenies.ecg.generator.ClassGeneratorFactory;
import com.progenies.ecg.model.ClassObject;
import com.progenies.ecg.model.ConstructorObject;
import com.progenies.ecg.model.MethodObject;
import com.progenies.ecg.model.ParameterObject;
import com.progenies.ecg.model.codeparts.CodeBlock;
import com.progenies.ecg.model.codeparts.lines.LineFactory;

public class TestSimpleMethodInvocation
{
	private ClassGenerator clsGenerator;
	private static int counter=0;
	
	@Before
	public void setUp() throws Exception {
		clsGenerator=ClassGeneratorFactory.getClassGenerator();
		assertNotNull(clsGenerator);
	}

	@Test
	public void testConstructor() throws Exception
	{
		MethodInterface iface=createClass().newInstance();
		assertNotNull(iface);
		assertEquals(0, iface.returnMethod());
	}
	
	@Test
	public void testConstructorWithParams() throws Exception
	{
		MethodInterface iface=createClass().getConstructor(new Class[] {int.class}).newInstance(new Object[] {5});
		assertNotNull(iface);
		assertEquals(5, iface.returnMethod());
	}
	
	
	@Test
	public void testMethodInvocation() throws Exception
	{
		MethodInterface iface=createClass().newInstance();
		assertNotNull(iface);
		
		iface.simpleMethod();
		iface.simpleMethod();
		iface.simpleMethod();
		iface.simpleMethod();
		iface.simpleMethod();

		assertEquals(5, iface.returnMethod());
		
		iface.paramsMethod(9);
		assertEquals(5+9, iface.returnMethod());
	}
	
	
	@Test
	public void testMethodInvocationInside() throws Exception
	{
		MethodInterface iface=createClass().newInstance();
		assertNotNull(iface);
		
		assertEquals(6, iface.methodToOverride(5));
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@SuppressWarnings("unchecked")
	private Class<? extends MethodInterface> createClass()
	{
		int index=++counter;
		LineFactory lineFactory=clsGenerator.lineFactory;
		ClassObject cls=clsGenerator.objectFactory.classes.createClass("test.MethodClass"+index);
		cls.setSuperClass(MethodClass.class);
		
		cls.getFields().add(clsGenerator.objectFactory.fields.createField("internalObj", MethodClass.class));
		
		//override default constructor
		ConstructorObject consObject=clsGenerator.objectFactory.constructors.createConstructor();
		consObject.setCode(lineFactory.methods.invokeSuperConstructor());
		cls.getConstructors().add(consObject);
		
		//override parameter constructor
		ConstructorObject consObjectParams=clsGenerator.objectFactory.constructors.createConstructor(new ParameterObject[] {new ParameterObject<Integer>("intValue", int.class)});
		consObjectParams.setCode(lineFactory.methods.invokeSuperConstructor(new ParameterObject[] {new ParameterObject<Integer>("intValue", "intValue", int.class)}));
		cls.getConstructors().add(consObjectParams);
		
		//Override method 'methodToOverride'
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("methodToOverride", new ParameterObject[] {new ParameterObject<Integer>("intValue", int.class)});
		method.setReturnType(int.class);
		CodeBlock block=lineFactory.createCodeBlock();
		block.add(lineFactory.methods.invokeMethod("paramsMethod", "this", new ParameterObject[] {new ParameterObject<Integer>("intValue", "intValue", int.class)}, null));
		block.add(lineFactory.methods.invokeMethod("simpleMethod", "this", null, null));
		block.add(lineFactory.variables.createLocalVariable("tmpReturn", int.class));
		block.add(lineFactory.methods.invokeMethodAndStore("returnMethod", "this", null, int.class, "tmpReturn"));
		block.add(lineFactory.methods.returnVariableValue("tmpReturn"));
		method.setCode(block);
		cls.getMethods().add(method);

		
		return (Class<? extends MethodInterface>) clsGenerator.generateClass(cls);
	}
	
	
	
	
	public static interface MethodInterface
	{
		public void simpleMethod();
		public int returnMethod();
		public void paramsMethod(int param1);
		
		public int methodToOverride(int param1);
	}
	
	
	
	public static class MethodClass implements MethodInterface
	{
		private int intValue=-1;

		public MethodClass()
		{
			intValue=0;
		}
		

		public MethodClass(int initValue)
		{
			intValue=initValue;
		}

		@Override
		public void simpleMethod()
		{
			intValue++;
		}

		@Override
		public int returnMethod() {
			return intValue;
		}

		@Override
		public void paramsMethod(int param1)
		{
			intValue+=param1;
		}


		@Override
		public int methodToOverride(int param1) {
			return -5;
		}
		
		public int methodToOverrideExample(int param1) {
			paramsMethod(param1);
			simpleMethod();
			int tmpReturn=returnMethod();
			return tmpReturn;
		}
		
	}

}
