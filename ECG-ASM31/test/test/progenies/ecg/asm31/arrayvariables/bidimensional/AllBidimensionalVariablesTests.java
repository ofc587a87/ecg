package test.progenies.ecg.asm31.arrayvariables.bidimensional;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import test.progenies.ecg.asm31.arrayvariables.bidimensional.primitives.TestStaticArrayVariables;
import test.progenies.ecg.asm31.arrayvariables.bidimensional.primitives.TestInstanceArrayVariables;
import test.progenies.ecg.asm31.arrayvariables.bidimensional.primitives.TestLocalArrayVariables;

@RunWith(Suite.class)
@SuiteClasses({TestLocalArrayVariables.class, test.progenies.ecg.asm31.arrayvariables.bidimensional.objects.TestLocalArrayVariables.class,
			   TestInstanceArrayVariables.class, test.progenies.ecg.asm31.arrayvariables.bidimensional.objects.TestInstanceArrayVariables.class,
			   TestStaticArrayVariables.class, test.progenies.ecg.asm31.arrayvariables.bidimensional.objects.TestStaticArrayVariables.class})
public class AllBidimensionalVariablesTests {

}
