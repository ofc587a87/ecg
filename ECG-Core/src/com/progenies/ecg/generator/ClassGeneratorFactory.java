package com.progenies.ecg.generator;

/**
 * Factory that will create the configurated Class Generation Tool.
 * @author ofc587a87
 *
 */
public final class ClassGeneratorFactory
{
	private static final class LazyInit
	{
		private static final ClassGenerator instance=createClassGenerator();
	}
	
	
	/**
	 * Creates an instance of the Class Generation Tool
	 * @return instance configurated
	 */
	public static ClassGenerator getClassGenerator()
	{
		//Using LazyInit, it's created on first use and it's thread safe (the classic 'if(xx==null) xx=new XX();' is not thread safe)
		return LazyInit.instance;
	}
	
	
	private final static ClassGenerator createClassGenerator()
	{
		//TODO: Support of other generators, by configuration or dynamic discovery
		try {
			return (ClassGenerator)Class.forName("com.progenies.ecg.asm31.generator.ClassGeneratorASM31").newInstance();
		} catch (Exception e)
		{
			throw new RuntimeException("Error creating the class generator", e);
		}
	}

}
