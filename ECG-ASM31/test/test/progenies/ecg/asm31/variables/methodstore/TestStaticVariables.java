package test.progenies.ecg.asm31.variables.methodstore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;

import org.junit.Test;
import org.objectweb.asm.Opcodes;

import test.progenies.ecg.asm31.variables.BaseTestStaticVariables;

import com.progenies.ecg.model.ClassObject;
import com.progenies.ecg.model.FieldObject;
import com.progenies.ecg.model.MethodObject;
import com.progenies.ecg.model.codeparts.CodeBlock;

public class TestStaticVariables extends BaseTestStaticVariables {

	
	
	
	@Test
	public void testStoreMethodValue() throws Exception
	{
		ClassObject cls=createClass();
		
		//uninitialized instance field
		FieldObject<Long> fld=clsGenerator.objectFactory.fields.createField("instanceField2", long.class);
		fld.setModifiers(new int[] {Opcodes.ACC_PROTECTED, Opcodes.ACC_STATIC});
		cls.getFields().add(fld);
		
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", long.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.methods.invokeMethodAndStore("instanceMethod", "this", null, long.class, "instanceField2"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceField2"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Long);
		assertEquals(5l, result);
	}
	
	
	@Test
	public void testStoreStaticMethodValue() throws Exception
	{
		ClassObject cls=createClass();
		
		//uninitialized instance field
		FieldObject<Long> fld=clsGenerator.objectFactory.fields.createField("instanceField2", long.class);
		fld.setModifiers(new int[] {Opcodes.ACC_PROTECTED, Opcodes.ACC_STATIC});
		cls.getFields().add(fld);
		
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", long.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.methods.invokeStaticMethodAndStore("staticMethod", null, null, long.class, "instanceField2"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceField2"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Long);
		assertEquals(9l, result);
	}
	
	
	
	
	


	@Test
	public void testStoreExternalMethodFieldValue() throws Exception
	{
		ClassObject cls=createClass();
		
		//uninitialized instance field
		FieldObject<Long> fld=clsGenerator.objectFactory.fields.createField("instanceField2", long.class);
		fld.setModifiers(new int[] {Opcodes.ACC_PROTECTED, Opcodes.ACC_STATIC});
		cls.getFields().add(fld);
		
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", long.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localObjVar", ExternalClass.class));
		block.add(clsGenerator.lineFactory.objects.createObjectAndStore(ExternalClass.class, null, "localObjVar"));
		block.add(clsGenerator.lineFactory.methods.invokeMethodAndStore("externalMethodLong", "localObjVar", null, long.class, "instanceField2"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceField2"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Long);
		assertEquals(9l, result);
	}
	
	
	@Test
	public void testStoreExternalMethodFieldObjectFieldValue() throws Exception
	{
		ClassObject cls=createClass();
		
		//uninitialized instance field
		FieldObject<Long> fld1=clsGenerator.objectFactory.fields.createField("instanceField2", long.class);
		fld1.setModifiers(new int[] {Opcodes.ACC_PROTECTED, Opcodes.ACC_STATIC});
		cls.getFields().add(fld1);
		
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", long.class);
		
		//field with object
		FieldObject<ExternalClass> fld=clsGenerator.objectFactory.fields.createField("fieldObj", ExternalClass.class, false);
		fld.setInitialize(true); //initialize without parameters (it would be specified on method "setInitializationParams"
		cls.getFields().add(fld);
		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.methods.invokeMethodAndStore("externalMethodLong", "fieldObj", null, long.class, "instanceField2"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceField2"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Long);
		assertEquals(9l, result);
	}
	
	
	@Test
	public void testStoreStaticExternalMethodFieldValue() throws Exception
	{
		ClassObject cls=createClass();
		
		//uninitialized instance field
		FieldObject<Long> fld1=clsGenerator.objectFactory.fields.createField("instanceField2", long.class);
		fld1.setModifiers(new int[] {Opcodes.ACC_PROTECTED, Opcodes.ACC_STATIC});
		cls.getFields().add(fld1);
		
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", long.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.methods.invokeStaticMethodAndStore("externalStaticMethodLong", ExternalClass.class, null, long.class, "instanceField2"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceField2"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Long);
		assertEquals(19l, result);
	}
	

}
