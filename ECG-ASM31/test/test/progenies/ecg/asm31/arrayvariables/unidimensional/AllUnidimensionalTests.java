package test.progenies.ecg.asm31.arrayvariables.unidimensional;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import test.progenies.ecg.asm31.arrayvariables.unidimensional.primitives.TestLocalArrayVariables;
import test.progenies.ecg.asm31.arrayvariables.unidimensional.primitives.TestStaticArrayVariables;
import test.progenies.ecg.asm31.arrayvariables.unidimensional.primitives.TestInstanceArrayVariables;

@RunWith(Suite.class)
@SuiteClasses({TestLocalArrayVariables.class, test.progenies.ecg.asm31.arrayvariables.unidimensional.objects.TestLocalArrayVariables.class,
				TestInstanceArrayVariables.class, test.progenies.ecg.asm31.arrayvariables.unidimensional.objects.TestInstanceArrayVariables.class,
				TestStaticArrayVariables.class, test.progenies.ecg.asm31.arrayvariables.unidimensional.objects.TestStaticArrayVariables.class})
public class AllUnidimensionalTests {

}
