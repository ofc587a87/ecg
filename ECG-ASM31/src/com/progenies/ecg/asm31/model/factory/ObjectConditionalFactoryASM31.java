package com.progenies.ecg.asm31.model.factory;

import com.progenies.ecg.asm31.model.codeparts.IfCodePartASM31;
import com.progenies.ecg.asm31.model.codeparts.SwitchCodePartASM31;
import com.progenies.ecg.model.ValueReference;
import com.progenies.ecg.model.codeparts.CaseBlock;
import com.progenies.ecg.model.codeparts.CodeBlock;
import com.progenies.ecg.model.codeparts.IfCodePart;
import com.progenies.ecg.model.codeparts.IfElseCodePart;
import com.progenies.ecg.model.codeparts.SwitchCodePart;
import com.progenies.ecg.model.conditions.ICondition;
import com.progenies.ecg.model.factory.IObjectConditionalFactory;

public class ObjectConditionalFactoryASM31 implements IObjectConditionalFactory {

	@Override
	public IfCodePart createIfBlock(ICondition condition, CodeBlock block)
	{
		return new IfCodePartASM31(condition, block);
	}

	@Override
	public IfElseCodePart createIfElseBlock(ICondition condition,	CodeBlock mainCodeBlock, CodeBlock elseCodeBlock)
	{
		return new IfCodePartASM31(condition, mainCodeBlock, elseCodeBlock);
	}

	@Override
	public SwitchCodePart createSwitchBlock(ValueReference<?> evaluatedValue, CaseBlock[] caseBlocks, CodeBlock defaultBlock) {
		return new SwitchCodePartASM31(evaluatedValue, caseBlocks, defaultBlock);
	}

}
