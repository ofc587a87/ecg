package test.progenies.ecg.asm31;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import test.progenies.ecg.asm31.arrayvariables.AllArrayVariablesTests;
import test.progenies.ecg.asm31.conditionals.AllConditionalTests;
import test.progenies.ecg.asm31.exceptions.TestTryCatchBlock;
import test.progenies.ecg.asm31.loops.AllLoopTests;
import test.progenies.ecg.asm31.methods.AllMethodTests;
import test.progenies.ecg.asm31.variables.AllVariableTests;

@RunWith(Suite.class)
@SuiteClasses({ TestSimpleMethodInvocation.class, TestSimpleGeneration.class, AllVariableTests.class, AllMethodTests.class, AllArrayVariablesTests.class,
	TestTryCatchBlock.class, AllConditionalTests.class, AllLoopTests.class})
public class AllTests {

}
