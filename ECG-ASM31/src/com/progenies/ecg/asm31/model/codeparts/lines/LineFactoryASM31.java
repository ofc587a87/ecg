package com.progenies.ecg.asm31.model.codeparts.lines;

import com.progenies.ecg.asm31.model.codeparts.CodeBlockASM31;
import com.progenies.ecg.model.codeparts.CodeBlock;
import com.progenies.ecg.model.codeparts.lines.IMethodFactory;
import com.progenies.ecg.model.codeparts.lines.IObjectManipulationFactory;
import com.progenies.ecg.model.codeparts.lines.IVariableFactory;
import com.progenies.ecg.model.codeparts.lines.LineFactory;

/**
 * According to Factory Pattern, this class will help in the creation
 * of individual LineCodeParts, dedicated to specific tasks.
 * 
 * @author ofc587a87
 * 
 * @see LineFactory
 *
 */
public class LineFactoryASM31 extends LineFactory
{

	@Override
	public CodeBlock createCodeBlock()
	{
		return new CodeBlockASM31();
	}

	@Override
	protected final IVariableFactory createVariableFactory()
	{
		return new VariablesFactoryASM31();
	}

	@Override
	protected final IMethodFactory createMethodFactory() {
		return new MethodFactoryASM31();
	}

	@Override
	protected final IObjectManipulationFactory createObjectManipulationFactory() {
		return new ObjectManipulationFactoryASM31();
	}
}
