package com.progenies.ecg.model.codeparts;

/**
 * Represents a Do-While Loop code block.
 * 
 * The condition cannot be null.
 * 
 * @author ofc587a87
 *
 */
public abstract class DoWhileLoopCodePart extends LoopCodePart
{

	
}
