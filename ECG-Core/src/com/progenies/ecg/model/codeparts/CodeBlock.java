package com.progenies.ecg.model.codeparts;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;


/**
 * CodePart that represents a collection of other code parts.
 * 
 * It's used on almost every other CodePart.
 * @author ofc587a87
 *
 */
public abstract class CodeBlock implements ICodePart, List<ICodePart>
{
	protected List<ICodePart> codeParts=new ArrayList<ICodePart>();

	
	
	@Override
	public void generateBytecode()
	{
		Iterator<ICodePart> it=this.codeParts.iterator();
		while(it.hasNext())
			it.next().generateBytecode();
	}
	
	
	
	
	
	
	@Override
	public boolean add(ICodePart codePart)
	{
		return this.codeParts.add(codePart);
	}
	
	@Override
	public void add(int index, ICodePart codePart)
	{
		this.codeParts.add(index, codePart);
	}
	
	@Override
	public boolean addAll(Collection<? extends ICodePart> c)
	{
		return this.codeParts.addAll(c);
	}
	
	@Override
	public boolean addAll(int index, Collection<? extends ICodePart> c)
	{
		return this.codeParts.addAll(index, c);
	}
	
	@Override
	public void clear() {
		this.codeParts.clear();
	}
	
	@Override
	public boolean contains(Object o) {
		return this.codeParts.contains(o);
	}
	
	@Override
	public boolean containsAll(Collection<?> c) {
		return this.codeParts.containsAll(c);
	}
	
	@Override
	public ICodePart get(int index) {
		return this.codeParts.get(index);
	}
	@Override
	public int indexOf(Object o) {
		return this.codeParts.indexOf(o);
	}
	@Override
	public boolean isEmpty() {
		return this.codeParts.isEmpty();
	}
	@Override
	public Iterator<ICodePart> iterator() {
		return this.codeParts.iterator();
	}
	@Override
	public int lastIndexOf(Object o) {
		return this.codeParts.lastIndexOf(o);
	}
	@Override
	public ListIterator<ICodePart> listIterator()
	{
		return this.codeParts.listIterator();
	}
	@Override
	public ListIterator<ICodePart> listIterator(int index) {
		return this.codeParts.listIterator(index);
	}
	@Override
	public boolean remove(Object o) {
		return this.codeParts.remove(o);
	}
	@Override
	public ICodePart remove(int index) {
		return this.codeParts.remove(index);
	}
	@Override
	public boolean removeAll(Collection<?> c) {
		return this.codeParts.removeAll(c);
	}
	@Override
	public boolean retainAll(Collection<?> c) {
		return this.codeParts.retainAll(c);
	}
	@Override
	public ICodePart set(int index, ICodePart element) {
		return this.codeParts.set(index, element);
	}
	@Override
	public int size() {
		return this.codeParts.size();
	}
	@Override
	public List<ICodePart> subList(int fromIndex, int toIndex) {
		return this.codeParts.subList(fromIndex, toIndex);
	}
	@Override
	public Object[] toArray() {
		return this.codeParts.toArray();
	}
	@Override
	public <T> T[] toArray(T[] a) {
		return this.codeParts.toArray(a);
	}


	


}
