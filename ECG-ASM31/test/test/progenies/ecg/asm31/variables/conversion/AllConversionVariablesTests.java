package test.progenies.ecg.asm31.variables.conversion;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestInstanceVariables.class, TestLocalVariables.class,
		TestStaticVariables.class })
public class AllConversionVariablesTests {

}
