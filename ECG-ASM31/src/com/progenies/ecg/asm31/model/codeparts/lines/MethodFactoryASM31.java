package com.progenies.ecg.asm31.model.codeparts.lines;

import com.progenies.ecg.model.ParameterObject;
import com.progenies.ecg.model.codeparts.lines.IMethodFactory;
import com.progenies.ecg.model.codeparts.lines.LineCodePart;

public class MethodFactoryASM31 implements IMethodFactory {


	/* (non-Javadoc)
	 * @see com.progenies.ecg.model.codeparts.lines.asm31_impl.ILineFactory#invokeMethod(java.lang.String, java.lang.Class, com.progenies.ecg.model.ParameterObject[])
	 */
	@Override
	public <T> LineCodePart invokeMethod(String name, String variableOwner, ParameterObject<?> params[], Class<?> returnType)
	{
		return new InvokeMethodLinePart<T>(name, variableOwner, params, returnType);
	}
	
	/* (non-Javadoc)
	 * @see com.progenies.ecg.model.codeparts.lines.asm31_impl.ILineFactory#invokeMethodAndStore(java.lang.String, java.lang.Class, com.progenies.ecg.model.ParameterObject[], java.lang.String)
	 */
	@Override
	public <T> LineCodePart invokeMethodAndStore(String name,  String variableOwner, ParameterObject<?> params[], Class<?> returnType, String variableName)
	{
		return new InvokeMethodLinePart<T>(name, variableOwner, params, returnType, variableName);
	}
	
	/* (non-Javadoc)
	 * @see com.progenies.ecg.model.codeparts.lines.asm31_impl.ILineFactory#invokeMethodAndReturn(java.lang.String, java.lang.Class, com.progenies.ecg.model.ParameterObject[])
	 */
	@Override
	public <T> LineCodePart invokeMethodAndReturn(String name,  String variableOwner, ParameterObject<?> params[], Class<?> returnType)
	{
		return new InvokeMethodLinePart<T>(name, variableOwner, params, returnType, "__^_return_^__");
	}
	
	/* (non-Javadoc)
	 * @see com.progenies.ecg.model.codeparts.lines.asm31_impl.ILineFactory#invokeMethod(java.lang.String, java.lang.Class, com.progenies.ecg.model.ParameterObject[])
	 */
	@Override
	public <T> LineCodePart invokeStaticMethod(String name, Class<T> classOwner, ParameterObject<?> params[], Class<?> returnType)
	{
		return new InvokeMethodLinePart<T>(name, classOwner, params, returnType);
	}
	
	/* (non-Javadoc)
	 * @see com.progenies.ecg.model.codeparts.lines.asm31_impl.ILineFactory#invokeMethodAndStore(java.lang.String, java.lang.Class, com.progenies.ecg.model.ParameterObject[], java.lang.String)
	 */
	@Override
	public <T> LineCodePart invokeStaticMethodAndStore(String name,  Class<T> classOwner, ParameterObject<?> params[], Class<?> returnType, String variableName)
	{
		return new InvokeMethodLinePart<T>(name, classOwner, params, returnType, variableName);
	}
	
	/* (non-Javadoc)
	 * @see com.progenies.ecg.model.codeparts.lines.asm31_impl.ILineFactory#invokeMethodAndStore(java.lang.String, java.lang.Class, com.progenies.ecg.model.ParameterObject[])
	 */
	@Override
	public <T> LineCodePart invokeStaticMethodAndReturn(String name,  Class<T> classOwner, ParameterObject<?> params[], Class<?> returnType)
	{
		return new InvokeMethodLinePart<T>(name, classOwner, params, returnType, "__^_return_^__");
	}
	



	@Override
	public LineCodePart returnVariableValue(String name)
	{
		return new ReturnVariableValue(name);
	}


	@Override
	public LineCodePart invokeSuperConstructor() {
		return invokeMethod("<init>", "super", null, null);
	}
	
	
	@Override
	public LineCodePart invokeSuperConstructor(ParameterObject<?> params[]) {
		return invokeMethod("<init>", "super", params, null);
	}


	@Override
	public LineCodePart returnEmpty() {
		return new ReturnVariableValue(null);
	}

	@Override
	public <T> LineCodePart returnStaticValue(T value) {
		return new ReturnStaticValue<T>(value);
	}

	@Override
	public LineCodePart throwNewException(Class<? extends Throwable> exception,	String message) {
		return new ThrowExceptionLinePart(exception, message, (String)null);
	}

	@Override
	public LineCodePart throwNewException(Class<? extends Throwable> exception,	String message, String causeVariableName) {
		return new ThrowExceptionLinePart(exception, message, causeVariableName);
	}

	@Override
	public LineCodePart throwException(String variableName)
	{
		return new ThrowExceptionLinePart(variableName);
	}

}
