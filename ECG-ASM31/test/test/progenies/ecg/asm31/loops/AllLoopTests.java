package test.progenies.ecg.asm31.loops;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestForEachLoop.class, TestForLoop.class, TestWhileLoop.class, TestDoWhileLoop.class })
public class AllLoopTests {

}
