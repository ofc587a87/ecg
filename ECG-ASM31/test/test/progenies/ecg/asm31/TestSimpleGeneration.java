package test.progenies.ecg.asm31;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;

import java.lang.reflect.Method;

import org.junit.Before;
import org.junit.Test;

import com.progenies.ecg.generator.ClassGenerator;
import com.progenies.ecg.generator.ClassGeneratorFactory;
import com.progenies.ecg.model.ClassObject;

public class TestSimpleGeneration
{
	private ClassGenerator clsGenerator;
	
	@Before
	public void setUp()
	{
		clsGenerator=ClassGeneratorFactory.getClassGenerator();
		assertNotNull(clsGenerator);
	}

	@Test
	public void testSimpleClass() throws Exception
	{
		ClassObject cls=clsGenerator.objectFactory.classes.createClass("test.SimpleClass1");
		cls.getFields().add(clsGenerator.objectFactory.fields.createField("field1", String.class, true));
		
		Class<?> resultCls=clsGenerator.generateClass(cls);
		assertNotNull(resultCls);
		assertEquals("test.SimpleClass1", resultCls.getName());
		assertEquals(Object.class.getMethods().length + 2, resultCls.getMethods().length);
		
		Object obj=resultCls.newInstance();
		assertNotNull(obj);
		
		//locate setters y getters
		Method setField1=resultCls.getMethod("setField1", new Class<?>[]{String.class});
		assertNotNull(setField1);
		Method getField1=resultCls.getMethod("getField1", (Class<?>[])null);
		assertNotNull(getField1);
		
		//before changes, getter should return null 
		assertNull(getField1.invoke(obj, (Object[])null));
		
		//change value
		setField1.invoke(obj, new Object[] {"MODIFIED"});

		assertNotNull(getField1.invoke(obj, (Object[])null));
		assertEquals("MODIFIED", getField1.invoke(obj, (Object[])null));
	}
	
	
	@Test
	public void testSimpleClassUsingInterface() throws Exception
	{
		ClassObject cls=clsGenerator.objectFactory.classes.createClass("test.SimpleClass2");
		cls.setInterfaces(new Class<?>[] {TestInterface.class});
		cls.getFields().add(clsGenerator.objectFactory.fields.createField("field1", String.class, true));
		
		@SuppressWarnings("unchecked")
		Class<? extends TestInterface> resultCls=(Class<? extends TestInterface>) clsGenerator.generateClass(cls);
		assertNotNull(resultCls);
		assertEquals("test.SimpleClass2", resultCls.getName());
		assertEquals(Object.class.getMethods().length + 2, resultCls.getMethods().length);
		
		TestInterface obj=resultCls.newInstance();
		assertNotNull(obj);
		
		//before changes, getter should return null 
		assertNull(obj.getField1());
		
		//change value
		obj.setField1("MODIFIED2");

		assertNotNull(obj.getField1());
		assertEquals("MODIFIED2", obj.getField1());
	}
	
	public static interface TestInterface
	{
		public String getField1();
		public void setField1(String newValue);
	}

}
