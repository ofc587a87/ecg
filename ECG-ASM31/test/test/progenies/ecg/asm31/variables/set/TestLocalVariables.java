package test.progenies.ecg.asm31.variables.set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;

import org.junit.Test;

import test.progenies.ecg.asm31.variables.BaseTestLocalVariables;
import test.progenies.ecg.asm31.variables.LocalFieldClass;

import com.progenies.ecg.model.ClassObject;
import com.progenies.ecg.model.FieldObject;
import com.progenies.ecg.model.MethodObject;
import com.progenies.ecg.model.ParameterObject;
import com.progenies.ecg.model.codeparts.CodeBlock;

public class TestLocalVariables extends BaseTestLocalVariables {

	
	
	
	@Test
	public void testSetVariableWithValueFromStaticValue() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar1", int.class));
		block.add(clsGenerator.lineFactory.variables.setVariableValue("localVar1", 65));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localVar1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Integer);
		assertEquals(65, result);
	}
	
	
	
	
	@Test
	public void testSetVariableWithValueFromLocalVar() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar2", int.class, 76));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar1", int.class));
		block.add(clsGenerator.lineFactory.variables.setVariableValueFromVariable("localVar1", "localVar2"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localVar1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Integer);
		assertEquals(76, result);
	}
	
	@Test
	public void testSetVariableWithValueFromInstanceVar() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar1", int.class));
		block.add(clsGenerator.lineFactory.variables.setVariableValueFromVariable("localVar1", "instanceField"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localVar1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Integer);
		assertEquals(56, result);
	}
	
	
	@Test
	public void testSetVariableWithValueFromStaticVar() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar1", int.class));
		block.add(clsGenerator.lineFactory.variables.setVariableValueFromVariable("localVar1", "staticField"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localVar1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Integer);
		assertEquals(88, result);
	}
	
	
	@Test
	public void testSetVariableWithValueFromParameter() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", new ParameterObject<?>[] {new ParameterObject<Integer>("paramVar", int.class)}, int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar1", int.class));
		block.add(clsGenerator.lineFactory.variables.setVariableValueFromVariable("localVar1", "paramVar"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localVar1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", new Class[] {int.class});
		Object result=mth.invoke(obj, new Object[] { 39 });
		assertTrue(result instanceof Integer);
		assertEquals(39, result);
	}
	
	
	
	
	@Test
	public void testSetVariableWithValueFromFieldInLocalObject() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar1", int.class));
		block.add(clsGenerator.lineFactory.variables.createLocalVariableAndInitialize("localObj1", LocalFieldClass.class));
		block.add(clsGenerator.lineFactory.variables.setVariableValueFromObjectField("localVar1", "localObj1", "intField", int.class));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localVar1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Integer);
		assertEquals(6, result);
	}
	
	@Test
	public void testSetVariableWithValueFromFieldInFieldObject() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		
		FieldObject<LocalFieldClass> fld=clsGenerator.objectFactory.fields.createField("instanceObj", LocalFieldClass.class);
		fld.setInitialize(true);
		cls.getFields().add(fld);
		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar1", int.class));
		block.add(clsGenerator.lineFactory.variables.setVariableValueFromObjectField("localVar1", "instanceObj", "intField", int.class));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localVar1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Integer);
		assertEquals(6, result);
	}
	
	
	
	@Test
	public void testSetVariableWithValueFromStaticFieldInClass() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar1", int.class));
		block.add(clsGenerator.lineFactory.variables.setVariableValueFromStaticClassField("localVar1", LocalFieldClass.class, "staticIntField", int.class));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localVar1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Integer);
		assertEquals(7, result);
	}


}
