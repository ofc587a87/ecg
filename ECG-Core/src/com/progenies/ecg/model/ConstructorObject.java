package com.progenies.ecg.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import com.progenies.ecg.model.codeparts.ICodePart;

//TODO: Support invocation of super/this with or without parameters
public abstract class ConstructorObject extends AbstractCodeObject
{
	
	/** Parameters of the method. It can be null or an empty array */
	protected ParameterObject<?> parameters[];
	
	protected List<Class<? extends Throwable>> exceptions;
	
	




	protected ICodePart code;
	
	


	public ICodePart getCode() {
		return code;
	}




	public void setCode(ICodePart code) {
		this.code = code;
	}


	public ParameterObject<?>[] getParameters() {
		return parameters==null?null:Arrays.copyOf(parameters, parameters.length);
	}




	public void setParameters(ParameterObject<?>[] parameters) {
		this.parameters = parameters==null?null:Arrays.copyOf(parameters, parameters.length);
	}
	
	public List<Class<? extends Throwable>> getExceptions() {
		return exceptions==null?null:Collections.unmodifiableList(exceptions);
	}




	public void setExceptions(List<Class<? extends Throwable>> exceptions) {
		this.exceptions = exceptions==null?null:new ArrayList<Class<? extends Throwable>>(exceptions);
	}

}
