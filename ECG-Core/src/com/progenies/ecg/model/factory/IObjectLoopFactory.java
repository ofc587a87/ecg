package com.progenies.ecg.model.factory;

import com.progenies.ecg.model.ValueReference;
import com.progenies.ecg.model.codeparts.CodeBlock;
import com.progenies.ecg.model.codeparts.DoWhileLoopCodePart;
import com.progenies.ecg.model.codeparts.ForLoopCodePart;
import com.progenies.ecg.model.codeparts.WhileLoopCodePart;
import com.progenies.ecg.model.codeparts.lines.LineCodePart;
import com.progenies.ecg.model.conditions.ICondition;

public interface IObjectLoopFactory
{
	public ForLoopCodePart createForLoop(LineCodePart initialCode, ICondition condition, LineCodePart postCode, CodeBlock codeBlock);
	
	public ForLoopCodePart createForEachLoop(Class<?> variableType, String variableName, ValueReference<? extends Object> iterableVariable, CodeBlock codeBlock);
	
	public WhileLoopCodePart createWhileLoop(ICondition condition, CodeBlock codeBlock);
	
	public DoWhileLoopCodePart createDoWhileLoop(ICondition condition, CodeBlock codeBlock);

	public LineCodePart createBreak();
	
	public LineCodePart createContinue();

}
