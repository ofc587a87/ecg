package com.progenies.ecg.model.codeparts.lines;

import com.progenies.ecg.model.ParameterObject;

public interface IObjectManipulationFactory
{
	/**
	 * Generate a LineCodePart that will generate an object instantiation, storing the result in a local or instance variable.
	 * @param classType Class type of the object. It cannot be null.
	 * @param params parameters of the constructor, they can be static or dynamic values. A null or empty array will mean no parameters.
	 * @param variableName name of the variable that will store the object. It can be either local or instance variable.
	 * @return LineCodePart created
	 */
	public <T> LineCodePart createObjectAndStore(Class<T> classType, ParameterObject<?> constructorParams[], String variableName);


}
