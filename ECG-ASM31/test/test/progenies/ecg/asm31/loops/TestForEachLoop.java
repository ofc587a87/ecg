package test.progenies.ecg.asm31.loops;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.lang.reflect.Method;
import java.util.ArrayList;

import org.junit.Before;
import org.junit.Test;

import com.progenies.ecg.generator.ClassGenerator;
import com.progenies.ecg.generator.ClassGeneratorFactory;
import com.progenies.ecg.model.ClassObject;
import com.progenies.ecg.model.ConstructorObject;
import com.progenies.ecg.model.MethodObject;
import com.progenies.ecg.model.ParameterObject;
import com.progenies.ecg.model.ValueReference;
import com.progenies.ecg.model.codeparts.CodeBlock;
import com.progenies.ecg.model.codeparts.ForLoopCodePart;
import com.progenies.ecg.model.codeparts.IfCodePart;
import com.progenies.ecg.model.codeparts.lines.LineFactory;
import com.progenies.ecg.model.conditions.BooleanCondition;
import com.progenies.ecg.model.conditions.ICondition;

public class TestForEachLoop {
	protected ClassGenerator clsGenerator;
	private static int counter = 0;
	
	@Test
	public void testSimpleIterationArray() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		
		block.add(clsGenerator.lineFactory.variables.createLocalArrayVariable("localIterable", int[].class,
				new ValueReference<?>[] {ValueReference.forStaticValue(3), ValueReference.forStaticValue(9), ValueReference.forStaticValue(9), ValueReference.forStaticValue(9), ValueReference.forStaticValue(9), ValueReference.forStaticValue(9)}));

		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 1)); //TODO: support increment from ValueReference
		
		ForLoopCodePart loopCode=clsGenerator.objectFactory.loops.createForEachLoop(int.class, "tmpInt", ValueReference.forVariable("localIterable"), bodyBlock);
		block.add(loopCode);
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
//		ASMIfierUtil.printClass(cls);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(2 + 1 + 1 +1 +1 +1 +1, result);
	}
	
	
	@Test
	public void testSimpleIterationIterable() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariableAndInitialize("localIterable", ArrayList.class));
		block.add(clsGenerator.lineFactory.methods.invokeMethod("add", "localIterable", new ParameterObject[] {new ParameterObject<String>("p1", String.class, "2")}, boolean.class));
		block.add(clsGenerator.lineFactory.methods.invokeMethod("add", "localIterable", new ParameterObject[] {new ParameterObject<String>("p1", String.class, "3")}, boolean.class));
		block.add(clsGenerator.lineFactory.methods.invokeMethod("add", "localIterable", new ParameterObject[] {new ParameterObject<String>("p1", String.class, "4")}, boolean.class));
		block.add(clsGenerator.lineFactory.methods.invokeMethod("add", "localIterable", new ParameterObject[] {new ParameterObject<String>("p1", String.class, "5")}, boolean.class));

		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 1)); //TODO: support increment from ValueReference
		
		ForLoopCodePart loopCode=clsGenerator.objectFactory.loops.createForEachLoop(String.class, "tmpStr", ValueReference.forVariable("localIterable"), bodyBlock);
		block.add(loopCode);
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
//		ASMIfierUtil.printClass(cls);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(2 + 1 + 1 +1 +1, result);
	}
	
	
	@Test
	public void testNoIterationArray() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		
		block.add(clsGenerator.lineFactory.variables.createLocalArrayVariable("localIterable", int[].class, new ValueReference<?>[0]));

		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 1)); //TODO: support increment from ValueReference
		
		ForLoopCodePart loopCode=clsGenerator.objectFactory.loops.createForEachLoop(int.class, "tmpInt", ValueReference.forVariable("localIterable"), bodyBlock);
		block.add(loopCode);
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
//		ASMIfierUtil.printClass(cls);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(2, result);
	}
	
	
	@Test
	public void testNoIterationIterable() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariableAndInitialize("localIterable", ArrayList.class));

		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 1)); //TODO: support increment from ValueReference
		
		ForLoopCodePart loopCode=clsGenerator.objectFactory.loops.createForEachLoop(String.class, "tmpStr", ValueReference.forVariable("localIterable"), bodyBlock);
		block.add(loopCode);
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
//		ASMIfierUtil.printClass(cls);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(2, result);
	}

	
	
	@Test
	public void testBreakContinue_NoConditionArrayIteration() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		
		block.add(clsGenerator.lineFactory.variables.createLocalArrayVariable("localIterable", int[].class,
				new ValueReference<?>[] {ValueReference.forStaticValue(3), ValueReference.forStaticValue(9), ValueReference.forStaticValue(9), ValueReference.forStaticValue(9), ValueReference.forStaticValue(9), ValueReference.forStaticValue(9)}));

		block.add(clsGenerator.lineFactory.variables.createLocalVariable("i", int.class, -1));

		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("i"));
		
		ICondition condition=new BooleanCondition<Object>(ValueReference.forVariable("i"), ValueReference.forStaticValue(4), BooleanCondition.ConditionType.IS_GREATER_OR_EQUALS_TO);
		CodeBlock ifBodyBlock=clsGenerator.lineFactory.createCodeBlock();
		ifBodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 10));
		ifBodyBlock.add(clsGenerator.objectFactory.loops.createBreak());
		
		CodeBlock elseBodyBlock=clsGenerator.lineFactory.createCodeBlock();
		elseBodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue"));
		elseBodyBlock.add(clsGenerator.objectFactory.loops.createContinue());
		
		IfCodePart ifCode=clsGenerator.objectFactory.conditionals.createIfElseBlock(condition, ifBodyBlock, elseBodyBlock);
		bodyBlock.add(ifCode);
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 50)); //dead code (if break/continue works well)
		
		
		ForLoopCodePart loopCode=clsGenerator.objectFactory.loops.createForEachLoop(int.class, "tmpInt", ValueReference.forVariable("localIterable"), bodyBlock);
		block.add(loopCode);
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(2 + 4 + 10, result);
	}
	
	
	
	@Test
	public void testBreakContinue_NoConditionIterableIteration() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariableAndInitialize("localIterable", ArrayList.class));
		block.add(clsGenerator.lineFactory.methods.invokeMethod("add", "localIterable", new ParameterObject[] {new ParameterObject<String>("p1", String.class, "2")}, boolean.class));
		block.add(clsGenerator.lineFactory.methods.invokeMethod("add", "localIterable", new ParameterObject[] {new ParameterObject<String>("p1", String.class, "3")}, boolean.class));
		block.add(clsGenerator.lineFactory.methods.invokeMethod("add", "localIterable", new ParameterObject[] {new ParameterObject<String>("p1", String.class, "4")}, boolean.class));
		block.add(clsGenerator.lineFactory.methods.invokeMethod("add", "localIterable", new ParameterObject[] {new ParameterObject<String>("p1", String.class, "5")}, boolean.class));
		block.add(clsGenerator.lineFactory.methods.invokeMethod("add", "localIterable", new ParameterObject[] {new ParameterObject<String>("p1", String.class, "5")}, boolean.class));
		block.add(clsGenerator.lineFactory.methods.invokeMethod("add", "localIterable", new ParameterObject[] {new ParameterObject<String>("p1", String.class, "5")}, boolean.class));
		block.add(clsGenerator.lineFactory.methods.invokeMethod("add", "localIterable", new ParameterObject[] {new ParameterObject<String>("p1", String.class, "5")}, boolean.class));
		block.add(clsGenerator.lineFactory.methods.invokeMethod("add", "localIterable", new ParameterObject[] {new ParameterObject<String>("p1", String.class, "5")}, boolean.class));

		block.add(clsGenerator.lineFactory.variables.createLocalVariable("i", int.class, -1));

		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("i"));
		
		ICondition condition=new BooleanCondition<Object>(ValueReference.forVariable("i"), ValueReference.forStaticValue(4), BooleanCondition.ConditionType.IS_GREATER_OR_EQUALS_TO);
		CodeBlock ifBodyBlock=clsGenerator.lineFactory.createCodeBlock();
		ifBodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 10));
		ifBodyBlock.add(clsGenerator.objectFactory.loops.createBreak());
		
		CodeBlock elseBodyBlock=clsGenerator.lineFactory.createCodeBlock();
		elseBodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue"));
		elseBodyBlock.add(clsGenerator.objectFactory.loops.createContinue());
		
		IfCodePart ifCode=clsGenerator.objectFactory.conditionals.createIfElseBlock(condition, ifBodyBlock, elseBodyBlock);
		bodyBlock.add(ifCode);
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 50)); //dead code (if break/continue works well)
		
		
		ForLoopCodePart loopCode=clsGenerator.objectFactory.loops.createForEachLoop(String.class, "tmpStr", ValueReference.forVariable("localIterable"), bodyBlock);
		block.add(loopCode);
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(2 + 4 + 10, result);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	

	
	
	
	
	@Before
	public void setUp() throws Exception {
		clsGenerator=ClassGeneratorFactory.getClassGenerator();
		assertNotNull(clsGenerator);
	}

	
	
	

	protected ClassObject createClass()
	{
		counter++;
		int index=counter;
		LineFactory lineFactory=clsGenerator.lineFactory;
		
		String subpackage=this.getClass().getPackage().getName().substring(this.getClass().getPackage().getName().lastIndexOf('.')+1);
		ClassObject cls=clsGenerator.objectFactory.classes.createClass("test.loops."+subpackage+".ForEachTestClass"+index);
		cls.setSuperClass(Object.class);

		
		//override default constructor
		ConstructorObject consObject=clsGenerator.objectFactory.constructors.createConstructor();
		consObject.setCode(lineFactory.methods.invokeSuperConstructor());
		cls.getConstructors().add(consObject);
		
		
		return cls;
		
	}

}
