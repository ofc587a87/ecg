package com.progenies.ecg.asm31.model.codeparts;

import org.objectweb.asm.ClassVisitor;

import com.progenies.ecg.asm31.util.VariableRegister;
import com.progenies.ecg.model.ClassObject;

public interface IConfigurableMethodObject {

	/* (non-Javadoc)
	 * @see com.progenies.ecg.model.AbstractCodeObject#generateBytecode()
	 */
	public void generateBytecode();

	public void configure(ClassVisitor visitor,	ClassObject parent, VariableRegister varRegister);

	public ClassObject getParent();

}