package test.progenies.ecg.asm31.arrayvariables.tridimensional.primitives;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;

import org.junit.Test;

import test.progenies.ecg.asm31.arrayvariables.tridimensional.BaseTestInstancelArrayVariables;

import com.progenies.ecg.model.ClassObject;
import com.progenies.ecg.model.FieldObject;
import com.progenies.ecg.model.MethodObject;
import com.progenies.ecg.model.codeparts.CodeBlock;

public class TestInstanceArrayVariables extends BaseTestInstancelArrayVariables
{
	@Test
	public void testCreateNotInitializedArrayVariable() throws Exception
	{
		ClassObject cls=createClass();
		
		//an instance array will be default initialized to null
		FieldObject<int[][][]> fld0=clsGenerator.objectFactory.fields.createField("instanceArray", int[][][].class);
		cls.getFields().add(fld0);
		
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int[][][].class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceArray"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNull(result);
	}
	
	@Test
	public void testCreateNullArrayVariable() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int[][][].class);
		
		FieldObject<int[][][]> fld0=clsGenerator.objectFactory.fields.createField("instanceArray", int[][][].class);
		fld0.setValue(null);
		cls.getFields().add(fld0);
				
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceArray"));
		method.setCode(block);
		cls.getMethods().add(method);

		Class<?> clsResult=clsGenerator.generateClass(cls);

		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNull(result);
	}
	
	@Test
	public void testCreateEmptyArrayVariable() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int[][][].class);
		
		cls.getFields().add(clsGenerator.objectFactory.fields.createFieldArray("instanceArray", int[][][].class, new int[]{5, 3, 9}));
		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceArray"));
		method.setCode(block);
		cls.getMethods().add(method);

		Class<?> clsResult=clsGenerator.generateClass(cls);

		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertTrue(result instanceof int[][][]);
		assertArrayEquals(new int[5][3][9], (int[][][])result);
	}
	
	
	
	@Test
	public void testCreateStaticValuesArrayVariable() throws Exception
	{
		ClassObject cls=createClass();
		
		cls.getFields().add(clsGenerator.objectFactory.fields.createFieldArrayWithDefaultValue("instanceArray", int[][][].class, new int[][][] {{{1, 2, 5}, {2, 6, 1}}, {{1, 2, 8}, {2, 6, 1}}}));
		
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int[][][].class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceArray"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);

		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertTrue(result instanceof int[][][]);
		assertArrayEquals(new int[][][] {{{1, 2, 5}, {2, 6, 1}}, {{1, 2, 8}, {2, 6, 1}}}, (int[][][])result);
	}
	
	
	
	
	
	
	
	/*--------------------------------------------
	 *----------- From method invocation --------- 
	 *--------------------------------------------
	 */
	
	
	@Test
	public void testCreateArrayFromLocalMethod() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int[][][].class);
		
		cls.getFields().add(clsGenerator.objectFactory.fields.createFieldArray("instanceArray", int[][][].class));
		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.methods.invokeMethodAndStore("createArray", "this", null, int[][][].class, "instanceArray"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceArray"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);

		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertTrue(result instanceof int[][][]);
		assertArrayEquals(new int[][][] {{{2,4,6},	{3,5,7}, {4,6,8}}, {{2,4,6},	{3,5,7}, {4,6,8}}}, (int[][][])result);
	}
	
	
	
	@Test
	public void testCreateArrayFromExternalInstanceMethod() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int[][][].class);
		
		cls.getFields().add(clsGenerator.objectFactory.fields.createFieldArray("instanceArray", int[][][].class));
		
		FieldObject<MethodClass> fld=clsGenerator.objectFactory.fields.createField("localObj", MethodClass.class);
		fld.setInitialize(true);
		cls.getFields().add(fld);
		
		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.methods.invokeMethodAndStore("createArray", "localObj", null, int[][][].class, "instanceArray"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceArray"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);

		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertTrue(result instanceof int[][][]);
		assertArrayEquals(new int[][][] {{{2,4,6},	{3,5,7}, {4,6,8}}, {{2,4,6},	{3,5,7}, {4,6,8}}}, (int[][][])result);
	}
	
	
	
	@Test
	public void testCreateArrayFromStaticMethod() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int[][][].class);
		
		cls.getFields().add(clsGenerator.objectFactory.fields.createFieldArray("instanceArray", int[][][].class));
		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.methods.invokeStaticMethodAndStore("createStaticArray", MethodClass.class, null, int[][][].class, "instanceArray"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceArray"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);

		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertTrue(result instanceof int[][][]);
		assertArrayEquals(new int[][][] {{{2,4,6,8}, {3,5,7,9}}, {{2,4,6,8}, {3,5,7,9}}}, (int[][][])result);
	}

}
