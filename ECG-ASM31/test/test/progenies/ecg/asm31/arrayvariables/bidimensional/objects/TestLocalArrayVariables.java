package test.progenies.ecg.asm31.arrayvariables.bidimensional.objects;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.lang.reflect.Method;

import org.junit.Test;

import test.progenies.ecg.asm31.arrayvariables.bidimensional.BaseTestLocalArrayVariables;

import com.progenies.ecg.model.ClassObject;
import com.progenies.ecg.model.FieldObject;
import com.progenies.ecg.model.MethodObject;
import com.progenies.ecg.model.ParameterObject;
import com.progenies.ecg.model.ValueReference;
import com.progenies.ecg.model.codeparts.CodeBlock;

public class TestLocalArrayVariables extends BaseTestLocalArrayVariables
{
	@Test(expected=RuntimeException.class)
	public void testCreateNotInitializedArrayVariable() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", MethodClass[][].class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar1", MethodClass[][].class));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localVar1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=null;
		try
		{
			//TODO: now ASM is throwing an exception, i should be able to know in an internal analysis if the variable has been initialized before load into stack
			clsResult=clsGenerator.generateClass(cls);
			clsResult.newInstance();
		}catch(RuntimeException ex)
		{
			if(ex.getMessage().startsWith("Error at instruction 2: Expected an object reference, but found ."))
				throw ex;
		}
		
		fail("An exception shold have been thrown!!");
	}
	
	@Test
	public void testCreateNullArrayVariable() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", MethodClass[][].class);
		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar1", MethodClass[][].class, null));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localVar1"));
		method.setCode(block);
		cls.getMethods().add(method);

		Class<?> clsResult=clsGenerator.generateClass(cls);

		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNull(result);
	}
	
	@Test
	public void testCreateEmptyArrayVariable() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", MethodClass[][].class);
		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalArrayVariable("localVar1", MethodClass[][].class, new int[]{5,2}));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localVar1"));
		method.setCode(block);
		cls.getMethods().add(method);

		Class<?> clsResult=clsGenerator.generateClass(cls);

		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertTrue(result instanceof MethodClass[][]);
		assertArrayEquals(new MethodClass[5][2], (MethodClass[][])result);
	}
	
	
	// Objects are not static values, so this test its not possible
//	@Test  
//	public void testCreateStaticValuesArrayVariable() throws Exception
//	{
//		ClassObject cls=createClass();
//		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", MethodClass[][].class);
//		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
//		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar1", MethodClass[][].class, new MethodClass[][] {new MethodClass(1), new MethodClass(2), new MethodClass(3)}));
//		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localVar1"));
//		method.setCode(block);
//		cls.getMethods().add(method);
//		
//		Class<?> clsResult=clsGenerator.generateClass(cls);
//
//		Object obj=clsResult.newInstance();
//		Method mth=clsResult.getMethod("method1", (Class[])null);
//		Object result=mth.invoke(obj, (Object[])null);
//		assertNotNull(result);
//		assertTrue(result instanceof MethodClass[][]);
//		assertArrayEquals(new MethodClass[][] {new MethodClass(1), new MethodClass(2), new MethodClass(3)}, (MethodClass[][])result);
//	}
	
	
	
	@Test
	public void testCreateMixedTypedArrayVariable() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", MethodClass[][].class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVarVal1", MethodClass.class));
		block.add(clsGenerator.lineFactory.objects.createObjectAndStore(MethodClass.class, new ParameterObject<?>[]{new ParameterObject<Integer>("p1", int.class, 2)}, "localVarVal1"));

		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVarVal2", ExtendedMethodClass.class));
		block.add(clsGenerator.lineFactory.objects.createObjectAndStore(ExtendedMethodClass.class, new ParameterObject<?>[]{new ParameterObject<Integer>("p1", int.class, 8)}, "localVarVal2"));
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVarVal3", MethodIface.class));
		block.add(clsGenerator.lineFactory.objects.createObjectAndStore(MethodClass.class, new ParameterObject<?>[]{new ParameterObject<Integer>("p1", int.class, 7)}, "localVarVal3"));

		block.add(clsGenerator.lineFactory.variables.createLocalArrayVariable("localVar1", MethodClass[][].class, new ValueReference[][] {
																{ValueReference.forVariable("localVarVal1"),
																ValueReference.forVariable("localVarVal2"),
																ValueReference.forVariable("localVarVal3"),
																ValueReference.forVariable("instanceObjVar")},{
																ValueReference.forVariable("staticObjVar"),
																ValueReference.forFieldInVariable("instanceObjVarValue", "instanceObj"),
																ValueReference.forFieldInVariable("instanceObjVarValue", "staticObj"),
																ValueReference.forStaticFieldInClass("staticObjVarValue", TestObj.class)}}));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localVar1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);

		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertTrue(result instanceof MethodClass[][]);
		assertArrayEquals(new MethodClass[][] {{new MethodClass(2), new ExtendedMethodClass(8), new MethodClass(7), new MethodClass(4)},
											   {new MethodClass(6), new MethodClass(50), new MethodClass(50), new MethodClass(100)}}, (MethodClass[][])result);
	}
	
	
	
	
	
	/*--------------------------------------------
	 *----------- From method invocation --------- 
	 *--------------------------------------------
	 */
	
	
	@Test
	public void testCreateArrayFromLocalMethod() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", MethodClass[][].class);
		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVarVal1", MethodClass[][].class));
		block.add(clsGenerator.lineFactory.methods.invokeMethodAndStore("createObjMArray", "this", null, MethodClass[][].class, "localVarVal1"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localVarVal1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);

		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertTrue(result instanceof MethodClass[][]);
		assertArrayEquals(new MethodClass[][] {{new MethodClass(2), new ExtendedMethodClass(2), new MethodClass(6)},
												{new MethodClass(3), new ExtendedMethodClass(3), new MethodClass(7)},
												{new MethodClass(4), new ExtendedMethodClass(4), new MethodClass(8)}}, (MethodClass[][])result);
	}
	
	
	
	@Test
	public void testCreateArrayFromExternalInstanceMethod() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", MethodClass[][].class);
		
		FieldObject<MethodClass> fld=clsGenerator.objectFactory.fields.createField("localObj", MethodClass.class);
		fld.setInitialize(true);
		cls.getFields().add(fld);
		
		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVarVal1", MethodClass[][].class));
		block.add(clsGenerator.lineFactory.methods.invokeMethodAndStore("createObjMArray", "localObj", null, MethodClass[][].class, "localVarVal1"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localVarVal1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);

		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertTrue(result instanceof MethodClass[][]);
		assertArrayEquals(new MethodClass[][] {{new MethodClass(2), new ExtendedMethodClass(2), new MethodClass(6)},
												{new MethodClass(3), new ExtendedMethodClass(3), new MethodClass(7)},
												{new MethodClass(4), new ExtendedMethodClass(4), new MethodClass(8)}}, (MethodClass[][])result);
	}
	
	
	
	@Test
	public void testCreateArrayFromStaticMethod() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", MethodClass[][].class);
		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVarVal1", MethodClass[][].class));
		block.add(clsGenerator.lineFactory.methods.invokeStaticMethodAndStore("createStaticObjMArray", MethodClass.class, null, MethodClass[][].class, "localVarVal1"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localVarVal1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);

		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertTrue(result instanceof MethodClass[][]);
		assertArrayEquals(new MethodClass[][] {{new MethodClass(2), new MethodClass(4), new ExtendedMethodClass(3), new MethodClass(8)},
												{new MethodClass(3), new MethodClass(5), new ExtendedMethodClass(4), new MethodClass(9)}}, (MethodClass[][])result);
	}

	
	
}
