package test.progenies.ecg.asm31.exceptions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.objectweb.asm.Opcodes;

import com.progenies.ecg.generator.ClassGenerator;
import com.progenies.ecg.generator.ClassGeneratorFactory;
import com.progenies.ecg.model.ClassObject;
import com.progenies.ecg.model.ConstructorObject;
import com.progenies.ecg.model.FieldObject;
import com.progenies.ecg.model.MethodObject;
import com.progenies.ecg.model.ParameterObject;
import com.progenies.ecg.model.codeparts.CodeBlock;
import com.progenies.ecg.model.codeparts.TryCatchCodePart;
import com.progenies.ecg.model.codeparts.TryCatchCodePart.CatchBlock;
import com.progenies.ecg.model.codeparts.lines.LineFactory;

public class TestTryCatchBlock {

	protected ClassGenerator clsGenerator;
	private static int counter = 0;


	
	@Test
	public void testCheckedExceptionCatch() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 0));

		TryCatchCodePart tryCatch=clsGenerator.objectFactory.trycatch.createTryCatchBlock();
		tryCatch.getBodyBlock().add(clsGenerator.lineFactory.methods.invokeMethod("checkExceptionMethod", "this", null, null));

		CatchBlock catchBlock1=clsGenerator.objectFactory.trycatch.createCatchBlock(ClassNotFoundException.class);
		catchBlock1.getCatchBody().add(clsGenerator.lineFactory.variables.setVariableValue("localValue", 8));
		tryCatch.getCatchBlocks().add(catchBlock1);

		CatchBlock catchBlock2=clsGenerator.objectFactory.trycatch.createCatchBlock(RuntimeException.class);
		catchBlock2.getCatchBody().add(clsGenerator.lineFactory.variables.setVariableValue("localValue", 6));
		tryCatch.getCatchBlocks().add(catchBlock2);
		
		tryCatch.getFinallyBlock().add(clsGenerator.lineFactory.variables.incrementVariable("localValue"));

		block.add(tryCatch);
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(9, result);
		
	}
	
	
	@Test
	public void testRuntimeExceptionCatch() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 0));

		TryCatchCodePart tryCatch=clsGenerator.objectFactory.trycatch.createTryCatchBlock();
		tryCatch.getBodyBlock().add(clsGenerator.lineFactory.methods.invokeMethod("runtimeExceptionMethod", "this", null, null));
//		tryCatch.getBodyBlock().add(clsGenerator.lineFactory.methods.throwNewException(ClassNotFoundException.class, "aa"));
		CatchBlock catchBlock1=clsGenerator.objectFactory.trycatch.createCatchBlock(ClassNotFoundException.class);
		catchBlock1.getCatchBody().add(clsGenerator.lineFactory.variables.setVariableValue("localValue", 8));
		tryCatch.getCatchBlocks().add(catchBlock1);

		CatchBlock catchBlock2=clsGenerator.objectFactory.trycatch.createCatchBlock(RuntimeException.class);
		catchBlock2.getCatchBody().add(clsGenerator.lineFactory.variables.setVariableValue("localValue", 6));
		tryCatch.getCatchBlocks().add(catchBlock2);
		
		tryCatch.getFinallyBlock().add(clsGenerator.lineFactory.variables.decrementVariable("localValue"));

		block.add(tryCatch);
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(5, result);
		
	}
	
	
	@Test
	public void testNotThrowException() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 0));

		TryCatchCodePart tryCatch=clsGenerator.objectFactory.trycatch.createTryCatchBlock();
		tryCatch.getBodyBlock().add(clsGenerator.lineFactory.variables.setVariableValue("localValue", 32));

		CatchBlock catchBlock1=clsGenerator.objectFactory.trycatch.createCatchBlock(ClassNotFoundException.class);
		catchBlock1.getCatchBody().add(clsGenerator.lineFactory.variables.setVariableValue("localValue", 8));
		tryCatch.getCatchBlocks().add(catchBlock1);

		CatchBlock catchBlock2=clsGenerator.objectFactory.trycatch.createCatchBlock(RuntimeException.class);
		catchBlock2.getCatchBody().add(clsGenerator.lineFactory.variables.setVariableValue("localValue", 6));
		tryCatch.getCatchBlocks().add(catchBlock2);
		
		tryCatch.getFinallyBlock().add(clsGenerator.lineFactory.variables.incrementVariable("localValue"));

		block.add(tryCatch);
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(33, result);
		
	}
	
	
	
	@Test
	public void testExceptionInCatch1() throws Exception
	{
		ClassObject cls=createClass();
		
		FieldObject<Integer> fld=clsGenerator.objectFactory.fields.createField("instanceField", int.class);
		fld.setModifiers(new int[] {Opcodes.ACC_PUBLIC});
		cls.getFields().add(fld);
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();

		TryCatchCodePart tryCatch=clsGenerator.objectFactory.trycatch.createTryCatchBlock();
		tryCatch.getBodyBlock().add(clsGenerator.lineFactory.methods.invokeMethod("checkExceptionMethod", "this", null, null));
		CatchBlock catchBlock1=clsGenerator.objectFactory.trycatch.createCatchBlock(ClassNotFoundException.class);
		catchBlock1.getCatchBody().add(clsGenerator.lineFactory.variables.setVariableValue("instanceField", 8));
		catchBlock1.getCatchBody().add(clsGenerator.lineFactory.methods.invokeMethod("runtimeExceptionMethod", "this", null, null));
		tryCatch.getCatchBlocks().add(catchBlock1);

		CatchBlock catchBlock2=clsGenerator.objectFactory.trycatch.createCatchBlock(RuntimeException.class);
		catchBlock2.getCatchBody().add(clsGenerator.lineFactory.variables.setVariableValue("instanceField", 6));
		catchBlock2.getCatchBody().add(clsGenerator.lineFactory.methods.invokeMethod("runtimeExceptionMethod", "this", null, null));
		tryCatch.getCatchBlocks().add(catchBlock2);
		
		tryCatch.getFinallyBlock().add(clsGenerator.lineFactory.variables.decrementVariable("instanceField"));

		block.add(tryCatch);
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceField"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
//		ASMIfierUtil.printClass(cls);

		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		
		Object obj=clsResult.newInstance();
		
		try
		{
			Method mth=clsResult.getMethod("method1", (Class[])null);
			mth.invoke(obj, (Object[])null);
			
			fail("An exception should had been thrown");
		}catch(Exception ex)
		{
//			ex.printStackTrace();
			assertTrue(ex instanceof RuntimeException || ex.getCause() instanceof RuntimeException);
		}
		
		Integer result=(Integer)clsResult.getField("instanceField").get(obj);
		assertNotNull(result);
		assertEquals(7, result.intValue());
		
	}
	
	
	@Test
	public void testExceptionInCatch2() throws Exception
	{
		ClassObject cls=createClass();
		
		FieldObject<Integer> fld=clsGenerator.objectFactory.fields.createField("instanceField", int.class);
		fld.setModifiers(new int[] {Opcodes.ACC_PUBLIC});
		cls.getFields().add(fld);
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();

		TryCatchCodePart tryCatch=clsGenerator.objectFactory.trycatch.createTryCatchBlock();
		tryCatch.getBodyBlock().add(clsGenerator.lineFactory.methods.invokeMethod("runtimeExceptionMethod", "this", null, null));
		CatchBlock catchBlock1=clsGenerator.objectFactory.trycatch.createCatchBlock(ClassNotFoundException.class);
		catchBlock1.getCatchBody().add(clsGenerator.lineFactory.variables.setVariableValue("instanceField", 8));
		tryCatch.getCatchBlocks().add(catchBlock1);

		CatchBlock catchBlock2=clsGenerator.objectFactory.trycatch.createCatchBlock(RuntimeException.class);
		catchBlock2.getCatchBody().add(clsGenerator.lineFactory.variables.setVariableValue("instanceField", 6));
		catchBlock2.getCatchBody().add(clsGenerator.lineFactory.methods.invokeMethod("runtimeExceptionMethod", "this", null, null));
		tryCatch.getCatchBlocks().add(catchBlock2);
		
		tryCatch.getFinallyBlock().add(clsGenerator.lineFactory.variables.decrementVariable("instanceField"));

		block.add(tryCatch);
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceField"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
//		ASMIfierUtil.printClass(cls);

		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		
		Object obj=clsResult.newInstance();
		
		try
		{
			Method mth=clsResult.getMethod("method1", (Class[])null);
			mth.invoke(obj, (Object[])null);
			
			fail("An exception should had been thrown");
		}catch(Exception ex)
		{
//			ex.printStackTrace();
			assertTrue(ex instanceof RuntimeException || ex.getCause() instanceof RuntimeException);
		}
		
		Integer result=(Integer)clsResult.getField("instanceField").get(obj);
		assertNotNull(result);
		assertEquals(5, result.intValue());
		
	}
	
	
	@Test
	public void testExceptionInFinally() throws Exception
	{
		ClassObject cls=createClass();
		
		FieldObject<Integer> fld=clsGenerator.objectFactory.fields.createField("instanceField", int.class);
		fld.setModifiers(new int[] {Opcodes.ACC_PUBLIC});
		cls.getFields().add(fld);
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();

		TryCatchCodePart tryCatch=clsGenerator.objectFactory.trycatch.createTryCatchBlock();
		tryCatch.getBodyBlock().add(clsGenerator.lineFactory.variables.decrementVariable("instanceField"));
		CatchBlock catchBlock1=clsGenerator.objectFactory.trycatch.createCatchBlock(ClassNotFoundException.class);
		catchBlock1.getCatchBody().add(clsGenerator.lineFactory.variables.setVariableValue("instanceField", 8));
		tryCatch.getCatchBlocks().add(catchBlock1);

		CatchBlock catchBlock2=clsGenerator.objectFactory.trycatch.createCatchBlock(RuntimeException.class);
		catchBlock2.getCatchBody().add(clsGenerator.lineFactory.variables.setVariableValue("instanceField", 6));
		catchBlock2.getCatchBody().add(clsGenerator.lineFactory.methods.invokeMethod("runtimeExceptionMethod", "this", null, null));
		tryCatch.getCatchBlocks().add(catchBlock2);
		
		tryCatch.getFinallyBlock().add(clsGenerator.lineFactory.methods.invokeMethod("runtimeExceptionMethod", "this", null, null));

		block.add(tryCatch);
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceField"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
//		ASMIfierUtil.printClass(cls);

		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		
		Object obj=clsResult.newInstance();
		
		try
		{
			Method mth=clsResult.getMethod("method1", (Class[])null);
			mth.invoke(obj, (Object[])null);
			
			fail("An exception should had been thrown");
		}catch(Exception ex)
		{
//			ex.printStackTrace();
			assertTrue(ex instanceof RuntimeException || ex.getCause() instanceof RuntimeException);
		}
		
		Integer result=(Integer)clsResult.getField("instanceField").get(obj);
		assertNotNull(result);
		assertEquals(-1, result.intValue());
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Before
	public void setUp() throws Exception {
		clsGenerator=ClassGeneratorFactory.getClassGenerator();
		assertNotNull(clsGenerator);
	}

	
	
	

	protected ClassObject createClass()
	{
		counter++;
		int index=counter;
		LineFactory lineFactory=clsGenerator.lineFactory;
		
		String subpackage=this.getClass().getPackage().getName().substring(this.getClass().getPackage().getName().lastIndexOf('.')+1);
		ClassObject cls=clsGenerator.objectFactory.classes.createClass("test.exceptions."+subpackage+".LocalVariableTestClass"+index);
		cls.setSuperClass(Object.class);
		
		
		//create checked exception method
		List<Class<? extends Throwable>> exceptions=new ArrayList<Class<? extends Throwable>>();
		exceptions.add(ClassNotFoundException.class);
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("checkExceptionMethod", (ParameterObject[])null, exceptions);
		method1.setCode(clsGenerator.lineFactory.methods.throwNewException(ClassNotFoundException.class, "Class not found over there"));
		cls.getMethods().add(method1);

		
		//create checked exception method
		MethodObject method2=clsGenerator.objectFactory.methods.createMethod("runtimeExceptionMethod");
		method2.setCode(clsGenerator.lineFactory.methods.throwNewException(RuntimeException.class, "Runtime Exception thrown"));
		cls.getMethods().add(method2);

		
		//override default constructor
		ConstructorObject consObject=clsGenerator.objectFactory.constructors.createConstructor();
		consObject.setCode(lineFactory.methods.invokeSuperConstructor());
		cls.getConstructors().add(consObject);
		
		
		return cls;
		
	}

}
