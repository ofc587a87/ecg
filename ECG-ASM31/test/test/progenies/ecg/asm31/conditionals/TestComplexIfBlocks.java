package test.progenies.ecg.asm31.conditionals;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.lang.reflect.Method;

import org.junit.Before;
import org.junit.Test;
import org.objectweb.asm.Opcodes;

import com.progenies.ecg.generator.ClassGenerator;
import com.progenies.ecg.generator.ClassGeneratorFactory;
import com.progenies.ecg.model.ClassObject;
import com.progenies.ecg.model.ConstructorObject;
import com.progenies.ecg.model.FieldObject;
import com.progenies.ecg.model.MethodObject;
import com.progenies.ecg.model.ValueReference;
import com.progenies.ecg.model.codeparts.CodeBlock;
import com.progenies.ecg.model.codeparts.IfCodePart;
import com.progenies.ecg.model.codeparts.lines.LineFactory;
import com.progenies.ecg.model.conditions.BooleanCondition;
import com.progenies.ecg.model.conditions.CombinationCondition;
import com.progenies.ecg.model.conditions.ICondition;

public class TestComplexIfBlocks {
	
	protected ClassGenerator clsGenerator;
	private static int counter = 0;
	
	
	
	@Test
	public void testIfLocalVariable() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localDecider", boolean.class, true));

		ICondition condition=new BooleanCondition<Object>(ValueReference.forVariable("localDecider"), BooleanCondition.ConditionType.IS_TRUE);
		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 22));
		IfCodePart ifCode=clsGenerator.objectFactory.conditionals.createIfBlock(condition, bodyBlock);
		block.add(ifCode);
		
		
		ICondition condition2=new BooleanCondition<Object>(ValueReference.forVariable("localDecider"), BooleanCondition.ConditionType.IS_FALSE);
		CodeBlock bodyBlock2=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock2.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 31));
		IfCodePart ifCode2=clsGenerator.objectFactory.conditionals.createIfBlock(condition2, bodyBlock2);
		block.add(ifCode2);
		
		block.add(clsGenerator.lineFactory.variables.setVariableValue("localDecider", false));
		
		ICondition condition3=new BooleanCondition<Object>(ValueReference.forVariable("localDecider"), BooleanCondition.ConditionType.IS_FALSE);
		CodeBlock bodyBlock3=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock3.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 5));
		IfCodePart ifCode3=clsGenerator.objectFactory.conditionals.createIfBlock(condition3, bodyBlock3);
		block.add(ifCode3);
		
		
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(2 + 22 + 5, result);
	}
	
	@Test
	public void testIfFieldVariable() throws Exception
	{
		ClassObject cls=createClass();
		
		FieldObject<Boolean> field=clsGenerator.objectFactory.fields.createField("fieldDecider", boolean.class);
		field.setValue(true);
		cls.getFields().add(field);
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));

		ICondition condition=new BooleanCondition<Object>(ValueReference.forVariable("fieldDecider"), BooleanCondition.ConditionType.IS_TRUE);
		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 22));
		IfCodePart ifCode=clsGenerator.objectFactory.conditionals.createIfBlock(condition, bodyBlock);
		block.add(ifCode);
		
		
		ICondition condition2=new BooleanCondition<Object>(ValueReference.forVariable("fieldDecider"), BooleanCondition.ConditionType.IS_FALSE);
		CodeBlock bodyBlock2=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock2.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 31));
		IfCodePart ifCode2=clsGenerator.objectFactory.conditionals.createIfBlock(condition2, bodyBlock2);
		block.add(ifCode2);
		
		block.add(clsGenerator.lineFactory.variables.setVariableValue("fieldDecider", false));
		
		ICondition condition3=new BooleanCondition<Object>(ValueReference.forVariable("fieldDecider"), BooleanCondition.ConditionType.IS_FALSE);
		CodeBlock bodyBlock3=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock3.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 5));
		IfCodePart ifCode3=clsGenerator.objectFactory.conditionals.createIfBlock(condition3, bodyBlock3);
		block.add(ifCode3);
		
		
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(2 + 22 + 5, result);
	}
	
	
	@Test
	public void testIfStaticFieldVariable() throws Exception
	{
		ClassObject cls=createClass();
		
		FieldObject<Boolean> field=clsGenerator.objectFactory.fields.createField("fieldDecider", boolean.class);
		field.setModifiers(new int[] {Opcodes.ACC_PRIVATE, Opcodes.ACC_STATIC});
		field.setValue(true);
		cls.getFields().add(field);
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));

		ICondition condition=new BooleanCondition<Object>(ValueReference.forVariable("fieldDecider"), BooleanCondition.ConditionType.IS_TRUE);
		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 22));
		IfCodePart ifCode=clsGenerator.objectFactory.conditionals.createIfBlock(condition, bodyBlock);
		block.add(ifCode);
		
		
		ICondition condition2=new BooleanCondition<Object>(ValueReference.forVariable("fieldDecider"), BooleanCondition.ConditionType.IS_FALSE);
		CodeBlock bodyBlock2=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock2.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 31));
		IfCodePart ifCode2=clsGenerator.objectFactory.conditionals.createIfBlock(condition2, bodyBlock2);
		block.add(ifCode2);
		
		block.add(clsGenerator.lineFactory.variables.setVariableValue("fieldDecider", false));
		
		ICondition condition3=new BooleanCondition<Object>(ValueReference.forVariable("fieldDecider"), BooleanCondition.ConditionType.IS_FALSE);
		CodeBlock bodyBlock3=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock3.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 5));
		IfCodePart ifCode3=clsGenerator.objectFactory.conditionals.createIfBlock(condition3, bodyBlock3);
		block.add(ifCode3);
		
		
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(2 + 22 + 5, result);
	}
	
	
	@Test
	public void testIfExternalStaticFieldVariable() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));

		ICondition condition=new BooleanCondition<StaticClass>(ValueReference.forStaticFieldInClass("decider", StaticClass.class), BooleanCondition.ConditionType.IS_TRUE);
		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 22));
		IfCodePart ifCode=clsGenerator.objectFactory.conditionals.createIfBlock(condition, bodyBlock);
		block.add(ifCode);
		
		
		ICondition condition2=new BooleanCondition<StaticClass>(ValueReference.forStaticFieldInClass("decider", StaticClass.class), BooleanCondition.ConditionType.IS_FALSE);
		CodeBlock bodyBlock2=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock2.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 31));
		IfCodePart ifCode2=clsGenerator.objectFactory.conditionals.createIfBlock(condition2, bodyBlock2);
		block.add(ifCode2);
		
		block.add(clsGenerator.lineFactory.variables.setStaticFieldValue("decider", StaticClass.class, false));
		
		ICondition condition3=new BooleanCondition<StaticClass>(ValueReference.forStaticFieldInClass("decider", StaticClass.class), BooleanCondition.ConditionType.IS_FALSE);
		CodeBlock bodyBlock3=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock3.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 5));
		IfCodePart ifCode3=clsGenerator.objectFactory.conditionals.createIfBlock(condition3, bodyBlock3);
		block.add(ifCode3);
		
		
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(2 + 22 + 5, result);
	}
	
	
	
	
	@Test
	public void testIfFieldInFieldVariable() throws Exception
	{
		ClassObject cls=createClass();
		
		FieldObject<StaticClass> field=clsGenerator.objectFactory.fields.createField("field", StaticClass.class);
		field.setInitialize(true);
		cls.getFields().add(field);
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));

		ICondition condition=new BooleanCondition<Object>(ValueReference.forFieldInVariable("fieldDecider", "field"), BooleanCondition.ConditionType.IS_TRUE);
		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 22));
		IfCodePart ifCode=clsGenerator.objectFactory.conditionals.createIfBlock(condition, bodyBlock);
		block.add(ifCode);
		
		
		ICondition condition2=new BooleanCondition<Object>(ValueReference.forFieldInVariable("fieldDecider", "field"), BooleanCondition.ConditionType.IS_FALSE);
		CodeBlock bodyBlock2=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock2.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 31));
		IfCodePart ifCode2=clsGenerator.objectFactory.conditionals.createIfBlock(condition2, bodyBlock2);
		block.add(ifCode2);
		
		block.add(clsGenerator.lineFactory.variables.setFieldInFieldValue("fieldDecider", "field", false));
		
		ICondition condition3=new BooleanCondition<Object>(ValueReference.forFieldInVariable("fieldDecider", "field"), BooleanCondition.ConditionType.IS_FALSE);
		CodeBlock bodyBlock3=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock3.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 5));
		IfCodePart ifCode3=clsGenerator.objectFactory.conditionals.createIfBlock(condition3, bodyBlock3);
		block.add(ifCode3);
		
		
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(2 + 22 + 5, result);
	}
	
	
	
	
	@Test
	public void testIfMethodDeciderVariable() throws Exception
	{
		ClassObject cls=createClass();
		
		FieldObject<Boolean> field=clsGenerator.objectFactory.fields.createField("fieldDecider", boolean.class);
		field.setModifiers(new int[] {Opcodes.ACC_PRIVATE});
		field.setValue(true);
		cls.getFields().add(field);
		
		MethodObject methodDecider=clsGenerator.objectFactory.methods.createMethod("methodDecider", boolean.class);
		methodDecider.setCode(clsGenerator.lineFactory.methods.returnVariableValue("fieldDecider"));
		cls.getMethods().add(methodDecider);
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));

		ValueReference<Object> referenceCondition1=ValueReference.forExecutionResult(clsGenerator.lineFactory.methods.invokeMethod("methodDecider", "this", null, boolean.class));
		ICondition condition=new BooleanCondition<Object>(referenceCondition1, BooleanCondition.ConditionType.IS_TRUE);
		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 22));
		IfCodePart ifCode=clsGenerator.objectFactory.conditionals.createIfBlock(condition, bodyBlock);
		block.add(ifCode);
		block.add(clsGenerator.lineFactory.methods.invokeMethod("methodDecider", "this", null, boolean.class));
		
		ValueReference<Object> referenceCondition2=ValueReference.forExecutionResult(clsGenerator.lineFactory.methods.invokeMethod("methodDecider", "this", null, boolean.class));
		ICondition condition2=new BooleanCondition<Object>(referenceCondition2, BooleanCondition.ConditionType.IS_FALSE);
		CodeBlock bodyBlock2=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock2.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 31));
		IfCodePart ifCode2=clsGenerator.objectFactory.conditionals.createIfBlock(condition2, bodyBlock2);
		block.add(ifCode2);
		
		block.add(clsGenerator.lineFactory.variables.setVariableValue("fieldDecider", false));
		
		ValueReference<Object> referenceCondition3=ValueReference.forExecutionResult(clsGenerator.lineFactory.methods.invokeMethod("methodDecider", "this", null, boolean.class));
		ICondition condition3=new BooleanCondition<Object>(referenceCondition3, BooleanCondition.ConditionType.IS_FALSE);
		CodeBlock bodyBlock3=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock3.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 5));
		IfCodePart ifCode3=clsGenerator.objectFactory.conditionals.createIfBlock(condition3, bodyBlock3);
		block.add(ifCode3);
		
		
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(2 + 22 + 5, result);
	}
	
	
	
	@Test
	public void testComplexAndAllTrue() throws Exception
	{
		
		StaticClass.decider=true;
		
		ClassObject cls=createClass();

		FieldObject<Boolean> field=clsGenerator.objectFactory.fields.createField("fieldDecider", boolean.class);
		field.setValue(true);
		cls.getFields().add(field);
		
		FieldObject<Boolean> field2=clsGenerator.objectFactory.fields.createField("fieldDecider2", boolean.class);
		field2.setModifiers(new int[] {Opcodes.ACC_PRIVATE, Opcodes.ACC_STATIC});
		field2.setValue(true);
		cls.getFields().add(field2);
		
		FieldObject<StaticClass> field3=clsGenerator.objectFactory.fields.createField("field3", StaticClass.class);
		field3.setInitialize(true);
		cls.getFields().add(field3);
		
		MethodObject methodDecider=clsGenerator.objectFactory.methods.createMethod("methodDecider", boolean.class);
		methodDecider.setCode(clsGenerator.lineFactory.methods.returnVariableValue("fieldDecider"));
		cls.getMethods().add(methodDecider);
		


		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localDecider", boolean.class, true));

		ICondition conditionA=new BooleanCondition<Object>(ValueReference.forVariable("localDecider"), BooleanCondition.ConditionType.IS_TRUE);
		ICondition conditionB=new BooleanCondition<Object>(ValueReference.forVariable("fieldDecider"), BooleanCondition.ConditionType.IS_TRUE);
		ICondition conditionC=new BooleanCondition<Object>(ValueReference.forVariable("fieldDecider2"), BooleanCondition.ConditionType.IS_TRUE);
		ICondition conditionD=new BooleanCondition<StaticClass>(ValueReference.forStaticFieldInClass("decider", StaticClass.class), BooleanCondition.ConditionType.IS_TRUE);
		ICondition conditionE=new BooleanCondition<Object>(ValueReference.forFieldInVariable("fieldDecider", "field3"), BooleanCondition.ConditionType.IS_TRUE);

		ValueReference<Object> referenceCondition1=ValueReference.forExecutionResult(clsGenerator.lineFactory.methods.invokeMethod("methodDecider", "this", null, boolean.class));
		ICondition conditionF=new BooleanCondition<Object>(referenceCondition1, BooleanCondition.ConditionType.IS_TRUE);

		ICondition ifCodeCombinationAB=new CombinationCondition(conditionA, conditionB, CombinationCondition.CombinationConditionType.AND);
		ICondition ifCodeCombinationABC=new CombinationCondition(ifCodeCombinationAB, conditionC, CombinationCondition.CombinationConditionType.AND);
		ICondition ifCodeCombinationABCD=new CombinationCondition(ifCodeCombinationABC, conditionD, CombinationCondition.CombinationConditionType.AND);
		ICondition ifCodeCombinationABCDE=new CombinationCondition(ifCodeCombinationABCD, conditionE, CombinationCondition.CombinationConditionType.AND);
		ICondition ifCodeCombinationABCDEF=new CombinationCondition(ifCodeCombinationABCDE, conditionF, CombinationCondition.CombinationConditionType.AND);
		
		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 23));

		IfCodePart ifCode=clsGenerator.objectFactory.conditionals.createIfBlock(ifCodeCombinationABCDEF, bodyBlock);
		
		block.add(ifCode);
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(25, result);
	}
	
	
	@Test
	public void testComplexAndAllTrueButLast() throws Exception
	{
		StaticClass.decider=true;
		
		ClassObject cls=createClass();

		FieldObject<Boolean> field=clsGenerator.objectFactory.fields.createField("fieldDecider", boolean.class);
		field.setValue(true);
		cls.getFields().add(field);
		
		FieldObject<Boolean> field2=clsGenerator.objectFactory.fields.createField("fieldDecider2", boolean.class);
		field2.setModifiers(new int[] {Opcodes.ACC_PRIVATE, Opcodes.ACC_STATIC});
		field2.setValue(true);
		cls.getFields().add(field2);
		
		FieldObject<StaticClass> field3=clsGenerator.objectFactory.fields.createField("field3", StaticClass.class);
		field3.setInitialize(true);
		cls.getFields().add(field3);
		
		MethodObject methodDecider=clsGenerator.objectFactory.methods.createMethod("methodDecider", boolean.class);
		methodDecider.setCode(clsGenerator.lineFactory.methods.returnVariableValue("fieldDecider"));
		cls.getMethods().add(methodDecider);
		


		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localDecider", boolean.class, true));

		ICondition conditionA=new BooleanCondition<Object>(ValueReference.forVariable("localDecider"), BooleanCondition.ConditionType.IS_TRUE);
		ICondition conditionB=new BooleanCondition<Object>(ValueReference.forVariable("fieldDecider"), BooleanCondition.ConditionType.IS_TRUE);
		ICondition conditionC=new BooleanCondition<Object>(ValueReference.forVariable("fieldDecider2"), BooleanCondition.ConditionType.IS_TRUE);
		ICondition conditionD=new BooleanCondition<StaticClass>(ValueReference.forStaticFieldInClass("decider", StaticClass.class), BooleanCondition.ConditionType.IS_TRUE);
		ICondition conditionE=new BooleanCondition<Object>(ValueReference.forFieldInVariable("fieldDecider", "field3"), BooleanCondition.ConditionType.IS_TRUE);

		ValueReference<Object> referenceCondition1=ValueReference.forExecutionResult(clsGenerator.lineFactory.methods.invokeMethod("methodDecider", "this", null, boolean.class));
		ICondition conditionF=new BooleanCondition<Object>(referenceCondition1, BooleanCondition.ConditionType.IS_FALSE);

		ICondition ifCodeCombinationAB=new CombinationCondition(conditionA, conditionB, CombinationCondition.CombinationConditionType.AND);
		ICondition ifCodeCombinationABC=new CombinationCondition(ifCodeCombinationAB, conditionC, CombinationCondition.CombinationConditionType.AND);
		ICondition ifCodeCombinationABCD=new CombinationCondition(ifCodeCombinationABC, conditionD, CombinationCondition.CombinationConditionType.AND);
		ICondition ifCodeCombinationABCDE=new CombinationCondition(ifCodeCombinationABCD, conditionE, CombinationCondition.CombinationConditionType.AND);
		ICondition ifCodeCombinationABCDEF=new CombinationCondition(ifCodeCombinationABCDE, conditionF, CombinationCondition.CombinationConditionType.AND);
		
		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 23));

		IfCodePart ifCode=clsGenerator.objectFactory.conditionals.createIfBlock(ifCodeCombinationABCDEF, bodyBlock);
		
		block.add(ifCode);
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(2, result);
	}
	
	
	@Test
	public void testComplexOrAllFalseButLast() throws Exception
	{
		
		StaticClass.decider=true;
		
		ClassObject cls=createClass();

		FieldObject<Boolean> field=clsGenerator.objectFactory.fields.createField("fieldDecider", boolean.class);
		field.setValue(true);
		cls.getFields().add(field);
		
		FieldObject<Boolean> field2=clsGenerator.objectFactory.fields.createField("fieldDecider2", boolean.class);
		field2.setModifiers(new int[] {Opcodes.ACC_PRIVATE, Opcodes.ACC_STATIC});
		field2.setValue(true);
		cls.getFields().add(field2);
		
		FieldObject<StaticClass> field3=clsGenerator.objectFactory.fields.createField("field3", StaticClass.class);
		field3.setInitialize(true);
		cls.getFields().add(field3);
		
		MethodObject methodDecider=clsGenerator.objectFactory.methods.createMethod("methodDecider", boolean.class);
		methodDecider.setCode(clsGenerator.lineFactory.methods.returnVariableValue("fieldDecider"));
		cls.getMethods().add(methodDecider);
		


		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localDecider", boolean.class, true));

		ICondition conditionA=new BooleanCondition<Object>(ValueReference.forVariable("localDecider"), BooleanCondition.ConditionType.IS_FALSE);
		ICondition conditionB=new BooleanCondition<Object>(ValueReference.forVariable("fieldDecider"), BooleanCondition.ConditionType.IS_FALSE);
		ICondition conditionC=new BooleanCondition<Object>(ValueReference.forVariable("fieldDecider2"), BooleanCondition.ConditionType.IS_FALSE);
		ICondition conditionD=new BooleanCondition<StaticClass>(ValueReference.forStaticFieldInClass("decider", StaticClass.class), BooleanCondition.ConditionType.IS_FALSE);
		ICondition conditionE=new BooleanCondition<Object>(ValueReference.forFieldInVariable("fieldDecider", "field3"), BooleanCondition.ConditionType.IS_FALSE);

		ValueReference<Object> referenceCondition1=ValueReference.forExecutionResult(clsGenerator.lineFactory.methods.invokeMethod("methodDecider", "this", null, boolean.class));
		ICondition conditionF=new BooleanCondition<Object>(referenceCondition1, BooleanCondition.ConditionType.IS_TRUE);

		ICondition ifCodeCombinationAB=new CombinationCondition(conditionA, conditionB, CombinationCondition.CombinationConditionType.OR);
		ICondition ifCodeCombinationABC=new CombinationCondition(ifCodeCombinationAB, conditionC, CombinationCondition.CombinationConditionType.OR);
		ICondition ifCodeCombinationABCD=new CombinationCondition(ifCodeCombinationABC, conditionD, CombinationCondition.CombinationConditionType.OR);
		ICondition ifCodeCombinationABCDE=new CombinationCondition(ifCodeCombinationABCD, conditionE, CombinationCondition.CombinationConditionType.OR);
		ICondition ifCodeCombinationABCDEF=new CombinationCondition(ifCodeCombinationABCDE, conditionF, CombinationCondition.CombinationConditionType.OR);
		
		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 23));

		IfCodePart ifCode=clsGenerator.objectFactory.conditionals.createIfBlock(ifCodeCombinationABCDEF, bodyBlock);
		
		block.add(ifCode);
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(25, result);
	}
	
	@Test
	public void testComplexOrAllFalse() throws Exception
	{
		
		StaticClass.decider=true;
		
		ClassObject cls=createClass();

		FieldObject<Boolean> field=clsGenerator.objectFactory.fields.createField("fieldDecider", boolean.class);
		field.setValue(true);
		cls.getFields().add(field);
		
		FieldObject<Boolean> field2=clsGenerator.objectFactory.fields.createField("fieldDecider2", boolean.class);
		field2.setModifiers(new int[] {Opcodes.ACC_PRIVATE, Opcodes.ACC_STATIC});
		field2.setValue(true);
		cls.getFields().add(field2);
		
		FieldObject<StaticClass> field3=clsGenerator.objectFactory.fields.createField("field3", StaticClass.class);
		field3.setInitialize(true);
		cls.getFields().add(field3);
		
		MethodObject methodDecider=clsGenerator.objectFactory.methods.createMethod("methodDecider", boolean.class);
		methodDecider.setCode(clsGenerator.lineFactory.methods.returnVariableValue("fieldDecider"));
		cls.getMethods().add(methodDecider);
		


		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localDecider", boolean.class, true));

		ICondition conditionA=new BooleanCondition<Object>(ValueReference.forVariable("localDecider"), BooleanCondition.ConditionType.IS_FALSE);
		ICondition conditionB=new BooleanCondition<Object>(ValueReference.forVariable("fieldDecider"), BooleanCondition.ConditionType.IS_FALSE);
		ICondition conditionC=new BooleanCondition<Object>(ValueReference.forVariable("fieldDecider2"), BooleanCondition.ConditionType.IS_FALSE);
		ICondition conditionD=new BooleanCondition<StaticClass>(ValueReference.forStaticFieldInClass("decider", StaticClass.class), BooleanCondition.ConditionType.IS_FALSE);
		ICondition conditionE=new BooleanCondition<Object>(ValueReference.forFieldInVariable("fieldDecider", "field3"), BooleanCondition.ConditionType.IS_FALSE);

		ValueReference<Object> referenceCondition1=ValueReference.forExecutionResult(clsGenerator.lineFactory.methods.invokeMethod("methodDecider", "this", null, boolean.class));
		ICondition conditionF=new BooleanCondition<Object>(referenceCondition1, BooleanCondition.ConditionType.IS_FALSE);

		ICondition ifCodeCombinationAB=new CombinationCondition(conditionA, conditionB, CombinationCondition.CombinationConditionType.OR);
		ICondition ifCodeCombinationABC=new CombinationCondition(ifCodeCombinationAB, conditionC, CombinationCondition.CombinationConditionType.OR);
		ICondition ifCodeCombinationABCD=new CombinationCondition(ifCodeCombinationABC, conditionD, CombinationCondition.CombinationConditionType.OR);
		ICondition ifCodeCombinationABCDE=new CombinationCondition(ifCodeCombinationABCD, conditionE, CombinationCondition.CombinationConditionType.OR);
		ICondition ifCodeCombinationABCDEF=new CombinationCondition(ifCodeCombinationABCDE, conditionF, CombinationCondition.CombinationConditionType.OR);
		
		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 23));

		IfCodePart ifCode=clsGenerator.objectFactory.conditionals.createIfBlock(ifCodeCombinationABCDEF, bodyBlock);
		
		block.add(ifCode);
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(2, result);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Before
	public void setUp() throws Exception {
		clsGenerator=ClassGeneratorFactory.getClassGenerator();
		assertNotNull(clsGenerator);
	}

	
	
	

	protected ClassObject createClass()
	{
		counter++;
		int index=counter;
		LineFactory lineFactory=clsGenerator.lineFactory;
		
		String subpackage=this.getClass().getPackage().getName().substring(this.getClass().getPackage().getName().lastIndexOf('.')+1);
		ClassObject cls=clsGenerator.objectFactory.classes.createClass("test.conditionals."+subpackage+".IfComplexTestClass"+index);
		cls.setSuperClass(Object.class);

		
		//override default constructor
		ConstructorObject consObject=clsGenerator.objectFactory.constructors.createConstructor();
		consObject.setCode(lineFactory.methods.invokeSuperConstructor());
		cls.getConstructors().add(consObject);
		
		
		return cls;
		
	}
	
	
	public static final class StaticClass
	{
		public static boolean decider=true;
		
		
		public boolean fieldDecider=true;
	}

}
