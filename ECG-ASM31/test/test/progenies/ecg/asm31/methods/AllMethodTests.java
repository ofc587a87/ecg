package test.progenies.ecg.asm31.methods;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import test.progenies.ecg.asm31.methods.exceptions.AllMethodExceptionTests;
import test.progenies.ecg.asm31.methods.invoke.AllMethodInvokeTests;
import test.progenies.ecg.asm31.methods.receive.AllMethodReceiveTests;
import test.progenies.ecg.asm31.methods.returns.AllMethodReturnsTests;

@RunWith(Suite.class)
@SuiteClasses({ AllMethodInvokeTests.class, AllMethodReceiveTests.class, AllMethodReturnsTests.class, AllMethodExceptionTests.class })
public class AllMethodTests {

}
