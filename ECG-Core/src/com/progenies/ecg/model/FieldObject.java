package com.progenies.ecg.model;

import java.util.Arrays;

/**
 * Represents a class field.
 * @author ofc587a87
 *
 */
public abstract class FieldObject<T> extends AbstractCodeObject
{
	/**
	 * Define the type of the field.
	 * Primitives can be defined as their warped classes,
	 * using autoboxing.
	 */
	protected Class<T> classType;
	protected boolean generateSetter;
	protected boolean generateGetter;
	protected T value;
	
	protected int[] arrayDimensions;
	protected ValueReference<?> varReference[];
	protected ValueReference<?> matrixVarReference[][];


	protected boolean initialize;
	protected ParameterObject<?> initializationParams[];
	
	

	public Class<T> getClassType() {
		return classType;
	}

	public void setClassType(Class<T> classType) {
		this.classType = classType;
	}
	
	public boolean isGenerateSetter() {
		return generateSetter;
	}

	public void setGenerateSetter(boolean generateSetter) {
		this.generateSetter = generateSetter;
	}

	public boolean isGenerateGetter() {
		return generateGetter;
	}

	public void setGenerateGetter(boolean generateGetter) {
		this.generateGetter = generateGetter;
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value = value;
	}

	public boolean isInitialize() {
		return initialize;
	}

	public void setInitialize(boolean initialize) {
		this.initialize = initialize;
	}

	public ParameterObject<?>[] getInitializationParams() {
		return initializationParams==null?null:Arrays.copyOf(initializationParams, initializationParams.length);
	}

	public void setInitializationParams(ParameterObject<?>[] initializationParams) {
		this.initializationParams = initializationParams==null?null:Arrays.copyOf(initializationParams, initializationParams.length);
		this.initialize=initializationParams!=null;
	}

	public int[] getArrayDimensions() {
		return arrayDimensions==null?null:Arrays.copyOf(arrayDimensions, arrayDimensions.length);
	}

	public void setArrayDimensions(int[] arrayDimensions) {
		this.arrayDimensions = arrayDimensions==null?null:Arrays.copyOf(arrayDimensions, arrayDimensions.length);
	}

	public ValueReference<?>[] getVarReference() {
		return varReference==null?null:Arrays.copyOf(varReference, varReference.length);
	}

	public void setVarReference(ValueReference<?> varReference[]) {
		this.varReference = varReference==null?null:Arrays.copyOf(varReference, varReference.length);
	}
	

	public ValueReference<?>[][] getMatrixVarReference() {
		return matrixVarReference==null?null:Arrays.copyOf(matrixVarReference, matrixVarReference.length);
	}

	public void setMatrixVarReference(ValueReference<?>[][] matrixVarReference) {
		this.matrixVarReference = matrixVarReference==null?null:Arrays.copyOf(matrixVarReference, matrixVarReference.length);
	}
	
}
