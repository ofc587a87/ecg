package com.progenies.ecg.model.factory;



public abstract class ObjectFactory
{
	
	public final IObjectClassFactory classes=createClassObjectFactory();

	protected abstract IObjectClassFactory createClassObjectFactory();

	
	public final IObjectFieldFactory fields=createFieldObjectFactory();

	protected abstract IObjectFieldFactory createFieldObjectFactory();
	
	
	public final IObjectConstructorFactory constructors=createConstructorObjectFactory();

	protected abstract IObjectConstructorFactory createConstructorObjectFactory();
	
	
	public final IObjectMethodFactory methods=createMethodObjectFactory();

	protected abstract IObjectMethodFactory createMethodObjectFactory();
	
	
	public final IObjectTryCatchFactory trycatch=createTryCatchObjectFactory();

	protected abstract IObjectTryCatchFactory createTryCatchObjectFactory();
	
	
	public final IObjectConditionalFactory conditionals=createConditionalsObjectFactory();

	protected abstract IObjectConditionalFactory createConditionalsObjectFactory();

	
	public final IObjectLoopFactory loops=createLoopObjectFactory();

	protected abstract IObjectLoopFactory createLoopObjectFactory();
}
