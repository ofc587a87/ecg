package com.progenies.ecg.asm31.model.factory;

import com.progenies.ecg.model.ClassObject;
import com.progenies.ecg.model.factory.IObjectClassFactory;

public class ObjectClassFactoryASM31 implements IObjectClassFactory {

	@Override
	public ClassObject createClass(String className)
	{
		return createClass(className, Object.class);
	}

	@Override
	public ClassObject createClass(String className, Class<?> baseClass)
	{
		if(className==null)
			throw new IllegalArgumentException("className cannot be null");
		if(baseClass==null)
			throw new IllegalArgumentException("baseClass cannot be null");
		
		ClassObject clsObject=new ClassObjectASM31(className, baseClass);
		
		return clsObject;
	}

}
