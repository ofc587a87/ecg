package com.progenies.ecg.model.codeparts;

import com.progenies.ecg.model.AbstractCodeObject;
import com.progenies.ecg.model.conditions.ICondition;

/**
 * Abstract class, base of any loop block
 * 
 * @author ofc587a87
 *
 */
public abstract class LoopCodePart extends AbstractCodeObject implements ICodePart
{
	/**
	 * Condition that decides if the loop must break or continue;
	 */
	protected ICondition condition;
	
	/**
	 * Body of the loop block.
	 */
	protected CodeBlock bodyBlock;
	
	
	
	public CodeBlock getBodyBlock() {
		return bodyBlock;
	}




	public void setBodyBlock(CodeBlock bodyBlock) {
		this.bodyBlock = bodyBlock;
	}




	public ICondition getCondition() {
		return condition;
	}




	public void setCondition(ICondition condition) {
		this.condition = condition;
	}

}
