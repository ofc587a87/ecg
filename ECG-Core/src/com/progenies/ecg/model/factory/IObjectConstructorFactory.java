package com.progenies.ecg.model.factory;

import com.progenies.ecg.model.ConstructorObject;
import com.progenies.ecg.model.ParameterObject;

public interface IObjectConstructorFactory
{
	


	/**
	 * Creates a default public constructor with no parameters
	 * @return Constructor Object
	 */
	public ConstructorObject createConstructor();
	
	/**
	 * Creates a public constructor with parameters.
	 * @param parameters parameters of the constructor. It can be null or an empty Array.
	 * @return Constructor Object
	 */
	public ConstructorObject createConstructor(ParameterObject<?>[] parameters);

}
