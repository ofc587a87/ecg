package test.progenies.ecg.asm31.methods.exceptions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.objectweb.asm.Opcodes;

import test.progenies.ecg.asm31.methods.BaseTestInternalStaticMethods;

import com.progenies.ecg.model.ClassObject;
import com.progenies.ecg.model.MethodObject;
import com.progenies.ecg.model.ParameterObject;


/**
 * Test method invocation, being the methods static but from the same class class
 * @author ofc587a87
 *
 */
public class TestInternalStaticMethods extends BaseTestInternalStaticMethods {

	

	
	/* --------------------------------------------- */
	/* --    Exception Tests    -------------------- */
	/* --------------------------------------------- */
	
	
	@Test
	public void testCheckedExceptionNotThrown() throws Exception
	{
		ClassObject cls=createClass();
		
		//create method
		List<Class<? extends Throwable>> exceptions=new ArrayList<Class<? extends Throwable>>();
		exceptions.add(ClassNotFoundException.class);
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", new ParameterObject<?>[]{new ParameterObject<ValueObject>("param1", ValueObject.class)}, exceptions);
		method.setModifiers(new int[] {Opcodes.ACC_STATIC, Opcodes.ACC_PUBLIC});
		method.setCode(clsGenerator.lineFactory.methods.invokeStaticMethod("testVoid", null, null, null));
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		//MethodClass obj=(MethodClass) clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", new Class[]{ValueObject.class});
		Object result=mth.invoke(null, new Object[] {new ValueObject(42)});
		assertNull(result);
		assertEquals(1, MethodClass.methodCheck());
	}
	
	@Test
	public void testCheckedExceptionThrown() throws Exception
	{
		ClassObject cls=createClass();
		
		//create method
		List<Class<? extends Throwable>> exceptions=new ArrayList<Class<? extends Throwable>>();
		exceptions.add(ClassNotFoundException.class);
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", new ParameterObject<?>[]{new ParameterObject<ValueObject>("param1", ValueObject.class)}, exceptions);
		method.setModifiers(new int[] {Opcodes.ACC_STATIC, Opcodes.ACC_PUBLIC});
		method.setCode(clsGenerator.lineFactory.methods.throwNewException(ClassNotFoundException.class, "Class not found in there"));
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
//		MethodClass obj=(MethodClass) clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", new Class[]{ValueObject.class});
		
		Object result=null;
		try
		{
			result=mth.invoke(null, new Object[] {new ValueObject(42)});
			result.toString();
		}catch(Exception ex)
		{
			if(ex.getCause()==null || !ex.getCause().getMessage().equals("Class not found in there"))
				throw ex;
		}
		assertNull(result);
		
		
	}
	
	@Test
	public void testUncheckedExceptionThrown() throws Exception
	{
		ClassObject cls=createClass();
		
		//create method
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", new ParameterObject<?>[]{new ParameterObject<ValueObject>("param1", ValueObject.class)});
		method.setModifiers(new int[] {Opcodes.ACC_STATIC, Opcodes.ACC_PUBLIC});
		method.setCode(clsGenerator.lineFactory.methods.throwNewException(RuntimeException.class, "Class not found in there"));
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
//		MethodClass obj=(MethodClass) clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", new Class[]{ValueObject.class});
		
		Object result=null;
		try
		{
			result=mth.invoke(null, new Object[] {new ValueObject(42)});
			result.toString();
		}catch(Exception ex)
		{
			if(ex.getCause()==null || !ex.getCause().getMessage().equals("Class not found in there"))
				throw ex;
		}
		assertNull(result);
		
	}
	
	//TODO: Exception thrown inside the method
}
