package com.progenies.ecg.model.codeparts;

public abstract class IfElseCodePart extends IfCodePart
{
	
	protected CodeBlock elseBlock;
	
	
	


	public CodeBlock getElseBlock() {
		return elseBlock;
	}

	public void setElseBlock(CodeBlock elseBlock) {
		this.elseBlock = elseBlock;
	}

}
