package com.progenies.ecg.asm31.model.codeparts.lines;

import org.objectweb.asm.Opcodes;
import org.objectweb.asm.Type;

import com.progenies.ecg.asm31.util.BytecodeInstructionsUtils;
import com.progenies.ecg.model.MethodObject;


public class ReturnStaticValue<T> extends ConfigurableLineCodePart
{

	private T value;

	ReturnStaticValue(T value)
	{
		this.value=value;
	}


	@Override
	public void generateBytecode()
	{
		if(this.visitor==null )
			throw new IllegalStateException("LinePart has not been configurated, please use ClassGenerator");
		
		if(value==null) //null value
		{
			visitor.visitInsn(Opcodes.ACONST_NULL);
			visitor.visitInsn(Opcodes.ARETURN);
		}
		else
		{
			BytecodeInstructionsUtils.insertValueIntoStack(visitor, value);
			
			//get type from the parent method
			MethodObject parent=(MethodObject) getParent();

			visitor.visitInsn(Type.getType(parent.getReturnType()).getOpcode(Opcodes.IRETURN));
		}
		
	}
	
	@Override
	public boolean isReturnCodePart() {
		return true;
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) {
		this.value=value;
	}

}
