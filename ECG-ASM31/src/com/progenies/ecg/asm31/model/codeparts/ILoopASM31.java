package com.progenies.ecg.asm31.model.codeparts;

import org.objectweb.asm.Label;

public interface ILoopASM31
{
	
	public Label getStartLabel();
	
	public Label getPostIterationLabel();
	
	public Label getEndLabel();

}
