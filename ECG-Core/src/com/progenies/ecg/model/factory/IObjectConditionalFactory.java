package com.progenies.ecg.model.factory;

import com.progenies.ecg.model.ValueReference;
import com.progenies.ecg.model.codeparts.CaseBlock;
import com.progenies.ecg.model.codeparts.CodeBlock;
import com.progenies.ecg.model.codeparts.IfCodePart;
import com.progenies.ecg.model.codeparts.IfElseCodePart;
import com.progenies.ecg.model.codeparts.SwitchCodePart;
import com.progenies.ecg.model.conditions.ICondition;

public interface IObjectConditionalFactory
{
	
	
	public IfCodePart createIfBlock(ICondition condition, CodeBlock block);

	public IfElseCodePart createIfElseBlock(ICondition condition, CodeBlock mainCodeBlock, CodeBlock elseCodeBlock);
	
	public SwitchCodePart createSwitchBlock(ValueReference<?> evaluatedValue, CaseBlock caseBlocks[], CodeBlock defaultBlock);
	
}
