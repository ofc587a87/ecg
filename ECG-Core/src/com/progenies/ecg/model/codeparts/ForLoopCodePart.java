package com.progenies.ecg.model.codeparts;

import com.progenies.ecg.model.ValueReference;
import com.progenies.ecg.model.codeparts.lines.LineCodePart;

/**
 * Represents a typical for(...;...;...) clause.
 * 
 * Any part of the for clause can be null for an infinite loop, including condition.
 * 
 * for-each loops are not supported by this class.
 * @author ofc587a87
 *
 */
public abstract class ForLoopCodePart extends LoopCodePart
{
	/**
	 * Instruction to be executed at the start of the loop. It can be null.
	 */
	protected LineCodePart initialInstruction;
	
	/**
	 * Instruction to be executed after every iteration of the loop.
	 */
	protected LineCodePart postIterationInstruction;
	
	
	
	protected ValueReference<? extends Object> iterableCollection;



	public ValueReference<? extends Object> getIterableCollecion() {
		return iterableCollection;
	}

	public void setIterableCollecion(ValueReference<? extends Object> iterableCollecion) {
		this.iterableCollection = iterableCollecion;
	}

	public LineCodePart getInitialInstruction() {
		return initialInstruction;
	}

	public void setInitialInstruction(LineCodePart initialInstruction) {
		this.initialInstruction = initialInstruction;
	}

	public LineCodePart getPostIterationInstruction() {
		return postIterationInstruction;
	}

	public void setPostIterationInstruction(LineCodePart postIterationInstruction) {
		this.postIterationInstruction = postIterationInstruction;
	}

}
