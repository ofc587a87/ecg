package com.progenies.ecg.generator;

import com.progenies.ecg.model.ClassObject;
import com.progenies.ecg.model.codeparts.lines.LineFactory;
import com.progenies.ecg.model.factory.ObjectFactory;

/**
 * Defines the interface that a Class Generation Tool must implement
 */
public abstract class ClassGenerator
{
	public abstract Class<?> generateClass(ClassObject classObject);

	public final LineFactory lineFactory=createLineFactory();
	
	public final ObjectFactory objectFactory=createObjectFactory();

	protected abstract LineFactory createLineFactory();

	protected abstract ObjectFactory createObjectFactory();

}
