package test.progenies.ecg.asm31.arrayvariables.tridimensional.primitives;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.lang.reflect.Method;

import org.junit.Test;

import test.progenies.ecg.asm31.arrayvariables.tridimensional.BaseTestLocalArrayVariables;

import com.progenies.ecg.model.ClassObject;
import com.progenies.ecg.model.FieldObject;
import com.progenies.ecg.model.MethodObject;
import com.progenies.ecg.model.codeparts.CodeBlock;

public class TestLocalArrayVariables extends BaseTestLocalArrayVariables
{
	@Test(expected=RuntimeException.class)
	public void testCreateNotInitializedArrayVariable() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int[][][].class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar1", int[][][].class));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localVar1"));
		method.setCode(block);
		cls.getMethods().add(method);
		

		Class<?> clsResult=null;
		try
		{
			//TODO: now ASM is throwing an exception, i should be able to know in an internal analysis if the variable has been initialized before load into stack
			clsResult=clsGenerator.generateClass(cls);
			clsResult.newInstance();
		}catch(RuntimeException ex)
		{
			if(ex.getMessage().startsWith("Error at instruction 2: Expected an object reference, but found ."))
				throw ex;
		}
		
		fail("An exception shold have been thrown!!");
	}
	
	@Test
	public void testCreateNullArrayVariable() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int[][][].class);
		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar1", int[][][].class, null));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localVar1"));
		method.setCode(block);
		cls.getMethods().add(method);

		Class<?> clsResult=clsGenerator.generateClass(cls);

		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNull(result);
	}
	
	@Test
	public void testCreateEmptyArrayVariable() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int[][][].class);
		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalArrayVariable("localVar1", int[][][].class, new int[]{5,2,6}));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localVar1"));
		method.setCode(block);
		cls.getMethods().add(method);

		Class<?> clsResult=clsGenerator.generateClass(cls);

		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertTrue(result instanceof int[][][]);
		assertArrayEquals(new int[5][2][6], (int[][][])result);
	}
	
	
	
	@Test
	public void testCreateStaticValuesArrayVariable() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int[][][].class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar1", int[][][].class, new int[][][] {{{1, 2, 3},{1, 2, 3}}, {{1, 2, 3},{1, 2, 3}}}));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localVar1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
				
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertTrue(result instanceof int[][][]);
		assertArrayEquals(new int[][][] {{{1, 2, 3},{1, 2, 3}}, {{1, 2, 3},{1, 2, 3}}}, (int[][][])result);
	}
	
	
	
	
	
	
	
	/*--------------------------------------------
	 *----------- From method invocation --------- 
	 *--------------------------------------------
	 */
	
	
	@Test
	public void testCreateArrayFromLocalMethod() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int[][][].class);
		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVarVal1", int[][][].class));
		block.add(clsGenerator.lineFactory.methods.invokeMethodAndStore("createArray", "this", null, int[][][].class, "localVarVal1"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localVarVal1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);

		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertTrue(result instanceof int[][][]);
		assertArrayEquals(new int[][][] {{{2,4,6},	{3,5,7}, {4,6,8}}, {{2,4,6},	{3,5,7}, {4,6,8}}}, (int[][][])result);
	}
	
	
	
	@Test
	public void testCreateArrayFromExternalInstanceMethod() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int[][][].class);
		
		FieldObject<MethodClass> fld=clsGenerator.objectFactory.fields.createField("localObj", MethodClass.class);
		fld.setInitialize(true);
		cls.getFields().add(fld);
		
		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVarVal1", int[][][].class));
		block.add(clsGenerator.lineFactory.methods.invokeMethodAndStore("createArray", "localObj", null, int[][][].class, "localVarVal1"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localVarVal1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);

		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertTrue(result instanceof int[][][]);
		assertArrayEquals(new int[][][] {{{2,4,6},	{3,5,7}, {4,6,8}}, {{2,4,6},	{3,5,7}, {4,6,8}}}, (int[][][])result);
	}
	
	
	
	@Test
	public void testCreateArrayFromStaticMethod() throws Exception
	{
		ClassObject cls=createClass();
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int[][][].class);
		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVarVal1", int[][][].class));
		block.add(clsGenerator.lineFactory.methods.invokeStaticMethodAndStore("createStaticArray", MethodClass.class, null, int[][][].class, "localVarVal1"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localVarVal1"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);

		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertTrue(result instanceof int[][][]);
		assertArrayEquals(new int[][][] {{{2,4,6,8}, {3,5,7,9}}, {{2,4,6,8}, {3,5,7,9}}}, (int[][][])result);
	}

}
