package com.progenies.ecg.asm31.model.codeparts.lines;

import org.objectweb.asm.Opcodes;

import com.progenies.ecg.asm31.model.codeparts.ILoopASM31;
import com.progenies.ecg.asm31.model.codeparts.SwitchCodePartASM31;
import com.progenies.ecg.asm31.util.GenerationUtils;


public class BreakContinueLinePartASM31  extends ConfigurableLineCodePart
{
	private boolean isBreak;
	
	public static final BreakContinueLinePartASM31 createBreak()
	{
		BreakContinueLinePartASM31 result=new BreakContinueLinePartASM31();
		result.isBreak=true;
		return result;
	}

	public static final BreakContinueLinePartASM31 createContinue()
	{
		BreakContinueLinePartASM31 result=new BreakContinueLinePartASM31();
		result.isBreak=false;
		return result;
	}
	
	
	private BreakContinueLinePartASM31() {}

	@Override
	public void generateBytecode()
	{
		
		//getParent (loops)
		ILoopASM31 loopParent=(ILoopASM31) GenerationUtils.getLoopObject(this.parent);
		if(loopParent!=null)
		{
			if(isBreak)
			{
				//Unconditionally jumps to end
				visitor.visitJumpInsn(Opcodes.GOTO, loopParent.getEndLabel());
			}
			else
			{
				//Unconditionally jumps to postIteration, to start again
				visitor.visitJumpInsn(Opcodes.GOTO, loopParent.getPostIterationLabel());
			}
		}
		else
		{
			SwitchCodePartASM31 switchParent=(SwitchCodePartASM31) GenerationUtils.getSwitchObject(this.parent);
			if(switchParent!=null)
			{
				visitor.visitJumpInsn(Opcodes.GOTO, switchParent.getEndLabel());
			}
			else
				throw new RuntimeException("Not in a loop or switch!");
		}

	}

}
