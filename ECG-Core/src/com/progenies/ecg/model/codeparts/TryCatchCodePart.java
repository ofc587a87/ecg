package com.progenies.ecg.model.codeparts;

import java.util.ArrayList;
import java.util.List;

/**
 * Represents a TryCatch Code Block.
 * 
 * Each catch clause is composed of an Exception and an associated CodeBlock. The Catch clauses are generated
 * in the same order they are added to the list, no ordering are applied to them.
 * At least one CatchBlock must be specified.
 * 
 * The finally block is optional.
 * 
 * @author ofc587a87
 *
 */
public abstract class TryCatchCodePart implements ICodePart
{
	/**
	 * Body block of the try clause. It cannot be null.
	 */
	protected CodeBlock bodyBlock;
	
	/**
	 * Collection of catch Clauses
	 */
	protected List<CatchBlock> catchBlocks=new ArrayList<CatchBlock>();
	
	/**
	 * Finally block of the try-catch clause. It can be null.
	 */
	protected CodeBlock finallyBlock;
	
	
	
	
	
	public CodeBlock getBodyBlock() {
		return bodyBlock;
	}


	public void setBodyBlock(CodeBlock bodyBlock) {
		this.bodyBlock = bodyBlock;
	}


	public List<CatchBlock> getCatchBlocks() {
		return catchBlocks;
	}


	public void setCatchBlocks(List<CatchBlock> catchBlocks) {
		this.catchBlocks = catchBlocks;
	}


	public CodeBlock getFinallyBlock() {
		return finallyBlock;
	}


	public void setFinallyBlock(CodeBlock finallyBlock) {
		this.finallyBlock = finallyBlock;
	}


	/**
	 * Represents each one of the catch clauses of the try-catch block.
	 * @author ofc587a87
	 *
	 */
	public static abstract class CatchBlock implements ICodePart
	{
		/**
		 * CodeBlock of the catch clause. It can be null.
		 */
		protected CodeBlock catchBody;
		
		/**
		 * Exception class to be captured
		 */
		protected Class<? extends Throwable> exceptionClass;

		public CodeBlock getCatchBody() {
			return catchBody;
		}

		public void setCatchBody(CodeBlock catchBody) {
			this.catchBody = catchBody;
		}

		public Class<? extends Throwable> getExceptionClass() {
			return exceptionClass;
		}

		public void setExceptionClass(Class<? extends Throwable> exceptionClass) {
			this.exceptionClass = exceptionClass;
		}
		
	}

}
