package com.progenies.ecg.model.codeparts;

/**
 * Interface that any block capable of generate bytecode instructions should implement  
 * @author ofc587a87
 *
 */
public interface ICodePart
{
	
	public void generateBytecode();

	public boolean isReturnCodePart();

}
