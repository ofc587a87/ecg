package test.progenies.ecg.asm31.variables.set;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;

import org.junit.Test;

import test.progenies.ecg.asm31.variables.BaseTestInstanceVariables;
import test.progenies.ecg.asm31.variables.InstanceFieldClass;

import com.progenies.ecg.model.ClassObject;
import com.progenies.ecg.model.FieldObject;
import com.progenies.ecg.model.MethodObject;
import com.progenies.ecg.model.ParameterObject;
import com.progenies.ecg.model.codeparts.CodeBlock;

public class TestInstanceVariables extends BaseTestInstanceVariables {

	
	
	@Test
	public void testSetFieldWithValueFromStaticValue() throws Exception
	{
		ClassObject cls=createClass();
		
		//uninitialized instance field
		FieldObject<Long> fld=clsGenerator.objectFactory.fields.createField("instanceField2", long.class);
		cls.getFields().add(fld);
		
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", long.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.setVariableValue("instanceField2", 65l));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceField2"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Long);
		assertEquals(65l, result);
	}
	
	@Test
	public void testSetFieldWithValueFromStaticVar() throws Exception
	{
		ClassObject cls=createClass();
		
		//uninitialized instance field
		FieldObject<Long> fld=clsGenerator.objectFactory.fields.createField("instanceField2", long.class);
		cls.getFields().add(fld);
		
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", long.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.setVariableValueFromVariable("instanceField2", "staticField"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceField2"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Long);
		assertEquals(88l, result);
	}
	
	@Test
	public void testSetFieldWithValueFromInstanceVar() throws Exception
	{
		ClassObject cls=createClass();
		
		//uninitialized instance field
		FieldObject<Long> fld=clsGenerator.objectFactory.fields.createField("instanceField2", long.class);
		cls.getFields().add(fld);
		
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", long.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.setVariableValueFromVariable("instanceField2", "instanceField"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceField2"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Long);
		assertEquals(56l, result);
	}
	
	@Test
	public void testSetFieldWithValueFromLocalVar() throws Exception
	{
		ClassObject cls=createClass();
		
		//uninitialized instance field
		FieldObject<Long> fld=clsGenerator.objectFactory.fields.createField("instanceField2", long.class);
		cls.getFields().add(fld);
		
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", long.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar2", long.class, 76l));
		block.add(clsGenerator.lineFactory.variables.setVariableValueFromVariable("instanceField2", "localVar2"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceField2"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Long);
		assertEquals(76l, result);
	}
	
	@Test
	public void testSetVariableWithValueFromParameter() throws Exception
	{
		ClassObject cls=createClass();
		
		//uninitialized instance field
		FieldObject<Long> fld=clsGenerator.objectFactory.fields.createField("instanceField2", long.class);
		cls.getFields().add(fld);
		
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", new ParameterObject<?>[] {new ParameterObject<Long>("paramVar", long.class)}, long.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.setVariableValueFromVariable("instanceField2", "paramVar"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceField2"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", new Class[] {long.class});
		Object result=mth.invoke(obj, new Object[] { 39l });
		assertTrue(result instanceof Long);
		assertEquals(39l, result);
	}
	
	
	

	@Test
	public void testSetVariableWithValueFromFieldInLocalObject() throws Exception
	{
		ClassObject cls=createClass();
		
		
		//uninitialized instance field
		FieldObject<Long> fld=clsGenerator.objectFactory.fields.createField("instanceField2", long.class);
		cls.getFields().add(fld);
		
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", long.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariableAndInitialize("localObj1", InstanceFieldClass.class));
		block.add(clsGenerator.lineFactory.variables.setVariableValueFromObjectField("instanceField2", "localObj1", "longField", long.class));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceField2"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Long);
		assertEquals(6l, result);
	}
	
	@Test
	public void testSetVariableWithValueFromFieldInFieldObject() throws Exception
	{
		ClassObject cls=createClass();
		
		//uninitialized instance field
		FieldObject<Long> fld1=clsGenerator.objectFactory.fields.createField("instanceField2", long.class);
		cls.getFields().add(fld1);
		
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", long.class);
		
		FieldObject<InstanceFieldClass> fld=clsGenerator.objectFactory.fields.createField("instanceObj", InstanceFieldClass.class);
		fld.setInitialize(true);
		cls.getFields().add(fld);
		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.setVariableValueFromObjectField("instanceField2", "instanceObj", "longField", long.class));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceField2"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Long);
		assertEquals(6l, result);
	}
	
	
	
	@Test
	public void testSetVariableWithValueFromStaticFieldInClass() throws Exception
	{
		ClassObject cls=createClass();
		
		//uninitialized instance field
		FieldObject<Long> fld=clsGenerator.objectFactory.fields.createField("instanceField2", long.class);
		cls.getFields().add(fld);
		
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", long.class);
		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.setVariableValueFromStaticClassField("instanceField2", InstanceFieldClass.class, "staticLongField", long.class));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceField2"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Long);
		assertEquals(7l, result);
	}
	

}
