package test.progenies.ecg.asm31.conditionals;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.lang.reflect.Method;

import org.junit.Before;
import org.junit.Test;

import com.progenies.ecg.generator.ClassGenerator;
import com.progenies.ecg.generator.ClassGeneratorFactory;
import com.progenies.ecg.model.ClassObject;
import com.progenies.ecg.model.ConstructorObject;
import com.progenies.ecg.model.MethodObject;
import com.progenies.ecg.model.ValueReference;
import com.progenies.ecg.model.codeparts.CaseBlock;
import com.progenies.ecg.model.codeparts.CodeBlock;
import com.progenies.ecg.model.codeparts.lines.LineFactory;

public class TestSwitchCodePart {

	protected ClassGenerator clsGenerator;
	private static int counter = 0;
	
	
	
	
	@Test
	public void testCase1Switch() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localDecider", int.class, 1));
		
		CaseBlock case1=createIncrementCase(1, "localValue", 5);
		CaseBlock case2=createIncrementCase(2, "localValue", 50);
		CaseBlock case3=createIncrementCase(3, "localValue", 60);
		CaseBlock case4=createIncrementCase(40, "localValue", 70);
		CodeBlock defaultBlock=createIncrementCodeBlock("localValue", 500);

		block.add(clsGenerator.objectFactory.conditionals.createSwitchBlock(ValueReference.forVariable("localDecider"), new CaseBlock[] {case1, case2, case3, case4}, defaultBlock));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		checkResult(2 + 5, cls);
	}


	
	
	
	@Test
	public void testCase2Switch() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localDecider", int.class, 2));
		
		CaseBlock case1=createIncrementCase(1, "localValue", 5);
		CaseBlock case2=createIncrementCase(2, "localValue", 50);
		CaseBlock case3=createIncrementCase(3, "localValue", 60);
		CaseBlock case4=createIncrementCase(40, "localValue", 70);
		CodeBlock defaultBlock=createIncrementCodeBlock("localValue", 500);

		block.add(clsGenerator.objectFactory.conditionals.createSwitchBlock(ValueReference.forVariable("localDecider"), new CaseBlock[] {case1, case2, case3, case4}, defaultBlock));
		
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		
		checkResult(2 + 50, cls);
	}
	
	@Test
	public void testCase2NoBreakSwitch() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localDecider", int.class, 2));
		
		CaseBlock case1=createIncrementCase(1, "localValue", 5);
		CaseBlock case2=createIncrementCaseNoBreak(2, "localValue", 50);
		CaseBlock case3=createIncrementCase(3, "localValue", 60);
		CaseBlock case4=createIncrementCase(40, "localValue", 70);
		CodeBlock defaultBlock=createIncrementCodeBlock("localValue", 500);

		block.add(clsGenerator.objectFactory.conditionals.createSwitchBlock(ValueReference.forVariable("localDecider"), new CaseBlock[] {case1, case2, case3, case4}, defaultBlock));
		
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		
		checkResult(2 + 50 + 60, cls);
	}
	
	@Test
	public void testCase3Switch() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localDecider", int.class, 3));
		
		CaseBlock case1=createIncrementCase(1, "localValue", 5);
		CaseBlock case2=createIncrementCase(2, "localValue", 50);
		CaseBlock case3=createIncrementCase(3, "localValue", 60);
		CaseBlock case4=createIncrementCase(40, "localValue", 70);
		CodeBlock defaultBlock=createIncrementCodeBlock("localValue", 500);

		block.add(clsGenerator.objectFactory.conditionals.createSwitchBlock(ValueReference.forVariable("localDecider"), new CaseBlock[] {case1, case2, case3, case4}, defaultBlock));
		
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		checkResult(2 + 60, cls);
	}
	
	@Test
	public void testCase4Switch() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localDecider", int.class, 40));
		
		CaseBlock case1=createIncrementCase(1, "localValue", 5);
		CaseBlock case2=createIncrementCase(2, "localValue", 50);
		CaseBlock case3=createIncrementCase(3, "localValue", 60);
		CaseBlock case4=createIncrementCase(40, "localValue", 70);
		CodeBlock defaultBlock=createIncrementCodeBlock("localValue", 500);

		block.add(clsGenerator.objectFactory.conditionals.createSwitchBlock(ValueReference.forVariable("localDecider"), new CaseBlock[] {case1, case2, case3, case4}, defaultBlock));
		
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		checkResult(2 + 70, cls);
	}
	
	
	@Test
	public void testDefaultSwitch() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localDecider", int.class, 9));
		
		CaseBlock case1=createIncrementCase(1, "localValue", 5);
		CaseBlock case2=createIncrementCase(2, "localValue", 50);
		CaseBlock case3=createIncrementCase(3, "localValue", 60);
		CaseBlock case4=createIncrementCase(40, "localValue", 70);
		CodeBlock defaultBlock=createIncrementCodeBlock("localValue", 500);

		block.add(clsGenerator.objectFactory.conditionals.createSwitchBlock(ValueReference.forVariable("localDecider"), new CaseBlock[] {case1, case2, case3, case4}, defaultBlock));
		
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		checkResult(2 + 500, cls);
	}
	
	
	@Test
	public void testReverseCase1Switch() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localDecider", int.class, 40));
		
		CaseBlock case1=createIncrementCase(40, "localValue", 5);
		CaseBlock case2=createIncrementCase(3, "localValue", 50);
		CaseBlock case3=createIncrementCase(2, "localValue", 60);
		CaseBlock case4=createIncrementCase(1, "localValue", 70);
		CodeBlock defaultBlock=createIncrementCodeBlock("localValue", 500);

		block.add(clsGenerator.objectFactory.conditionals.createSwitchBlock(ValueReference.forVariable("localDecider"), new CaseBlock[] {case1, case2, case3, case4}, defaultBlock));
		
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		checkResult(2 + 5, cls);
	}
	
	
	@Test
	public void testReverseCase2Switch() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localDecider", int.class, 3));
		
		CaseBlock case1=createIncrementCase(40, "localValue", 5);
		CaseBlock case2=createIncrementCase(3, "localValue", 50);
		CaseBlock case3=createIncrementCase(2, "localValue", 60);
		CaseBlock case4=createIncrementCase(1, "localValue", 70);
		CodeBlock defaultBlock=createIncrementCodeBlock("localValue", 500);

		block.add(clsGenerator.objectFactory.conditionals.createSwitchBlock(ValueReference.forVariable("localDecider"), new CaseBlock[] {case1, case2, case3, case4}, defaultBlock));
		
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		checkResult(2 + 50, cls);
	}
	
	@Test
	public void testReverseCase2NoBreakSwitch() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localDecider", int.class, 3));
		
		CaseBlock case1=createIncrementCase(40, "localValue", 5);
		CaseBlock case2=createIncrementCaseNoBreak(3, "localValue", 50);
		CaseBlock case3=createIncrementCase(2, "localValue", 60);
		CaseBlock case4=createIncrementCase(1, "localValue", 70);
		CodeBlock defaultBlock=createIncrementCodeBlock("localValue", 500);

		block.add(clsGenerator.objectFactory.conditionals.createSwitchBlock(ValueReference.forVariable("localDecider"), new CaseBlock[] {case1, case2, case3, case4}, defaultBlock));
		
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		checkResult(2 + 50 + 60, cls);
	}
	
	@Test
	public void testReverseCase3Switch() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localDecider", int.class, 2));
		
		CaseBlock case1=createIncrementCase(40, "localValue", 5);
		CaseBlock case2=createIncrementCase(3, "localValue", 50);
		CaseBlock case3=createIncrementCase(2, "localValue", 60);
		CaseBlock case4=createIncrementCase(1, "localValue", 70);
		CodeBlock defaultBlock=createIncrementCodeBlock("localValue", 500);

		block.add(clsGenerator.objectFactory.conditionals.createSwitchBlock(ValueReference.forVariable("localDecider"), new CaseBlock[] {case1, case2, case3, case4}, defaultBlock));
		
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		checkResult(2 + 60, cls);
	}
	
	@Test
	public void testReverseCase4Switch() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localDecider", int.class, 1));
		
		CaseBlock case1=createIncrementCase(40, "localValue", 5);
		CaseBlock case2=createIncrementCase(3, "localValue", 50);
		CaseBlock case3=createIncrementCase(2, "localValue", 60);
		CaseBlock case4=createIncrementCase(1, "localValue", 70);
		CodeBlock defaultBlock=createIncrementCodeBlock("localValue", 500);

		block.add(clsGenerator.objectFactory.conditionals.createSwitchBlock(ValueReference.forVariable("localDecider"), new CaseBlock[] {case1, case2, case3, case4}, defaultBlock));
		
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		checkResult(2 + 70, cls);
	}
	
	
	@Test
	public void testReverseDefaultSwitch() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localDecider", int.class, 9));
		
		CaseBlock case1=createIncrementCase(40, "localValue", 5);
		CaseBlock case2=createIncrementCase(3, "localValue", 50);
		CaseBlock case3=createIncrementCase(2, "localValue", 60);
		CaseBlock case4=createIncrementCase(1, "localValue", 70);
		CodeBlock defaultBlock=createIncrementCodeBlock("localValue", 500);

		block.add(clsGenerator.objectFactory.conditionals.createSwitchBlock(ValueReference.forVariable("localDecider"), new CaseBlock[] {case1, case2, case3, case4}, defaultBlock));
		
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		checkResult(2 + 500, cls);
	}
	
	
	

	@Test
	public void testShuffledKeysCase1Switch() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localDecider", int.class, 40));
		
		CaseBlock case1=createIncrementCase(40, "localValue", 5);
		CaseBlock case2=createIncrementCase(1, "localValue", 50);
		CaseBlock case3=createIncrementCase(3, "localValue", 60);
		CaseBlock case4=createIncrementCase(2, "localValue", 70);
		CodeBlock defaultBlock=createIncrementCodeBlock("localValue", 500);

		block.add(clsGenerator.objectFactory.conditionals.createSwitchBlock(ValueReference.forVariable("localDecider"), new CaseBlock[] {case1, case2, case3, case4}, defaultBlock));
		
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		checkResult(2 + 5, cls);
	}
	
	
	@Test
	public void testShuffledKeysCase2Switch() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localDecider", int.class, 1));
		
		CaseBlock case1=createIncrementCase(40, "localValue", 5);
		CaseBlock case2=createIncrementCase(1, "localValue", 50);
		CaseBlock case3=createIncrementCase(3, "localValue", 60);
		CaseBlock case4=createIncrementCase(2, "localValue", 70);
		CodeBlock defaultBlock=createIncrementCodeBlock("localValue", 500);

		block.add(clsGenerator.objectFactory.conditionals.createSwitchBlock(ValueReference.forVariable("localDecider"), new CaseBlock[] {case1, case2, case3, case4}, defaultBlock));
		
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		checkResult(2 + 50, cls);
	}
	
	
	@Test
	public void testShuffledKeysCase2NoBreakSwitch() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localDecider", int.class, 1));
		
		CaseBlock case1=createIncrementCase(40, "localValue", 5);
		CaseBlock case2=createIncrementCaseNoBreak(1, "localValue", 50);
		CaseBlock case3=createIncrementCase(3, "localValue", 60);
		CaseBlock case4=createIncrementCase(2, "localValue", 70);
		CodeBlock defaultBlock=createIncrementCodeBlock("localValue", 500);

		block.add(clsGenerator.objectFactory.conditionals.createSwitchBlock(ValueReference.forVariable("localDecider"), new CaseBlock[] {case1, case2, case3, case4}, defaultBlock));
		
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		checkResult(2 + 50 + 60, cls);
	}
	
	@Test
	public void testShuffledKeysCase3Switch() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localDecider", int.class, 3));
		
		CaseBlock case1=createIncrementCase(40, "localValue", 5);
		CaseBlock case2=createIncrementCase(1, "localValue", 50);
		CaseBlock case3=createIncrementCase(3, "localValue", 60);
		CaseBlock case4=createIncrementCase(2, "localValue", 70);
		CodeBlock defaultBlock=createIncrementCodeBlock("localValue", 500);

		block.add(clsGenerator.objectFactory.conditionals.createSwitchBlock(ValueReference.forVariable("localDecider"), new CaseBlock[] {case1, case2, case3, case4}, defaultBlock));
		
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		checkResult(2 + 60, cls);
	}
	
	@Test
	public void testShuffledKeysCase4Switch() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localDecider", int.class, 2));
		
		CaseBlock case1=createIncrementCase(40, "localValue", 5);
		CaseBlock case2=createIncrementCase(1, "localValue", 50);
		CaseBlock case3=createIncrementCase(3, "localValue", 60);
		CaseBlock case4=createIncrementCase(2, "localValue", 70);
		CodeBlock defaultBlock=createIncrementCodeBlock("localValue", 500);

		block.add(clsGenerator.objectFactory.conditionals.createSwitchBlock(ValueReference.forVariable("localDecider"), new CaseBlock[] {case1, case2, case3, case4}, defaultBlock));
		
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		checkResult(2 + 70, cls);
	}
	
	
	@Test
	public void testShuffledKeysDefaultSwitch() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localDecider", int.class, 9));
		
		CaseBlock case1=createIncrementCase(40, "localValue", 5);
		CaseBlock case2=createIncrementCase(1, "localValue", 50);
		CaseBlock case3=createIncrementCase(3, "localValue", 60);
		CaseBlock case4=createIncrementCase(2, "localValue", 70);
		CodeBlock defaultBlock=createIncrementCodeBlock("localValue", 500);

		block.add(clsGenerator.objectFactory.conditionals.createSwitchBlock(ValueReference.forVariable("localDecider"), new CaseBlock[] {case1, case2, case3, case4}, defaultBlock));
		
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		checkResult(2 + 500, cls);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Before
	public void setUp() throws Exception {
		clsGenerator=ClassGeneratorFactory.getClassGenerator();
		assertNotNull(clsGenerator);
	}

	
	
	

	protected ClassObject createClass()
	{
		counter++;
		int index=counter;
		LineFactory lineFactory=clsGenerator.lineFactory;
		
		String subpackage=this.getClass().getPackage().getName().substring(this.getClass().getPackage().getName().lastIndexOf('.')+1);
		ClassObject cls=clsGenerator.objectFactory.classes.createClass("test.conditionals."+subpackage+".SwitchTestClass"+index);
		cls.setSuperClass(Object.class);

		
		//override default constructor
		ConstructorObject consObject=clsGenerator.objectFactory.constructors.createConstructor();
		consObject.setCode(lineFactory.methods.invokeSuperConstructor());
		cls.getConstructors().add(consObject);
		
		
		return cls;		
	}
	
	private void checkResult(int expectedResult, ClassObject cls) throws Exception
	{
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(expectedResult, result);
	}
	
	
	
	private CaseBlock createIncrementCase(int key, String varName, int ammount)
	{
		CodeBlock bodyBlock=createIncrementCodeBlock(varName, ammount);
		bodyBlock.add(clsGenerator.objectFactory.loops.createBreak());
		return new CaseBlock(key, bodyBlock);
	}

	private CaseBlock createIncrementCaseNoBreak(int key, String varName, int ammount)
	{
		return new CaseBlock(key, createIncrementCodeBlock(varName, ammount));
	}
	
	private CodeBlock createIncrementCodeBlock(String varName, int ammount)
	{
		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable(varName, ammount));
		return bodyBlock;
	}

}
