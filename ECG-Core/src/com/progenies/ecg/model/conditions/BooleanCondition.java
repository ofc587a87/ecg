package com.progenies.ecg.model.conditions;

import com.progenies.ecg.model.ValueReference;


public class BooleanCondition <T> implements ICondition
{
	public static enum ConditionType
	{
		//single value conditions
		IS_TRUE, IS_FALSE,
		IS_NULL, IS_NOT_NULL,
		
		//compare two values conditions
		IS_GREATER_THAN, IS_GREATER_OR_EQUALS_TO,
		IS_LESS_THAN, IS_LESS_OR_EQUALS_TO,
		IS_EQUALS_TO, IS_NOT_EQUALS_TO;
		
		public boolean isSingleValue()
		{
			switch(this)
			{
				case IS_TRUE:
				case IS_FALSE:
				case IS_NULL:
				case IS_NOT_NULL:
					return true;
				default:
					return false;
			}
		}
		
	}
	

	protected ValueReference<T> conditionValue;
	protected ValueReference<? extends T> secundaryConditionValue;
	protected ConditionType conditionType;
	
	
	
	public BooleanCondition(ValueReference<T> conditionValue, ConditionType conditionType)
	{
		//check sanity: Only single value condition type allowed
		if(!conditionType.isSingleValue())
			throw new RuntimeException("Not supported comparisons for single value conditions");
		
		this.conditionValue=conditionValue;
		this.conditionType=conditionType;
	}
	
	
	public BooleanCondition(ValueReference<T> conditionValue, ValueReference<? extends T> secundaryConditionValue, ConditionType conditionType)
	{
		//check sanity: no single value condition type allowed
		if(conditionType.isSingleValue())
			throw new RuntimeException("Not supported single value evaluations for two values");
		
		this.conditionValue=conditionValue;
		this.secundaryConditionValue=secundaryConditionValue;
		this.conditionType=conditionType;
	}
	

	public ValueReference<T> getConditionValue() {
		return conditionValue;
	}

	public void setConditionCode(ValueReference<T> conditionValue) {
		this.conditionValue = conditionValue;
	}



	public ValueReference<? extends T> getSecundaryConditionValue() {
		return secundaryConditionValue;
	}


	public ConditionType getConditionType() {
		return conditionType;
	}
}
