package test.progenies.ecg.asm31.conditionals;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.lang.reflect.Method;

import org.junit.Before;
import org.junit.Test;

import com.progenies.ecg.generator.ClassGenerator;
import com.progenies.ecg.generator.ClassGeneratorFactory;
import com.progenies.ecg.model.ClassObject;
import com.progenies.ecg.model.ConstructorObject;
import com.progenies.ecg.model.MethodObject;
import com.progenies.ecg.model.ValueReference;
import com.progenies.ecg.model.codeparts.CodeBlock;
import com.progenies.ecg.model.codeparts.IfCodePart;
import com.progenies.ecg.model.codeparts.lines.LineFactory;
import com.progenies.ecg.model.conditions.BooleanCondition;
import com.progenies.ecg.model.conditions.ICondition;

public class TestCompareIfBlocks {

	protected ClassGenerator clsGenerator;
	private static int counter = 0;
	
	
	
	@Test
	public void testCompareGT_True() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("compare1", int.class, 5));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("compare2", int.class, 3));

		ICondition condition=new BooleanCondition<Object>(ValueReference.forVariable("compare1"), ValueReference.forVariable("compare2"),
															BooleanCondition.ConditionType.IS_GREATER_THAN);

		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 23));
		
		IfCodePart ifCode=clsGenerator.objectFactory.conditionals.createIfBlock(condition, bodyBlock);
		block.add(ifCode);
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(25, result);
	}
	
	
	@Test
	public void testCompareGT_False() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("compare1", int.class, 3));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("compare2", int.class, 5));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("compare3", int.class, 3));

		ICondition condition1=new BooleanCondition<Object>(ValueReference.forVariable("compare1"), ValueReference.forVariable("compare2"),
				BooleanCondition.ConditionType.IS_GREATER_THAN);
		ICondition condition2=new BooleanCondition<Object>(ValueReference.forVariable("compare1"), ValueReference.forVariable("compare3"),
				BooleanCondition.ConditionType.IS_GREATER_THAN);

		CodeBlock bodyBlock1=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock1.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 23));
		IfCodePart ifCode1=clsGenerator.objectFactory.conditionals.createIfBlock(condition1, bodyBlock1);
		block.add(ifCode1);
		
		CodeBlock bodyBlock2=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock2.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 24));
		IfCodePart ifCode2=clsGenerator.objectFactory.conditionals.createIfBlock(condition2, bodyBlock2);
		block.add(ifCode2);
		
		
		
		
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(2, result);
	}
	
	@Test
	public void testCompareGE_True() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("compare1", int.class, 5));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("compare2", int.class, 3));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("compare3", int.class, 3));

		ICondition condition1=new BooleanCondition<Object>(ValueReference.forVariable("compare1"), ValueReference.forVariable("compare2"),
				BooleanCondition.ConditionType.IS_GREATER_OR_EQUALS_TO);
		ICondition condition2=new BooleanCondition<Object>(ValueReference.forVariable("compare1"), ValueReference.forVariable("compare3"),
				BooleanCondition.ConditionType.IS_GREATER_OR_EQUALS_TO);

		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 23));
		
		CodeBlock bodyBlock1=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock1.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 23));
		IfCodePart ifCode1=clsGenerator.objectFactory.conditionals.createIfBlock(condition1, bodyBlock1);
		block.add(ifCode1);
		
		CodeBlock bodyBlock2=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock2.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 24));
		IfCodePart ifCode2=clsGenerator.objectFactory.conditionals.createIfBlock(condition2, bodyBlock2);
		block.add(ifCode2);
		
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(49, result);
	}
	
	
	@Test
	public void testCompareGE_False() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("compare1", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("compare2", int.class, 3));

		ICondition condition=new BooleanCondition<Object>(ValueReference.forVariable("compare1"), ValueReference.forVariable("compare2"),
															BooleanCondition.ConditionType.IS_GREATER_OR_EQUALS_TO);

		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 23));
		
		IfCodePart ifCode=clsGenerator.objectFactory.conditionals.createIfBlock(condition, bodyBlock);
		block.add(ifCode);
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(2, result);
	}
	
	
	
	@Test
	public void testCompareLT_True() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("compare1", int.class, 3));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("compare2", int.class, 5));

		ICondition condition=new BooleanCondition<Object>(ValueReference.forVariable("compare1"), ValueReference.forVariable("compare2"),
															BooleanCondition.ConditionType.IS_LESS_THAN);

		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 23));
		
		IfCodePart ifCode=clsGenerator.objectFactory.conditionals.createIfBlock(condition, bodyBlock);
		block.add(ifCode);
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(25, result);
	}
	
	
	@Test
	public void testCompareLT_False() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("compare1", int.class, 5));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("compare2", int.class, 3));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("compare3", int.class, 5));

		ICondition condition1=new BooleanCondition<Object>(ValueReference.forVariable("compare1"), ValueReference.forVariable("compare2"),
				BooleanCondition.ConditionType.IS_LESS_THAN);
		ICondition condition2=new BooleanCondition<Object>(ValueReference.forVariable("compare1"), ValueReference.forVariable("compare3"),
				BooleanCondition.ConditionType.IS_LESS_THAN);

		CodeBlock bodyBlock1=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock1.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 23));
		IfCodePart ifCode1=clsGenerator.objectFactory.conditionals.createIfBlock(condition1, bodyBlock1);
		block.add(ifCode1);
		
		CodeBlock bodyBlock2=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock2.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 24));
		IfCodePart ifCode2=clsGenerator.objectFactory.conditionals.createIfBlock(condition2, bodyBlock2);
		block.add(ifCode2);
		
		
		
		
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(2, result);
	}
	
	@Test
	public void testCompareLE_True() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("compare1", int.class, 3));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("compare2", int.class, 5));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("compare3", int.class, 5));

		ICondition condition1=new BooleanCondition<Object>(ValueReference.forVariable("compare1"), ValueReference.forVariable("compare2"),
				BooleanCondition.ConditionType.IS_LESS_OR_EQUALS_TO);
		ICondition condition2=new BooleanCondition<Object>(ValueReference.forVariable("compare1"), ValueReference.forVariable("compare3"),
				BooleanCondition.ConditionType.IS_LESS_OR_EQUALS_TO);

		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 23));
		
		CodeBlock bodyBlock1=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock1.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 23));
		IfCodePart ifCode1=clsGenerator.objectFactory.conditionals.createIfBlock(condition1, bodyBlock1);
		block.add(ifCode1);
		
		CodeBlock bodyBlock2=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock2.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 24));
		IfCodePart ifCode2=clsGenerator.objectFactory.conditionals.createIfBlock(condition2, bodyBlock2);
		block.add(ifCode2);
		
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(49, result);
	}
	
	
	@Test
	public void testCompareLE_False() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("compare1", int.class, 3));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("compare2", int.class, 2));

		ICondition condition=new BooleanCondition<Object>(ValueReference.forVariable("compare1"), ValueReference.forVariable("compare2"),
															BooleanCondition.ConditionType.IS_LESS_OR_EQUALS_TO);

		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 23));
		
		IfCodePart ifCode=clsGenerator.objectFactory.conditionals.createIfBlock(condition, bodyBlock);
		block.add(ifCode);
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(2, result);
	}
	
	
	
	@Test
	public void testCompareEQ_True() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("compare1", int.class, 3));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("compare2", int.class, 3));

		ICondition condition=new BooleanCondition<Object>(ValueReference.forVariable("compare1"), ValueReference.forVariable("compare2"),
															BooleanCondition.ConditionType.IS_EQUALS_TO);

		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 23));
		
		IfCodePart ifCode=clsGenerator.objectFactory.conditionals.createIfBlock(condition, bodyBlock);
		block.add(ifCode);
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(25, result);
	}
	
	@Test
	public void testCompareEQ_False() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("compare1", int.class, 3));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("compare2", int.class, 2));

		ICondition condition=new BooleanCondition<Object>(ValueReference.forVariable("compare1"), ValueReference.forVariable("compare2"),
															BooleanCondition.ConditionType.IS_EQUALS_TO);

		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 23));
		
		IfCodePart ifCode=clsGenerator.objectFactory.conditionals.createIfBlock(condition, bodyBlock);
		block.add(ifCode);
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(2, result);
	}
	
	@Test
	public void testCompareNE_True() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("compare1", int.class, 3));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("compare2", int.class, 2));

		ICondition condition=new BooleanCondition<Object>(ValueReference.forVariable("compare1"), ValueReference.forVariable("compare2"),
															BooleanCondition.ConditionType.IS_NOT_EQUALS_TO);

		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 23));
		
		IfCodePart ifCode=clsGenerator.objectFactory.conditionals.createIfBlock(condition, bodyBlock);
		block.add(ifCode);
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(25, result);
	}
	
	@Test
	public void testCompareNE_False() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("compare1", int.class, 3));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("compare2", int.class, 3));

		ICondition condition=new BooleanCondition<Object>(ValueReference.forVariable("compare1"), ValueReference.forVariable("compare2"),
															BooleanCondition.ConditionType.IS_NOT_EQUALS_TO);

		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 23));
		
		IfCodePart ifCode=clsGenerator.objectFactory.conditionals.createIfBlock(condition, bodyBlock);
		block.add(ifCode);
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(2, result);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Before
	public void setUp() throws Exception {
		clsGenerator=ClassGeneratorFactory.getClassGenerator();
		assertNotNull(clsGenerator);
	}

	
	
	

	protected ClassObject createClass()
	{
		counter++;
		int index=counter;
		LineFactory lineFactory=clsGenerator.lineFactory;
		
		String subpackage=this.getClass().getPackage().getName().substring(this.getClass().getPackage().getName().lastIndexOf('.')+1);
		ClassObject cls=clsGenerator.objectFactory.classes.createClass("test.conditionals."+subpackage+".IfCompareTestClass"+index);
		cls.setSuperClass(Object.class);

		
		//override default constructor
		ConstructorObject consObject=clsGenerator.objectFactory.constructors.createConstructor();
		consObject.setCode(lineFactory.methods.invokeSuperConstructor());
		cls.getConstructors().add(consObject);
		
		
		return cls;
		
	}

}
