package com.progenies.ecg.model.factory;

import com.progenies.ecg.model.FieldObject;
import com.progenies.ecg.model.ValueReference;

public interface IObjectFieldFactory
{
	
	

	
	
	/**
	 * Creates a new Field Object. By default, it will have access protected, and wont generate getter and setter method.
	 * @param name name of the field. No duplication check will be done.
	 * @param fieldType Class type of the field. For primitives, use wrapper classes.
	 * @return Field Object
	 */
	public <T> FieldObject<T> createField(String name, Class<T> fieldType);

	
	public <T> FieldObject<T> createFieldWithDefaultValue(String name, Class<T> fieldType, T value);

	/**
	 * Creates a new Field Object. By default, it will have access protected.
	 * @param name name of the field. No duplication check will be done.
	 * @param fieldType Class type of the field. For primitives, use wrapper classes.
	 * @param generateAccessorMethods Marks if the generation if setter/getter methods will be needed.
	 * @return Field Object
	 */
	public <T> FieldObject<T> createField(String name, Class<T> fieldType, boolean generateAccessorMethods);

	public <T> FieldObject<T> createFieldWithDefaultValue(String name, Class<T> fieldType, T value, boolean generateAccessorMethods);


	public <T> FieldObject<T> createFieldArray(String name, Class<T> fieldType);

	public <T> FieldObject<T> createFieldArray(String name, Class<T> fieldType, boolean generateAccessorMethods);

	public <T> FieldObject<T> createFieldArrayWithDefaultValue(String name, Class<T> fieldType, T value);

	public <T> FieldObject<T> createFieldArrayWithDefaultValue(String name, Class<T> fieldType, T value, boolean generateAccessorMethods);

	public <T> FieldObject<T> createFieldArray(String name, Class<T> fieldType, ValueReference<?> varReference[]);

	public <T> FieldObject<T> createFieldArray(String name, Class<T> fieldType, ValueReference<?> varReference[], boolean generateAccessorMethods);

	public <T> FieldObject<T> createFieldArray(String name, Class<T> fieldType, ValueReference<?> varReference[][]);

	public <T> FieldObject<T> createFieldArray(String name, Class<T> fieldType, ValueReference<?> varReference[][], boolean generateAccessorMethods);

	public <T> FieldObject<T> createFieldArray(String name, Class<T> fieldType, int arraySize);

	public <T> FieldObject<T> createFieldArray(String name, Class<T> fieldType, int arraySize, boolean generateAccessorMethods);

	public <T> FieldObject<T> createFieldArray(String name, Class<T> fieldType, int[] matrixSize);

	public <T> FieldObject<T> createFieldArray(String name, Class<T> fieldType, int[] matrixSize, boolean generateAccessorMethods);

}
