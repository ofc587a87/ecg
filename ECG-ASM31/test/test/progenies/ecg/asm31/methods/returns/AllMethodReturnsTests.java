package test.progenies.ecg.asm31.methods.returns;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestInternalLocalMethods.class, TestInternalStaticMethods.class,
	TestExternalLocalMethods.class, TestExternalFieldMethods.class, TestExternalStaticMethods.class})
public class AllMethodReturnsTests {

}
