package test.progenies.ecg.asm31.methods.exceptions;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import test.progenies.ecg.asm31.methods.BaseTestExternalLocalMethods;

import com.progenies.ecg.model.ClassObject;
import com.progenies.ecg.model.MethodObject;
import com.progenies.ecg.model.ParameterObject;

/**
 * Test method invocation, being the methods external to the class, received as parameters (same as local variables)
 * @author ofc587a87
 *
 */
public class TestExternalLocalMethods  extends BaseTestExternalLocalMethods {

	
	
	/* --------------------------------------------- */
	/* --    Exception Tests    -------------------- */
	/* --------------------------------------------- */
	
	
	@Test
	public void testCheckedExceptionNotThrown() throws Exception
	{
		
		ClassObject cls=createClass();
		
		//create method
		List<Class<? extends Throwable>> exceptions=new ArrayList<Class<? extends Throwable>>();
		exceptions.add(ClassNotFoundException.class);
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", new ParameterObject[] {new ParameterObject<MethodClass>("param1", MethodClass.class)}, exceptions);
		method.setCode(clsGenerator.lineFactory.methods.invokeMethod("testVoid", "param1", null, null));
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		MethodClass obj=new MethodClass();
		Method mth=clsResult.getMethod("method1", new Class[] {MethodClass.class});
		Object result=mth.invoke(clsResult.newInstance(), new Object[] {obj});
		assertNull(result);
		assertEquals(1, obj.methodCheck());
	}
	
	@Test
	public void testCheckedExceptionThrown() throws Exception
	{
		ClassObject cls=createClass();
		
		//create method
		List<Class<? extends Throwable>> exceptions=new ArrayList<Class<? extends Throwable>>();
		exceptions.add(ClassNotFoundException.class);
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", new ParameterObject[]{new ParameterObject<MethodClass>("param1", MethodClass.class)}, exceptions);
		method.setCode(clsGenerator.lineFactory.methods.throwNewException(ClassNotFoundException.class, "Class not found in there"));
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		MethodClass obj=new MethodClass();
		Method mth=clsResult.getMethod("method1", new Class[] {MethodClass.class});
		
		Object result=null;
		try
		{
			result=mth.invoke(clsResult.newInstance(), new Object[] {obj});
			result.toString();
		}catch(Exception ex)
		{
			if(ex.getCause()==null || !ex.getCause().getMessage().equals("Class not found in there"))
				throw ex;
		}
		assertNull(result);
		
		
	}
	
	@Test
	public void testUncheckedExceptionThrown() throws Exception
	{
		ClassObject cls=createClass();
		
		//create method
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", new ParameterObject[]{new ParameterObject<MethodClass>("param1", MethodClass.class)});
		method.setCode(clsGenerator.lineFactory.methods.throwNewException(RuntimeException.class, "Class not found in there"));
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		MethodClass obj=new MethodClass();
		Method mth=clsResult.getMethod("method1", new Class[] {MethodClass.class});
		
		Object result=null;
		try
		{
			result=mth.invoke(clsResult.newInstance(), new Object[] {obj});
			result.toString();
		}catch(Exception ex)
		{
			if(ex.getCause()==null || !ex.getCause().getMessage().equals("Class not found in there"))
				throw ex;
		}
		assertNull(result);
		
	}
	
	//TODO: Exception thrown inside the method

}
