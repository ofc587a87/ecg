package test.progenies.ecg.asm31.methods.invoke;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestExternalFieldMethods.class, TestExternalLocalMethods.class,
		TestExternalStaticMethods.class, TestInternalLocalMethods.class,
		TestInternalStaticMethods.class })
public class AllMethodInvokeTests {

}
