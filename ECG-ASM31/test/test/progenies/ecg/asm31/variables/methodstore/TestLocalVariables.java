package test.progenies.ecg.asm31.variables.methodstore;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.lang.reflect.Method;

import org.junit.Test;

import test.progenies.ecg.asm31.variables.BaseTestLocalVariables;

import com.progenies.ecg.model.ClassObject;
import com.progenies.ecg.model.FieldObject;
import com.progenies.ecg.model.MethodObject;
import com.progenies.ecg.model.codeparts.CodeBlock;

public class TestLocalVariables extends  BaseTestLocalVariables {

	@Test
	public void testStoreMethodValue() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar", int.class));
		block.add(clsGenerator.lineFactory.methods.invokeMethodAndStore("instanceMethod", "this", null, int.class, "localVar"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localVar"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Integer);
		assertEquals(5, result);
	}
	
	
	@Test
	public void testStoreStaticMethodValue() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar", int.class));
		block.add(clsGenerator.lineFactory.methods.invokeStaticMethodAndStore("staticMethod", null, null, int.class, "localVar"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localVar"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Integer);
		assertEquals(8, result);
	}
	
	
	
	@Test
	public void testStoreExternalMethodLocalObjectValue() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar", int.class));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localObjVar", ExternalClass.class));
		block.add(clsGenerator.lineFactory.objects.createObjectAndStore(ExternalClass.class, null, "localObjVar"));
		block.add(clsGenerator.lineFactory.methods.invokeMethodAndStore("externalMethodInt", "localObjVar", null, int.class, "localVar"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localVar"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Integer);
		assertEquals(9, result);
	}
	
	
	@Test
	public void testStoreExternalMethodFieldObjectLocalValue() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		
		//field with object
		FieldObject<ExternalClass> fld=clsGenerator.objectFactory.fields.createField("fieldObj", ExternalClass.class, false);
		fld.setInitialize(true); //initialize without parameters (it would be specified on method "setInitializationParams"
		cls.getFields().add(fld);
		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar", int.class));
		block.add(clsGenerator.lineFactory.methods.invokeMethodAndStore("externalMethodInt", "fieldObj", null, int.class, "localVar"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localVar"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Integer);
		assertEquals(9, result);
	}
	
	
	@Test
	public void testStoreStaticExternalMethodLocalValue() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localVar", int.class));
		block.add(clsGenerator.lineFactory.methods.invokeStaticMethodAndStore("externalStaticMethodInt", ExternalClass.class, null, int.class, "localVar"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localVar"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Integer);
		assertEquals(19, result);
	}

}
