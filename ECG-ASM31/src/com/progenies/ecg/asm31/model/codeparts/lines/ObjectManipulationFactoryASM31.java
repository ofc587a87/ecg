package com.progenies.ecg.asm31.model.codeparts.lines;

import com.progenies.ecg.model.ParameterObject;
import com.progenies.ecg.model.codeparts.lines.IObjectManipulationFactory;
import com.progenies.ecg.model.codeparts.lines.LineCodePart;

public class ObjectManipulationFactoryASM31 implements	IObjectManipulationFactory {

	
	
	/* (non-Javadoc)
	 * @see com.progenies.ecg.model.codeparts.lines.asm31_impl.ILineFactory#createObjectAndStore(java.lang.Class, com.progenies.ecg.model.ParameterObject[], java.lang.String)
	 */
	@Override
	public <T> LineCodePart createObjectAndStore(Class<T> classType, ParameterObject<?> constructorParams[], String variableName)
	{
		return new CreateObjectLinePart<T>(classType, constructorParams, variableName);
	}

}
