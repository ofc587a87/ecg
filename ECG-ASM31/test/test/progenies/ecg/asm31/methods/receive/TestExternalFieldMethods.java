package test.progenies.ecg.asm31.methods.receive;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.fail;

import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import org.junit.Test;

import test.progenies.ecg.asm31.methods.BaseTestExternalFieldMethods;

import com.progenies.ecg.model.ClassObject;
import com.progenies.ecg.model.MethodObject;
import com.progenies.ecg.model.ParameterObject;
import com.progenies.ecg.model.codeparts.CodeBlock;

/**
 * Test method invocation, being the methods external to the class, from a field object
 * @author ofc587a87
 *
 */
public class TestExternalFieldMethods extends BaseTestExternalFieldMethods {

	
	/* --------------------------------------------- */
	/* --    Receipt Tests    ----------------------- */
	/* --------------------------------------------- */
	
	@Test
	public void testReceiveVoid() throws Exception
	{
		ClassObject cls=createClass();
		
		//create method
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setCode(clsGenerator.lineFactory.methods.invokeMethod("testVoid", "fieldObj", null, null));
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		MethodClass obj=new MethodClass();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Constructor<?> ctr=clsResult.getConstructor(new Class[] {MethodClass.class});
		Object result=mth.invoke(ctr.newInstance(new Object[] {obj}), (Object[])null);
		assertNull(result);
		assertEquals(1, obj.methodCheck());
	}
	
	@Test
	public void testReceivePrimitiveSize1() throws Exception
	{
		ClassObject cls=createClass();
		
		//create method
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("value", int.class));
		block.add(clsGenerator.lineFactory.methods.invokeMethodAndStore("testInt", "fieldObj", null, int.class, "value"));
		block.add(clsGenerator.lineFactory.methods.invokeMethod("setValue", "fieldObj", new ParameterObject<?>[]{new ParameterObject<Integer>("p1", "value", int.class)}, null));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		MethodClass obj=new MethodClass();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Constructor<?> ctr=clsResult.getConstructor(new Class[] {MethodClass.class});
		Object result=mth.invoke(ctr.newInstance(new Object[] {obj}), (Object[])null);
		assertNull(result);
		assertEquals(5, obj.methodCheck());
	}
	
	
	@Test
	public void testReceivePrimitiveSize2() throws Exception
	{
		ClassObject cls=createClass();
		
		//create method
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("value", int.class));
		block.add(clsGenerator.lineFactory.methods.invokeMethodAndStore("testDouble", "fieldObj", null, double.class, "value"));
		block.add(clsGenerator.lineFactory.methods.invokeMethod("setValue", "fieldObj", new ParameterObject<?>[]{new ParameterObject<Integer>("p1", "value", int.class)}, null));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		MethodClass obj=new MethodClass();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Constructor<?> ctr=clsResult.getConstructor(new Class[] {MethodClass.class});
		Object result=mth.invoke(ctr.newInstance(new Object[] {obj}), (Object[])null);
		assertNull(result);
		assertEquals(7, obj.methodCheck());
	}
	
	@Test
	public void testReceiveObject() throws Exception
	{
		ClassObject cls=createClass();
		
		//create method
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("tmpObj", ValueObject.class));
		block.add(clsGenerator.lineFactory.methods.invokeMethodAndStore("testValueObject", "fieldObj", new ParameterObject<?>[] {new ParameterObject<Integer>("param1", int.class, 8)}, ValueObject.class, "tmpObj"));
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("value", int.class));
		block.add(clsGenerator.lineFactory.methods.invokeMethodAndStore("getInternalValue", "tmpObj", null, int.class, "value"));
		block.add(clsGenerator.lineFactory.methods.invokeMethod("setValue", "fieldObj", new ParameterObject<?>[]{new ParameterObject<Integer>("p1", "value", int.class)}, null));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		MethodClass obj=new MethodClass();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Constructor<?> ctr=clsResult.getConstructor(new Class[] {MethodClass.class});
		Object result=mth.invoke(ctr.newInstance(new Object[] {obj}), (Object[])null);
		assertNull(result);
		assertEquals(8, obj.methodCheck());
	}
	
	
	@Test(expected=NullPointerException.class)
	public void testReceiveNullObject() throws Throwable
	{
		ClassObject cls=createClass();
		
		//create method
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("tmpObj", ValueObject.class));
		block.add(clsGenerator.lineFactory.methods.invokeMethodAndStore("testValueObject", "fieldObj", new ParameterObject<?>[] {new ParameterObject<Integer>("param1", int.class, -8)}, ValueObject.class, "tmpObj"));
		block.add(clsGenerator.lineFactory.methods.invokeMethodAndStore("getInternalValue", "tmpObj", null, int.class, "value"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		MethodClass obj=new MethodClass();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Constructor<?> ctr=clsResult.getConstructor(new Class[] {MethodClass.class});
		
		try
		{
			Object result=mth.invoke(ctr.newInstance(new Object[] {obj}), (Object[])null);
			result.hashCode();
			fail("Should has been thrown an exception!");
		}catch(InvocationTargetException ex)
		{
			throw ex.getCause();
		}
	}
	

}
