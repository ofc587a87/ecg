package com.progenies.ecg.asm31.model.factory;

import java.util.List;

import com.progenies.ecg.model.MethodObject;
import com.progenies.ecg.model.ParameterObject;
import com.progenies.ecg.model.factory.IObjectMethodFactory;

public class ObjectMethodFactoryASM31 implements IObjectMethodFactory {



	@Override
	public MethodObject createMethod(String name)
	{
		return createMethod(name, (ParameterObject<?>[])null, (Class<?>)null);
	}
	

	@Override
	public MethodObject createMethod(String name, Class<?> returnType) {
		return createMethod(name, (ParameterObject<?>[])null, returnType);
	}

	@Override
	public MethodObject createMethod(String name, ParameterObject<?>[] parameters) {
		return createMethod(name, parameters, (Class<?>)null);
	}


	@Override
	public MethodObject createMethod(String name, ParameterObject<?>[] parameters, Class<?> returnType)
	{
		return createMethod(name, parameters, returnType, null);
	}

	@Override
	public MethodObject createMethod(String name, ParameterObject<?>[] parameters, List<Class<? extends Throwable>> exceptions) {
		return createMethod(name, parameters, null, exceptions);
	}

	@Override
	public MethodObject createMethod(String name, ParameterObject<?>[] parameters, Class<?> returnType, List<Class<? extends Throwable>> exceptions) {
		if(name==null)
			throw new IllegalArgumentException("name cannot be null");
		return new MethodObjectASM31(name, parameters, returnType, exceptions);
	}

}
