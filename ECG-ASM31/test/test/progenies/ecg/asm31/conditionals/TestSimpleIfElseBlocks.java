package test.progenies.ecg.asm31.conditionals;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.lang.reflect.Method;

import org.junit.Before;
import org.junit.Test;

import com.progenies.ecg.generator.ClassGenerator;
import com.progenies.ecg.generator.ClassGeneratorFactory;
import com.progenies.ecg.model.ClassObject;
import com.progenies.ecg.model.ConstructorObject;
import com.progenies.ecg.model.MethodObject;
import com.progenies.ecg.model.ValueReference;
import com.progenies.ecg.model.codeparts.CodeBlock;
import com.progenies.ecg.model.codeparts.IfElseCodePart;
import com.progenies.ecg.model.codeparts.lines.LineFactory;
import com.progenies.ecg.model.conditions.BooleanCondition;
import com.progenies.ecg.model.conditions.CombinationCondition;
import com.progenies.ecg.model.conditions.ICondition;

public class TestSimpleIfElseBlocks {

	

	protected ClassGenerator clsGenerator;
	private static int counter = 0;
	

	@Test
	public void testSimpleIfTrue() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));

		ICondition condition=new BooleanCondition<Boolean>(ValueReference.forStaticValue(true), BooleanCondition.ConditionType.IS_TRUE);
		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 23));
		CodeBlock elseBlock=clsGenerator.lineFactory.createCodeBlock();
		elseBlock.add(clsGenerator.lineFactory.variables.decrementVariable("localValue"));
		
		IfElseCodePart ifCode=clsGenerator.objectFactory.conditionals.createIfElseBlock(condition, bodyBlock, elseBlock);
		
		block.add(ifCode);
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(25, result);
	}
	
	
	@Test
	public void testSimpleIfFalse() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));

		ICondition condition=new BooleanCondition<Boolean>(ValueReference.forStaticValue(true), BooleanCondition.ConditionType.IS_FALSE);
		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 23));
		CodeBlock elseBlock=clsGenerator.lineFactory.createCodeBlock();
		elseBlock.add(clsGenerator.lineFactory.variables.decrementVariable("localValue"));
		
		IfElseCodePart ifCode=clsGenerator.objectFactory.conditionals.createIfElseBlock(condition, bodyBlock, elseBlock);
		
		block.add(ifCode);
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(1, result);
	}
	
	
	@Test
	public void testSimpleAndTT() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));

		ICondition conditionA=new BooleanCondition<Boolean>(ValueReference.forStaticValue(true), BooleanCondition.ConditionType.IS_TRUE);
		ICondition conditionB=new BooleanCondition<Boolean>(ValueReference.forStaticValue(true), BooleanCondition.ConditionType.IS_TRUE);

		ICondition ifCodeCombination=new CombinationCondition(conditionA, conditionB, CombinationCondition.CombinationConditionType.AND);
		
		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 23));

		CodeBlock elseBlock=clsGenerator.lineFactory.createCodeBlock();
		elseBlock.add(clsGenerator.lineFactory.variables.decrementVariable("localValue"));
		
		IfElseCodePart ifCode=clsGenerator.objectFactory.conditionals.createIfElseBlock(ifCodeCombination, bodyBlock, elseBlock);
		
		block.add(ifCode);
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(25, result);
	}
	
	
	@Test
	public void testSimpleAndTF() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));

		ICondition conditionA=new BooleanCondition<Boolean>(ValueReference.forStaticValue(true), BooleanCondition.ConditionType.IS_TRUE);
		ICondition conditionB=new BooleanCondition<Boolean>(ValueReference.forStaticValue(false), BooleanCondition.ConditionType.IS_TRUE);

		ICondition ifCodeCombination=new CombinationCondition(conditionA, conditionB, CombinationCondition.CombinationConditionType.AND);
		
		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 23));

		CodeBlock elseBlock=clsGenerator.lineFactory.createCodeBlock();
		elseBlock.add(clsGenerator.lineFactory.variables.decrementVariable("localValue"));
		
		IfElseCodePart ifCode=clsGenerator.objectFactory.conditionals.createIfElseBlock(ifCodeCombination, bodyBlock, elseBlock);
		
		block.add(ifCode);
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(1, result);
	}
	
	@Test
	public void testSimpleAndFT() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));

		ICondition conditionA=new BooleanCondition<Boolean>(ValueReference.forStaticValue(false), BooleanCondition.ConditionType.IS_TRUE);
		ICondition conditionB=new BooleanCondition<Boolean>(ValueReference.forStaticValue(true), BooleanCondition.ConditionType.IS_TRUE);

		ICondition ifCodeCombination=new CombinationCondition(conditionA, conditionB, CombinationCondition.CombinationConditionType.AND);
		
		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 23));

		CodeBlock elseBlock=clsGenerator.lineFactory.createCodeBlock();
		elseBlock.add(clsGenerator.lineFactory.variables.decrementVariable("localValue"));
		
		IfElseCodePart ifCode=clsGenerator.objectFactory.conditionals.createIfElseBlock(ifCodeCombination, bodyBlock, elseBlock);
		
		block.add(ifCode);
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(1, result);
	}
	
	@Test
	public void testSimpleAndFF() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));

		ICondition conditionA=new BooleanCondition<Boolean>(ValueReference.forStaticValue(false), BooleanCondition.ConditionType.IS_TRUE);
		ICondition conditionB=new BooleanCondition<Boolean>(ValueReference.forStaticValue(false), BooleanCondition.ConditionType.IS_TRUE);

		ICondition ifCodeCombination=new CombinationCondition(conditionA, conditionB, CombinationCondition.CombinationConditionType.AND);
		
		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 23));

		CodeBlock elseBlock=clsGenerator.lineFactory.createCodeBlock();
		elseBlock.add(clsGenerator.lineFactory.variables.decrementVariable("localValue"));
		
		IfElseCodePart ifCode=clsGenerator.objectFactory.conditionals.createIfElseBlock(ifCodeCombination, bodyBlock, elseBlock);
		
		block.add(ifCode);
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(1, result);
	}
	
	
	
	
	@Test
	public void testSimpleOrTT() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));

		ICondition conditionA=new BooleanCondition<Boolean>(ValueReference.forStaticValue(true), BooleanCondition.ConditionType.IS_TRUE);
		ICondition conditionB=new BooleanCondition<Boolean>(ValueReference.forStaticValue(true), BooleanCondition.ConditionType.IS_TRUE);

		ICondition ifCodeCombination=new CombinationCondition(conditionA, conditionB, CombinationCondition.CombinationConditionType.OR);
		
		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 23));

		CodeBlock elseBlock=clsGenerator.lineFactory.createCodeBlock();
		elseBlock.add(clsGenerator.lineFactory.variables.decrementVariable("localValue"));
		
		IfElseCodePart ifCode=clsGenerator.objectFactory.conditionals.createIfElseBlock(ifCodeCombination, bodyBlock, elseBlock);
		
		block.add(ifCode);
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(25, result);
	}
	
	
	@Test
	public void testSimpleOrTF() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));

		ICondition conditionA=new BooleanCondition<Boolean>(ValueReference.forStaticValue(true), BooleanCondition.ConditionType.IS_TRUE);
		ICondition conditionB=new BooleanCondition<Boolean>(ValueReference.forStaticValue(false), BooleanCondition.ConditionType.IS_TRUE);

		ICondition ifCodeCombination=new CombinationCondition(conditionA, conditionB, CombinationCondition.CombinationConditionType.OR);
		
		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 23));

		CodeBlock elseBlock=clsGenerator.lineFactory.createCodeBlock();
		elseBlock.add(clsGenerator.lineFactory.variables.decrementVariable("localValue"));
		
		IfElseCodePart ifCode=clsGenerator.objectFactory.conditionals.createIfElseBlock(ifCodeCombination, bodyBlock, elseBlock);
		
		block.add(ifCode);
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
//		AMSIfierUtil.printClass(cls);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(25, result);
	}
	
	@Test
	public void testSimpleOrFT() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));

		ICondition conditionA=new BooleanCondition<Boolean>(ValueReference.forStaticValue(false), BooleanCondition.ConditionType.IS_TRUE);
		ICondition conditionB=new BooleanCondition<Boolean>(ValueReference.forStaticValue(true), BooleanCondition.ConditionType.IS_TRUE);

		ICondition ifCodeCombination=new CombinationCondition(conditionA, conditionB, CombinationCondition.CombinationConditionType.OR);
		
		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 23));

		CodeBlock elseBlock=clsGenerator.lineFactory.createCodeBlock();
		elseBlock.add(clsGenerator.lineFactory.variables.decrementVariable("localValue"));
		
		IfElseCodePart ifCode=clsGenerator.objectFactory.conditionals.createIfElseBlock(ifCodeCombination, bodyBlock, elseBlock);
		
		block.add(ifCode);
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(25, result);
	}
	
	@Test
	public void testSimpleORFF() throws Exception
	{
		ClassObject cls=createClass();
		
		MethodObject method1=clsGenerator.objectFactory.methods.createMethod("method1", int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		
		block.add(clsGenerator.lineFactory.variables.createLocalVariable("localValue", int.class, 2));

		ICondition conditionA=new BooleanCondition<Boolean>(ValueReference.forStaticValue(false), BooleanCondition.ConditionType.IS_TRUE);
		ICondition conditionB=new BooleanCondition<Boolean>(ValueReference.forStaticValue(false), BooleanCondition.ConditionType.IS_TRUE);

		ICondition ifCodeCombination=new CombinationCondition(conditionA, conditionB, CombinationCondition.CombinationConditionType.OR);
		
		CodeBlock bodyBlock=clsGenerator.lineFactory.createCodeBlock();
		bodyBlock.add(clsGenerator.lineFactory.variables.incrementVariable("localValue", 23));

		CodeBlock elseBlock=clsGenerator.lineFactory.createCodeBlock();
		elseBlock.add(clsGenerator.lineFactory.variables.decrementVariable("localValue"));
		
		IfElseCodePart ifCode=clsGenerator.objectFactory.conditionals.createIfElseBlock(ifCodeCombination, bodyBlock, elseBlock);
		
		block.add(ifCode);
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("localValue"));
		method1.setCode(block);
		cls.getMethods().add(method1);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertEquals(1, result);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Before
	public void setUp() throws Exception {
		clsGenerator=ClassGeneratorFactory.getClassGenerator();
		assertNotNull(clsGenerator);
	}

	
	
	

	protected ClassObject createClass()
	{
		counter++;
		int index=counter;
		LineFactory lineFactory=clsGenerator.lineFactory;
		
		String subpackage=this.getClass().getPackage().getName().substring(this.getClass().getPackage().getName().lastIndexOf('.')+1);
		ClassObject cls=clsGenerator.objectFactory.classes.createClass("test.conditionals."+subpackage+".IfElseSimpleTestClass"+index);
		cls.setSuperClass(Object.class);

		
		//override default constructor
		ConstructorObject consObject=clsGenerator.objectFactory.constructors.createConstructor();
		consObject.setCode(lineFactory.methods.invokeSuperConstructor());
		cls.getConstructors().add(consObject);
		
		
		return cls;
		
	}


}
