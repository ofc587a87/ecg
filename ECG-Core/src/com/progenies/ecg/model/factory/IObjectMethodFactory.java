package com.progenies.ecg.model.factory;

import java.util.List;

import com.progenies.ecg.model.MethodObject;
import com.progenies.ecg.model.ParameterObject;

public interface IObjectMethodFactory
{
	

	
	/**
	 * Creates a new method, public without parameters
	 * @param name name of the new method. Will be no be checked for duplication or overloading.
	 * @return Constructor Object
	 */
	public MethodObject createMethod(String name);
	
	public MethodObject createMethod(String name, Class<?> returnType);
	
	/**
	 * Creates a new method, public with parameters
	 * @param parameters parameters of the constructor. It can be null or an empty Array.
	 * @param name name of the new method. Will be no be checked for duplication or overloading.
	 * @return Constructor Object
	 */
	public MethodObject createMethod(String name, ParameterObject<?>[] parameters);
	
	public MethodObject createMethod(String name, ParameterObject<?>[] parameters, List<Class<? extends Throwable>> exceptions);

	public MethodObject createMethod(String name, ParameterObject<?>[] parameters, Class<?> returnType);
	
	public MethodObject createMethod(String name, ParameterObject<?>[] parameters, Class<?> returnType, List<Class<? extends Throwable>> exceptions);

}
