package com.progenies.ecg.asm31.model.factory;

import com.progenies.ecg.model.ConstructorObject;
import com.progenies.ecg.model.ParameterObject;
import com.progenies.ecg.model.factory.IObjectConstructorFactory;

public class ObjectConstructorFactoryASM31 implements IObjectConstructorFactory {



	@Override
	public ConstructorObject createConstructor() {
		return createConstructor((ParameterObject<?>[])null);
	}

	@Override
	public ConstructorObject createConstructor(ParameterObject<?>[] parameters)
	{
		ConstructorObject constr=new ConstructorObjectASM31(parameters);
		constr.setName("<init>");
		return constr;
	}

}
