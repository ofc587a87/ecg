package test.progenies.ecg.asm31.conditionals;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TestSimpleIfBlocks.class, TestSimpleIfElseBlocks.class,
				TestComplexIfBlocks.class, TestCompareIfBlocks.class,
				TestSwitchCodePart.class})
public class AllConditionalTests {

}
