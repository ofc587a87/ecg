package com.progenies.ecg.asm31.model.codeparts;

import org.objectweb.asm.ClassVisitor;

public interface IConfigurableClassObject {

	/**
	 * Configures the class object for bytecode generation
	 * @param visitor  ClassVisitor object
	 */
	public void configure(int javaVersion, ClassVisitor visitor);

	public void generateBytecode();

}