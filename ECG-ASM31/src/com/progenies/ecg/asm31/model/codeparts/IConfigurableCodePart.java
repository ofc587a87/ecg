package com.progenies.ecg.asm31.model.codeparts;

import org.objectweb.asm.MethodVisitor;

import com.progenies.ecg.asm31.util.VariableRegister;
import com.progenies.ecg.model.AbstractCodeObject;
import com.progenies.ecg.model.codeparts.ICodePart;

/**
 * Define the methods a ICOdePart using ASM3.1 must implement (configure method) 
 * @author ofc587a87
 *
 */
public interface IConfigurableCodePart extends ICodePart
{
	
	public void configure(MethodVisitor visitor, AbstractCodeObject parent, VariableRegister varRegister);

}
