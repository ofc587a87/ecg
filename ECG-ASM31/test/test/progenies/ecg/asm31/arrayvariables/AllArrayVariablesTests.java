package test.progenies.ecg.asm31.arrayvariables;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import test.progenies.ecg.asm31.arrayvariables.bidimensional.AllBidimensionalVariablesTests;
import test.progenies.ecg.asm31.arrayvariables.tridimensional.AllTridimensionalVariablesTests;
import test.progenies.ecg.asm31.arrayvariables.unidimensional.AllUnidimensionalTests;

@RunWith(Suite.class)
@SuiteClasses({AllUnidimensionalTests.class, AllBidimensionalVariablesTests.class, AllTridimensionalVariablesTests.class})
public class AllArrayVariablesTests {

}
