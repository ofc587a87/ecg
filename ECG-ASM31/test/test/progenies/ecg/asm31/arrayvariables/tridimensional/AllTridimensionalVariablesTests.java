package test.progenies.ecg.asm31.arrayvariables.tridimensional;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

import test.progenies.ecg.asm31.arrayvariables.tridimensional.primitives.TestStaticArrayVariables;
import test.progenies.ecg.asm31.arrayvariables.tridimensional.primitives.TestInstanceArrayVariables;
import test.progenies.ecg.asm31.arrayvariables.tridimensional.primitives.TestLocalArrayVariables;

@RunWith(Suite.class)
@SuiteClasses({TestLocalArrayVariables.class, test.progenies.ecg.asm31.arrayvariables.tridimensional.objects.TestLocalArrayVariables.class,
			   TestInstanceArrayVariables.class, test.progenies.ecg.asm31.arrayvariables.tridimensional.objects.TestInstanceArrayVariables.class,
			   TestStaticArrayVariables.class, test.progenies.ecg.asm31.arrayvariables.tridimensional.objects.TestStaticArrayVariables.class
			   })
public class AllTridimensionalVariablesTests {

}
