package com.progenies.ecg.asm31.model.factory;

import com.progenies.ecg.model.factory.IObjectClassFactory;
import com.progenies.ecg.model.factory.IObjectConditionalFactory;
import com.progenies.ecg.model.factory.IObjectConstructorFactory;
import com.progenies.ecg.model.factory.IObjectFieldFactory;
import com.progenies.ecg.model.factory.IObjectLoopFactory;
import com.progenies.ecg.model.factory.IObjectMethodFactory;
import com.progenies.ecg.model.factory.IObjectTryCatchFactory;
import com.progenies.ecg.model.factory.ObjectFactory;

/**
 * Object Factory implementation for ASM3.1
 * 
 * @author ofc587a87
 * 
 * @see ObjectFactory
 *
 */
public final class ObjectFactoryASM31 extends ObjectFactory
{

	@Override
	protected IObjectClassFactory createClassObjectFactory() {
		return new ObjectClassFactoryASM31();
	}

	@Override
	protected IObjectFieldFactory createFieldObjectFactory() {
		return new ObjectFieldFactoryASM31();
	}

	@Override
	protected IObjectConstructorFactory createConstructorObjectFactory() {
		return new ObjectConstructorFactoryASM31();
	}

	@Override
	protected IObjectMethodFactory createMethodObjectFactory() {
		return new ObjectMethodFactoryASM31();
	}

	@Override
	protected IObjectTryCatchFactory createTryCatchObjectFactory() {
		return new ObjectTryCatchFactoryASM31();
	}

	@Override
	protected IObjectConditionalFactory createConditionalsObjectFactory() {
		return new ObjectConditionalFactoryASM31();
	}

	@Override
	protected IObjectLoopFactory createLoopObjectFactory() {
		return new ObjectLoopsFactoryASM31();
	}

	



	

}
