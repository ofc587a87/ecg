package test.progenies.ecg.asm31.variables.creation;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertTrue;
import static org.junit.Assert.fail;

import java.lang.reflect.Method;

import org.junit.Test;
import org.objectweb.asm.Opcodes;

import test.progenies.ecg.asm31.variables.BaseTestInstanceVariables;

import com.progenies.ecg.model.ClassObject;
import com.progenies.ecg.model.FieldObject;
import com.progenies.ecg.model.MethodObject;
import com.progenies.ecg.model.codeparts.CodeBlock;

public class TestInstanceVariables extends BaseTestInstanceVariables {

	@Test
	public void testCreateEmptyField() throws Exception
	{
		ClassObject cls=createClass();
		
		//uninitialized instance field (by default, longs are initialized to zero)
		cls.getFields().add(clsGenerator.objectFactory.fields.createField("instanceField2", long.class));
		
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setReturnType(long.class);
		method.setCode(clsGenerator.lineFactory.methods.returnVariableValue("instanceField2"));
		cls.getMethods().add(method);
		
		//uninitialized instance variables defaults to 0 or null, depending of their type. 
		Class<?> clsResult=clsGenerator.generateClass(cls);
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Long);
		assertEquals(0l, result);
	}
	
	@Test
	public void testCreateVariableWithStaticValue() throws Exception
	{
		ClassObject cls=createClass();
		
		//initialized instance field
		FieldObject<Long> fld=clsGenerator.objectFactory.fields.createField("instanceField2", long.class);
		fld.setValue(7l);
		cls.getFields().add(fld);
		
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setReturnType(long.class);
		method.setCode(clsGenerator.lineFactory.methods.returnVariableValue("instanceField2"));
		cls.getMethods().add(method);
		 
		Class<?> clsResult=clsGenerator.generateClass(cls);
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Long);
		assertEquals(7l, result);
	}
	
	@Test  //FIXME: Issue 15 (expected=IllegalAccessError.class)
	public void testCreateFinalVariableWithStaticValue() throws Exception
	{
		ClassObject cls=createClass();
		
		//initialized instance field
		FieldObject<Long> fld=clsGenerator.objectFactory.fields.createField("instanceField2", long.class);
		fld.setValue(7l);
		fld.setModifiers(new int[] {Opcodes.ACC_PROTECTED, Opcodes.ACC_FINAL});
		cls.getFields().add(fld);
		
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setReturnType(long.class);
		method.setCode(clsGenerator.lineFactory.methods.returnVariableValue("instanceField2"));
		cls.getMethods().add(method);
		
		MethodObject method2=clsGenerator.objectFactory.methods.createMethod("method2");
		method2.setCode(clsGenerator.lineFactory.variables.setVariableValue("instanceField2", 9l));
		cls.getMethods().add(method2);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Long);
		assertEquals(7l, result);
		
		Method mth2=clsResult.getMethod("method2", (Class[])null);
		mth2.invoke(obj, (Object[])null);
		result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof Long);
		assertEquals(9l, result);
	}
	
	@Test
	public void testCreateVariableWithObjectValue() throws Exception
	{
		ClassObject cls=createClass();
		
		//initialized instance field
		FieldObject<String> fld=clsGenerator.objectFactory.fields.createField("instanceField2", String.class);
		fld.setValue("CHECKING");
		cls.getFields().add(fld);
				
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setReturnType(String.class);
		method.setCode(clsGenerator.lineFactory.methods.returnVariableValue("instanceField2"));
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertTrue(result instanceof String);
		assertEquals("CHECKING", result);
	}
	
	@Test  //FIXME: Issue 15 (expected=IllegalAccessError.class)
	public void testCreateFinalVariableWithObjectValue() throws Exception
	{
		ClassObject cls=createClass();
		
		//initialized instance field
		FieldObject<String> fld=clsGenerator.objectFactory.fields.createField("instanceField2", String.class);
		fld.setValue("CHECKING");
		fld.setModifiers(new int[] {Opcodes.ACC_PROTECTED, Opcodes.ACC_FINAL});
		cls.getFields().add(fld);
				
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setReturnType(String.class);
		method.setCode(clsGenerator.lineFactory.methods.returnVariableValue("instanceField2"));
		cls.getMethods().add(method);
		
		MethodObject method2=clsGenerator.objectFactory.methods.createMethod("method2");
		method2.setCode(clsGenerator.lineFactory.variables.setVariableValue("instanceField2", "MODIFIED"));
		cls.getMethods().add(method2);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertTrue(result instanceof String);
		assertEquals("CHECKING", result);
		
		Method mth2=clsResult.getMethod("method2", (Class[])null);
		mth2.invoke(obj, (Object[])null);
		result=mth.invoke(obj, (Object[])null);
		assertTrue(result instanceof String);
		assertEquals("MODIFIED", result);
	}
	
	@Test
	public void testCreateWithNullObjectValue() throws Exception
	{
		ClassObject cls=createClass();
		
		//initialized instance field (by default, objects are initialized to null)
		FieldObject<String> fld=clsGenerator.objectFactory.fields.createField("instanceField2", String.class);
//		fld.setValue(null);
		cls.getFields().add(fld);
		
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setReturnType(String.class);
		method.setCode(clsGenerator.lineFactory.methods.returnVariableValue("instanceField2"));
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNull(result);
	}
	
	@Test
	public void testSetPrimitiveWithNullObjectValue() throws Exception
	{
		ClassObject cls=createClass();
		FieldObject<Integer> fld=clsGenerator.objectFactory.fields.createField("instanceField2", int.class);
		cls.getFields().add(fld);
		
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setReturnType(int.class);
		CodeBlock block=clsGenerator.lineFactory.createCodeBlock();
		block.add(clsGenerator.lineFactory.variables.setVariableValueNull("instanceField2"));
		block.add(clsGenerator.lineFactory.methods.returnVariableValue("instanceField2"));
		method.setCode(block);
		cls.getMethods().add(method);
		
		try
		{
			clsGenerator.generateClass(cls);
			fail("Error should had been thrown");
		}
		catch(RuntimeException ex)
		{
			assertEquals("The variable type is primitive or array, can't accept null values", ex.getMessage());
		}
	}
	

	@Test(expected=NullPointerException.class)
	public void testCreateAndInitializePrimitive() throws Exception
	{
		ClassObject cls=createClass();
		
		//autoinitialized instance field
		FieldObject<Integer> fld=clsGenerator.objectFactory.fields.createField("instanceField2", int.class);
		fld.setInitialize(true);
		cls.getFields().add(fld);
		
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setReturnType(int.class);
		method.setCode(clsGenerator.lineFactory.methods.returnVariableValue("instanceField2"));
		cls.getMethods().add(method);
		
		clsGenerator.generateClass(cls); //exception thrown!!

		fail("An exceptions should have been thrown!!");
		
	}
	
	
	@Test
	public void testCreateAndInitializeObject() throws Exception
	{
		ClassObject cls=createClass();

		//autoinitialized instance field
		FieldObject<String> fld=clsGenerator.objectFactory.fields.createField("instanceField2", String.class);
		fld.setInitialize(true);
		cls.getFields().add(fld);
		
		MethodObject method=clsGenerator.objectFactory.methods.createMethod("method1");
		method.setReturnType(String.class);
		method.setCode(clsGenerator.lineFactory.methods.returnVariableValue("instanceField2"));
		cls.getMethods().add(method);
		
		Class<?> clsResult=clsGenerator.generateClass(cls);
		
		Object obj=clsResult.newInstance();
		Method mth=clsResult.getMethod("method1", (Class[])null);
		Object result=mth.invoke(obj, (Object[])null);
		assertNotNull(result);
		assertTrue(result instanceof String);
		assertEquals("", result);
	}


}
